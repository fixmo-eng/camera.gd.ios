//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZCTextPreviewController.h"
#import "GoodSecureFileManager.h"
#import "SZLFile.h"

#define kMinimumTextFontSize		8
#define kMaximumTextFontSize		48
#define kCodeFontFamily				@"Courier"

@interface SZCTextPreviewController ()
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, strong) UISegmentedControl *fontControl;
@property (nonatomic, assign) int textFontSize;
@property (nonatomic, assign) bool isCode;
@end

@implementation SZCTextPreviewController

+ (SZCTextPreviewController *) createTextPreviewer
{
    NSBundle *SafeZoneCommonBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"SafeZoneCommonBundle" ofType:@"bundle"]];
    UIStoryboard *theStoryboard = [UIStoryboard storyboardWithName:kStoryboard_Previewers_both bundle:SafeZoneCommonBundle];
    SZCTextPreviewController *theController = [theStoryboard instantiateViewControllerWithIdentifier:@"SZCTextPreviewController"];
    theController.textFontSize = 16;
	theController.isCode = false;

	return theController;
}

+ (SZCTextPreviewController *) createCodePreviewer
{
    SZCTextPreviewController *theController = [[self class] createTextPreviewer];
	theController.textFontSize = 14;
	theController.isCode = true;

	return theController;
}

- (void) viewDidLoad
{
	[super viewDidLoad];

	self.fontControl = [[UISegmentedControl alloc] initWithItems:
						[NSArray arrayWithObjects:
							[UIImage imageNamed:@"FixMailRsrc.bundle/decreaseFont.png"],
							[UIImage imageNamed:@"FixMailRsrc.bundle/increaseFont.png"],
						 nil]];
	[self.fontControl addTarget:self action:@selector(toggleFontChange:) forControlEvents:UIControlEventValueChanged];
	self.fontControl.frame = CGRectMake(0, 0, 90, 30.0);
	self.fontControl.segmentedControlStyle = UISegmentedControlStyleBar;
	self.fontControl.momentary = YES;

	UIBarButtonItem *fontButton = [[UIBarButtonItem alloc] initWithCustomView:self.fontControl];


	NSMutableArray *buttonArray = [NSMutableArray arrayWithArray:self.navigationItem.rightBarButtonItems];
	[buttonArray addObject:fontButton];
	self.navigationItem.rightBarButtonItems = buttonArray;
}

- (void) previewFile
{
	NSString *theString = @"";
    if (self.file.path.length > 0) {
        NSData *fileData = [[SZLSecureFileManager defaultManager] contentsAtPath:self.file.path];
		theString = [[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    }

	self.textView.text = theString;

	[self redoControls];

    return;
}

- (void) setTextFontSize:(int)inTextFontSize
{
	int newFontSize = inTextFontSize;
	if (newFontSize < kMinimumTextFontSize) newFontSize = kMinimumTextFontSize;
	if (newFontSize > kMaximumTextFontSize) newFontSize = kMaximumTextFontSize;

	if (newFontSize != _textFontSize) {
		_textFontSize = newFontSize;
		[self redoControls];
	}

	return;
}

#pragma mark - IBActions

- (IBAction) toggleFontChange:(id)inSender
{
	if (self.fontControl.selectedSegmentIndex == 0) {
		--self.textFontSize;
	} else {
		++self.textFontSize;
	}

	return;
}

#pragma mark - Private methods

- (void) redoControls
{
	if (self.isCode) {
		self.textView.font = [UIFont fontWithName:kCodeFontFamily size:_textFontSize];
	} else {
		self.textView.font = [UIFont systemFontOfSize:_textFontSize];
	}
	[self.fontControl setEnabled:(_textFontSize > kMinimumTextFontSize) forSegmentAtIndex:0];
	[self.fontControl setEnabled:(_textFontSize < kMaximumTextFontSize) forSegmentAtIndex:1];
}

@end


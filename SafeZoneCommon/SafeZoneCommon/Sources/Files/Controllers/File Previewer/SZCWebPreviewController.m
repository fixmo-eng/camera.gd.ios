//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZCWebPreviewController.h"
#import "GoodSecureFileManager.h"
#import "SZLFile.h"

@interface SZCWebPreviewController ()
@property (nonatomic, weak) IBOutlet UIWebView* webView;
@end

@implementation SZCWebPreviewController

+ (SZCWebPreviewController *) createWebPreviewer
{
    NSBundle *SafeZoneCommonBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"SafeZoneCommonBundle" ofType:@"bundle"]];
    UIStoryboard *theStoryboard = [UIStoryboard storyboardWithName:kStoryboard_Previewers_both bundle:SafeZoneCommonBundle];
    SZCWebPreviewController *theController = [theStoryboard instantiateViewControllerWithIdentifier:@"SZCWebPreviewController"];
	return theController;
}

- (void)previewFile
{
    if (self.file) {
        NSData* fileData = [[SZLSecureFileManager defaultManager] contentsAtPath:self.file.path];
        if (fileData) {
            [self.webView loadData:fileData MIMEType:self.file.mime textEncodingName:@"utf-8" baseURL:[NSURL fileURLWithPath:self.file.path]];
        }
    }
    return;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeOther) {
        return YES;
    } else {
        return NO;
    }
}

@end

//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZCImagePreviewController.h"
#import "GoodSecureFileManager.h"
#import "SZLFile.h"

@interface SZCImagePreviewController ()
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@end

@implementation SZCImagePreviewController

+ (SZCImagePreviewController *) createImagePreviewer
{

    NSBundle *SafeZoneCommonBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"SafeZoneCommonBundle" ofType:@"bundle"]];
    UIStoryboard *theStoryboard = [UIStoryboard storyboardWithName:kStoryboard_Previewers_both bundle:SafeZoneCommonBundle];
    SZCImagePreviewController *theController = [theStoryboard instantiateViewControllerWithIdentifier:@"SZCImagePreviewController"];

	return theController;
}

- (void) previewFile
{
	UIImage *theImage = nil;
    if (self.file.path.length > 0) {
        NSData *fileData = [[SZLSecureFileManager defaultManager] contentsAtPath:self.file.path];
		theImage = [UIImage imageWithData:fileData];
    }

	// we'll analyze the size of the image and do the following:
	// bigger: aspect fit
	// smaller: center
	CGSize imageSize = theImage.size;
	if ((imageSize.width > self.imageView.frame.size.width) || (imageSize.height > self.imageView.frame.size.height)) {
		self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        theImage = [self downsizeImage:theImage];
		self.scrollView.maximumZoomScale = 5.0;
	} else {
		self.imageView.contentMode = UIViewContentModeCenter;
		float widthRatio = self.imageView.frame.size.width / imageSize.width;
		float heightRatio = self.imageView.frame.size.height / imageSize.height;
		// only allow it to scale to screen-size
		self.scrollView.maximumZoomScale = (widthRatio > heightRatio) ? widthRatio : heightRatio;
	}
	self.imageView.image = theImage;

    return;
}

-(UIImage*)downsizeImage:(UIImage*)image
{
    CGFloat oldWidth = image.size.width;
    CGFloat oldHeight = image.size.height;

    CGFloat scaleFactor = (oldWidth > oldHeight) ? 1024 / oldWidth : 1024 / oldHeight;

    CGFloat newHeight = oldHeight * scaleFactor;
    CGFloat newWidth = oldWidth * scaleFactor;

    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark UIScrollViewDelegate methods

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)inScrollView
{
	return self.imageView;
}

@end

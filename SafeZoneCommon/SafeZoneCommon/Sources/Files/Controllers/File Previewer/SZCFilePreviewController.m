//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZCFilePreviewController.h"
#import "GoodSecureFileManager_SZL.h"

#import "PSPDFActionSheet.h"
#import "PSPDFAlertView.h"

#import "SZLFileFactory.h"
#import "SZLFileImage.h"
#import "SZLFileCert.h"

#import "SZLFile+previewer.h"
#import "SZLFileImage+previewer.h"
#import "SZLFileCert+previewer.h"

#define kReplace 0
#define kSave 1
#define kEdit 2

#define Localized(key) \
[[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"SafeZoneCommonBundle" ofType:@"bundle"]] localizedStringForKey:(key) value:@"" table:@"filebrowser"]


@implementation SZCFilePreviewController

+ (SZCFilePreviewController *) createPreviewer:(NSString *)inFilePath name:(NSString *)inSaveFileName passPhrase:(NSString *)inPassPhrase
{
	NSParameterAssert(inFilePath != nil);
	NSParameterAssert(inSaveFileName != nil);

    SZLFile *file = [[SZLFileFactory instance] fileFromPath:inFilePath];
	SZCFilePreviewController *theController = [SZLFile previewerWithPassPhrase:inPassPhrase];
    
	theController.file = file;

	return theController;
}

- (void) viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *barButton = nil;

    
    if (self.canSave && !(self.file.isEditable && self.canEdit)) {
        barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                  target:self
                                                                  action:@selector(toggleSave)];
    } else if (self.file.isEditable && self.canEdit && !self.canSave) {
        barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                  target:self
                                                                  action:@selector(toggleEdit)];
    } else if ((self.file.isEditable && self.canEdit) || self.canSave) {
        barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                  target:self
                                                                  action:@selector(toggleAction)];
    }

    self.navigationItem.rightBarButtonItem = barButton;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.navigationController.navigationBarHidden = NO;
    self.navigationController.toolbarHidden = YES;
    self.title = self.file.name;

    [self previewFile];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD()) {
        return YES;
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
    }
}

- (void) toggleAction
{
    PSPDFActionSheet* actionSheet = [[PSPDFActionSheet alloc] initWithTitle:Localized(@"ACTION")];

    //FIXME: Translate
    if (self.canSave) {
        [actionSheet addButtonWithTitle:Localized(@"SAVE") block:^{
            [self toggleSave];
        }];
    }

    if (self.canEdit) {
        [actionSheet addButtonWithTitle:Localized(@"EDIT") block:^{
            [self toggleEdit];
        }];
    }
    [actionSheet setCancelButtonWithTitle:Localized(@"CANCEL") block:nil];
    [actionSheet showInView:self.view];
}

- (void) toggleEdit
{
    if ([self.delegate respondsToSelector:@selector(filePreview:didEditItemWithPath:)])
        [self.delegate filePreview:self didEditItemWithPath:self.file.path];
}

- (void) toggleSave
{
    if ([self.delegate respondsToSelector:@selector(filePreview:didSaveItemWithPath:)])
         [self.delegate filePreview:self didSaveItemWithPath:self.file.path];
}

-(void)previewFile
{
}

@end

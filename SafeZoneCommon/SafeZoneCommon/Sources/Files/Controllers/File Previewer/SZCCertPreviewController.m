//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZCCertPreviewController.h"
#import "SZLSecureFileManager.h"
#import "SZLCertificate.h"
#import "SZLFile.h"

@interface SZCCertPreviewController ()
@property (nonatomic, weak) IBOutlet UITextView *attributesTextView;
@property (nonatomic, weak) IBOutlet UITextView *rawTextView;
@property (nonatomic, strong) NSString *passPhrase;
@property (nonatomic, assign) bool isPEM;
@end

@implementation SZCCertPreviewController

+ (SZCCertPreviewController *) createP12Previewer:(NSString *)inPassPhrase
{
    NSBundle *SafeZoneCommonBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"SafeZoneCommonBundle" ofType:@"bundle"]];
    UIStoryboard *theStoryboard = [UIStoryboard storyboardWithName:kStoryboard_Previewers_both bundle:SafeZoneCommonBundle];
    SZCCertPreviewController *theController = [theStoryboard instantiateViewControllerWithIdentifier:@"SZCCertPreviewController"];

	theController.isPEM = false;
	theController.passPhrase = inPassPhrase;

	return theController;
}

+ (SZCCertPreviewController *) createPEMPreviewer:(NSString *)inPassPhrase
{
	SZCCertPreviewController *theController = [[self class] createPEMPreviewer:inPassPhrase];
	theController.isPEM = true;

	return theController;
}

- (void) viewDidLoad
{
	[super viewDidLoad];
}

- (void) previewFile
{
	NSString *rawString = @"<Not Applicable>";
	NSString *attributesString = @"";
    if (self.file.path.length > 0) {
        NSString * kTempCertificateIdentifier = @"tempCertificate";
        NSData *fileData = [[SZLSecureFileManager defaultManager] contentsAtPath:self.file.path];
        SZLCertificate *cert = [SZLCertificate newCertificateFromPKCS12Data:fileData
                                                              andPassphrase:self.passPhrase
                                                             withIdentifier:kTempCertificateIdentifier];
        if (cert) {
            NSArray *theAttributes = [cert getAllAttributes];
            [cert deleteCertificate];

            if (theAttributes.count > 0) {
                attributesString = [theAttributes componentsJoinedByString:@"\n"];
            }
        }

		if (self.isPEM) {
			rawString = [[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
		}
    }

	self.attributesTextView.text = attributesString;
	self.rawTextView.text = rawString;

    return;
}

@end


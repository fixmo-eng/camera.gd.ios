//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLFileBrowserController.h"
@class SZLFile;
@class SZCFilePreviewController;

#define kStoryboard_Previewers_both				@"previewers_Both"

@protocol SZCFilePreviewDelegate <NSObject>
@optional
- (void)filePreview:(SZCFilePreviewController*)previewer didSaveItemWithPath:(NSString *)inPath;
- (void)filePreview:(SZCFilePreviewController*)previewer didEditItemWithPath:(NSString *)inPath;
@end


@interface SZCFilePreviewController : UIViewController

@property (nonatomic, strong) SZLFile *file;

@property (nonatomic) BOOL canSave;
@property (nonatomic) BOOL canEdit;

@property (nonatomic, strong) id<SZCFilePreviewDelegate> delegate;

+ (SZCFilePreviewController *) createPreviewer:(NSString *)inFilePath name:(NSString *)inSaveFileName passPhrase:(NSString *)inPassPhrase;
- (void) previewFile;

@end

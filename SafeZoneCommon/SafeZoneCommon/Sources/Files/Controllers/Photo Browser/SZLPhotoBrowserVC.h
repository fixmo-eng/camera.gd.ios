//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//
//  Originally part of MWPhotoBrowser library, License: MIT

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class SZLPhotoBrowserVC;

@protocol SZLPhotoBrowserDelegate <NSObject>
@required
- (void)photoBrowser:(SZLPhotoBrowserVC*)photoBrowser didSelect:(NSArray*)photos;
- (void)photoBrowser:(SZLPhotoBrowserVC*)photoBrowser didDelete:(NSArray*)photos;
- (void)photoBrowser:(SZLPhotoBrowserVC*)photoBrowser didEmail:(NSArray*)photos fromBarButton:(UIBarButtonItem*)barButton;
- (void)photoBrowser:(SZLPhotoBrowserVC*)photoBrowser didTransfer:(NSArray*)photos fromBarButton:(UIBarButtonItem*)barButton;
- (void)photoBrowserDidRequestFiles:(SZLPhotoBrowserVC*)photoBrowser fromBarButton:(UIBarButtonItem*)barButton;

- (BOOL)canTransferPhotosWithCount:(NSInteger)count;
@end

@interface SZLPhotoBrowserVC : UIViewController <UIScrollViewDelegate, UIActionSheetDelegate>

@property (nonatomic, weak) IBOutlet id<SZLPhotoBrowserDelegate> delegate;

@property (nonatomic) BOOL canSelect;
@property (nonatomic) BOOL canDelete;
@property (nonatomic) BOOL canEmail;
@property (nonatomic) BOOL canImport;
@property (nonatomic) BOOL canTransfer;

@property (nonatomic) NSInteger maxItems;
@property (nonatomic) NSInteger maxItemSize;
@property (nonatomic) NSInteger maxTotalSize;

@property (nonatomic) BOOL zoomPhotosToFill;
@property (nonatomic) BOOL startOnGrid;
@property (nonatomic, readonly) NSUInteger currentIndex;

- (id)initWithDelegate:(id <SZLPhotoBrowserDelegate>)delegate;

// Set the file extension whitelist and blacklist.
- (void)setExtensionWhitelist:(NSSet*)whitelist extensionBlacklist:(NSSet*)blacklist;

// Reloads the photo browser and refetches data
- (void)reloadData;

// Set page that photo browser starts on
- (void)setCurrentPhotoIndex:(NSUInteger)index;

// Attempt to change photo selection in browser.
// Returns success. Presents error alert if fails.
- (BOOL)attemptToToggleSelectionAtIndex:(NSUInteger)index;

// Navigation
- (void)showNextPhotoAnimated:(BOOL)animated;
- (void)showPreviousPhotoAnimated:(BOOL)animated;

@end

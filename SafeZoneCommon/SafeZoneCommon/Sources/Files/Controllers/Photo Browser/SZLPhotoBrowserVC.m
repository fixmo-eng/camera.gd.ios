//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//
//  Originally part of MWPhotoBrowser library, License: MIT


#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

#import "SZLPhotoBrowserVC.h"
#import "SZLPhotoBrowserVC+Private.h"
#import "MWGridViewController.h"
#import "MWZoomingScrollView.h"
#import "SZLPhotoBrowser.h"
#import "SZLFileImage.h"

#import "PSPDFActionSheet.h"
#import "PSPDFAlertView.h"

#define Localized(key) \
[[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"SafeZoneCommonBundle" ofType:@"bundle"]] localizedStringForKey:(key) value:@"" table:@"photobrowser"]

#define PADDING                  10
#define ACTION_SHEET_OLD_ACTIONS 2000

// Declare private methods of browser
@interface SZLPhotoBrowserVC ()
{
	size_t selectedFileTotalSize;
}
    
// Data
@property (nonatomic) SZLPhotoBrowser *browser;

// Views
@property (nonatomic) UIScrollView *pagingScrollView;

// Paging & layout
@property (nonatomic) NSMutableSet *visiblePages, *recycledPages;
@property (nonatomic) NSUInteger currentPageIndex;
@property (nonatomic) NSUInteger previousPageIndex;
@property (nonatomic) CGRect previousLayoutBounds;
@property (nonatomic) NSUInteger pageIndexBeforeRotation;

// Navigation & controls
@property (nonatomic) UIBarButtonItem *previousButton, *nextButton, *gridButton;
@property (nonatomic) UIBarButtonItem *doneButton, *deleteButton, *emailButton, *importButton;
@property (nonatomic) UIActionSheet *actionSheet;

// Grid
@property (nonatomic) MWGridViewController *gridController;
@property (nonatomic) UIBarButtonItem *gridPreviousLeftNavItem;
@property (nonatomic) UIBarButtonItem *gridPreviousRightNavItem;

// Misc
@property (nonatomic) BOOL performingLayout;
@property (nonatomic) BOOL rotating;
@property (nonatomic) BOOL viewIsActive; // active as in it's in the view heirarchy
@property (nonatomic) BOOL didSavePreviousStateOfNavBar;
@property (nonatomic) BOOL skipNextPagingScrollViewPositioning;
@property (nonatomic) BOOL viewHasAppearedInitially;
@property (nonatomic) BOOL navPopGestureRecognizerPrevEnabled;
@property (nonatomic) CGPoint currentGridContentOffset;

// Properties
@property (nonatomic) UIActivityViewController *activityViewController;

@end


@implementation SZLPhotoBrowserVC

#pragma mark - Init

- (id)init
{
    if ((self = [super init])) {
        [self _initialisation];
    }
    return self;
}

- (id)initWithDelegate:(id <SZLPhotoBrowserDelegate>)delegate
{
    if ((self = [self init])) {
        _delegate = delegate;
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super initWithCoder:decoder])) {
        [self _initialisation];
	}
	return self;
}

- (void)_initialisation
{

    // Defaults
    _previousLayoutBounds = CGRectZero;
    _currentPageIndex = 0;
    _previousPageIndex = NSUIntegerMax;
    _zoomPhotosToFill = YES;
    _performingLayout = NO; // Reset on view did appear
    _rotating = NO;
    _viewIsActive = NO;
    _startOnGrid = YES;
    _visiblePages = [[NSMutableSet alloc] init];
    _recycledPages = [[NSMutableSet alloc] init];
    _canDelete = NO;
    _canEmail = NO;
    _canSelect = NO;
    
    _currentGridContentOffset = CGPointMake(0, CGFLOAT_MAX);
    _didSavePreviousStateOfNavBar = NO;
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]){
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    // Listen for MWPhoto notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleMWPhotoLoadingDidEndNotification:)
                                                 name:MWPHOTO_LOADING_DID_END_NOTIFICATION
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground:) name:kDidEnterBackground object:nil];
}

- (void)dealloc
{
    _pagingScrollView.delegate = nil;
    _browser = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setExtensionWhitelist:(NSSet*)whitelist extensionBlacklist:(NSSet*)blacklist
{
	if (whitelist)
	{
		self.browser.extensionWhitelist = whitelist;
	}
	self.browser.extensionBlacklist = blacklist;
	
	[self.browser reloadData];
}

- (void)didReceiveMemoryWarning
{

	// Release any cached data, images, etc that aren't in use.
    [self.browser releaseAllUnderlyingPhotos];
	[_recycledPages removeAllObjects];

	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

}

#pragma mark - View Loading

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// View
	self.view.backgroundColor = [UIColor blackColor];
    self.view.clipsToBounds = YES;

	// Setup paging scrolling view
	CGRect pagingScrollViewFrame = [self frameForPagingScrollView];
	_pagingScrollView = [[UIScrollView alloc] initWithFrame:pagingScrollViewFrame];
	_pagingScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_pagingScrollView.pagingEnabled = YES;
	_pagingScrollView.delegate = self;
	_pagingScrollView.showsHorizontalScrollIndicator = NO;
	_pagingScrollView.showsVerticalScrollIndicator = NO;
	_pagingScrollView.backgroundColor = [UIColor blackColor];
    _pagingScrollView.contentSize = [self contentSizeForPagingScrollView];
	[self.view addSubview:_pagingScrollView];
	
	if (self.canImport)
	{
		self.importButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(importImages:)];
		self.navigationItem.rightBarButtonItem = self.importButton;
	}
}

- (void)performLayout
{
    // Setup
    _performingLayout = YES;
    
	// Setup pages
    [_visiblePages removeAllObjects];
    [_recycledPages removeAllObjects];

    // Update nav
	[self refreshButtons];
    [self refreshTitle];

    // Content offset
	_pagingScrollView.contentOffset = [self contentOffsetForPageAtIndex:_currentPageIndex];
    [self tilePages];
    _performingLayout = NO;

}

// Release any retained subviews of the main view.
- (void)viewDidUnload
{
	_currentPageIndex = 0;
    _pagingScrollView = nil;
    _visiblePages = nil;
    _recycledPages = nil;
    _previousButton = nil;
    _nextButton = nil;
    [super viewDidUnload];
}

- (BOOL)presentingViewControllerPrefersStatusBarHidden
{
    UIViewController *presenting = self.presentingViewController;
    if (presenting) {
        if ([presenting isKindOfClass:[UINavigationController class]]) {
            presenting = [(UINavigationController *)presenting topViewController];
        }
    } else {
        // We're in a navigation controller so get previous one!
        if (self.navigationController && self.navigationController.viewControllers.count > 1) {
            presenting = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
        }
    }
    if (presenting) {
        return [presenting prefersStatusBarHidden];
    } else {
        return NO;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
	// Super
	[super viewWillAppear:animated];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self setupNavigationButtons];

    // Navigation Controller State
    self.navPopGestureRecognizerPrevEnabled = self.navigationController.interactivePopGestureRecognizer.enabled;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;

    // Initial appearance
    if (!_viewHasAppearedInitially) {
        
        // Update
        [self reloadData];
        
        if (_startOnGrid) {
            [self showGrid:NO];
        }
        _viewHasAppearedInitially = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    // Check that we're being popped for good
    if ([self.navigationController.viewControllers objectAtIndex:0] != self &&
        ![self.navigationController.viewControllers containsObject:self]) {
        
        // State
        _viewIsActive = NO;
    }
    
    // Controls
    [self.navigationController.navigationBar.layer removeAllAnimations]; // Stop all animations on nav bar
    [NSObject cancelPreviousPerformRequestsWithTarget:self]; // Cancel any pending toggles from taps

    // Restore Navigation Controller State
    self.navigationController.interactivePopGestureRecognizer.enabled = self.navPopGestureRecognizerPrevEnabled;
    
  	// Super
	[super viewWillDisappear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setupToolbarButtons:animated];
	
	// This is needed because the status bar flip flops and the view sits under the nav bar
	// in the future, need to go through it all and change it to use edgesforextended...etc
	[self.gridController.view setNeedsLayout];
	
    _viewIsActive = YES;
}

#pragma mark - Appearance

- (void)setupNavigationButtons
{
    // If using photo browser for selection, show done button
    if (self.canSelect) {
        self.doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                  target:self
                                                                  action:@selector(doneButtonPressed:)];
        self.navigationItem.rightBarButtonItem = self.doneButton;
		self.navigationItem.hidesBackButton = YES;
    }
}

- (void)setupToolbarButtons:(BOOL)animated
{
    // No buttons if there's no photos
    if ([self numberOfPhotos] == 0) {
        [self.navigationController setToolbarHidden:YES animated:animated];
        return;
    }
    
    // Toolbar Items
    if (!_deleteButton)
        _deleteButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                                                      target:self
                                                                      action:@selector(deleteButtonPressed:)];
    
    if (!_previousButton)
        _previousButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"SafeZoneCommonBundle.bundle/UIBarButtonItemArrowLeft"]
                                                           style:UIBarButtonItemStylePlain
                                                          target:self
                                                          action:@selector(gotoPreviousPage)];
    
    if (!_nextButton)
        _nextButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"SafeZoneCommonBundle.bundle/UIBarButtonItemArrowRight"]
                                                       style:UIBarButtonItemStylePlain
                                                      target:self
                                                      action:@selector(gotoNextPage)];
    
    if (!_gridButton)
        _gridButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"SafeZoneCommonBundle.bundle/UIBarButtonItemGrid"]
                                                       style:UIBarButtonItemStylePlain
                                                      target:self
                                                      action:@selector(showGridAnimated)];
    if (!_emailButton)
        _emailButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                     target:self
                                                                     action:@selector(emailButtonPressed:)];
    
    // Toolbar filler items
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
    fixedSpace.width = 32; // To balance action button
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    // Left
    if (self.canDelete) {
        [items addObject:_deleteButton];
    } else {
        [items addObject:fixedSpace];
    }
    
    if (!_gridController) {
        // Middle - Nav
        [items addObject:flexSpace];
        [items addObject:_previousButton]; // Previous Button
        [items addObject:flexSpace];
        [items addObject:_gridButton]; // Grid
        [items addObject:flexSpace];
        [items addObject:_nextButton]; // Next Button
        [items addObject:flexSpace];
    } else {
        [items addObject:flexSpace];
    }
    
    // Right
    if (self.canEmail || (self.canTransfer && ([self.delegate canTransferPhotosWithCount:1] || [self.delegate canTransferPhotosWithCount:2])))
	{
        [items addObject:_emailButton];
    } else {
        [items addObject:fixedSpace];
    }
    
    [self.navigationController.toolbar setItems:[items copy] animated:NO];
    
    // Should hide or display the toolbar based on whether it's empty
    BOOL hideToolbar = YES;
    for (UIBarButtonItem* item in items) {
        if (item != fixedSpace && item != flexSpace) {
            hideToolbar = NO;
            break;
        }
    }
    
    [self.navigationController setToolbarHidden:hideToolbar animated:animated];
}

- (void)refreshButtons
{
    // Buttons Enabled
    
    // Grid view, enabled if there's a selection
    if (self.gridController)
	{
		self.importButton.enabled = YES;
        self.gridButton.enabled = NO;
        self.emailButton.enabled = ([self.browser.selection count] > 0);
        self.deleteButton.enabled = ([self.browser.selection count] > 0);
        self.doneButton.enabled = ([self.browser.selection count] > 0) || self.canSelect;
    }
    // Photo zoomed view, enable if has photos and setup left and right buttons
    else {
		self.importButton.enabled = NO;
        self.gridButton.enabled = YES;
        self.emailButton.enabled = ([self numberOfPhotos] > 0);
        self.deleteButton.enabled = ([self numberOfPhotos] > 0);
        self.previousButton.enabled =  (_currentPageIndex > 0);
        self.nextButton.enabled = (_currentPageIndex < [self numberOfPhotos] - 1);
        self.doneButton.enabled = ([self numberOfPhotos] > 0) || self.canSelect;
    }
}

- (void)refreshTitle
{
    // Title should be different depending if seeing grid in selection mode,
    // or currently zoomed into the photo
    NSUInteger numberOfPhotos = [self numberOfPhotos];
    if (_gridController) {
        if (_gridController.selectionMode) {
            self.title = Localized(@"Select Photos");
        } else {
            NSString *photosText;
            if (numberOfPhotos == 1) {
                photosText = Localized(@"photo");
            } else {
                photosText = Localized(@"photos");
            }
            self.title = [NSString stringWithFormat:@"%lu %@", (unsigned long)numberOfPhotos, photosText];
        }
    } else if (numberOfPhotos > 0) {
		self.title = [NSString stringWithFormat:Localized(@"%lu of %lu"), (unsigned long)(_currentPageIndex+1), (unsigned long)numberOfPhotos];
	} else {
        self.title = Localized(@"No Photos");
	}
}

#pragma mark - Layout

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self layoutVisiblePages];
}

- (void)layoutVisiblePages
{
	// Flag
	_performingLayout = YES;

    // Remember index
	NSUInteger indexPriorToLayout = _currentPageIndex;

	// Get paging scroll view frame to determine if anything needs changing
	CGRect pagingScrollViewFrame = [self frameForPagingScrollView];

	// Frame needs changing
    if (!_skipNextPagingScrollViewPositioning) {
        _pagingScrollView.frame = pagingScrollViewFrame;
    }
    _skipNextPagingScrollViewPositioning = NO;

	// Recalculate contentSize based on current orientation
	_pagingScrollView.contentSize = [self contentSizeForPagingScrollView];

	// Adjust frames and configuration of each visible page
	for (MWZoomingScrollView *page in _visiblePages) {
        NSUInteger index = page.index;
		page.frame = [self frameForPageAtIndex:index];
        
        if (page.selectedButton) {
            page.selectedButton.frame = [self frameForSelectedButton:page.selectedButton atIndex:index];
        }

        // Adjust scales if bounds has changed since last time
        if (!CGRectEqualToRect(_previousLayoutBounds, self.view.bounds)) {
            // Update zooms for new bounds
            [page setMaxMinZoomScalesForCurrentBounds];
            _previousLayoutBounds = self.view.bounds;
        }

	}

	// Adjust contentOffset to preserve page location based on values collected prior to location
	_pagingScrollView.contentOffset = [self contentOffsetForPageAtIndex:indexPriorToLayout];
	[self didStartViewingPageAtIndex:_currentPageIndex]; // initial

	// Reset
	_currentPageIndex = indexPriorToLayout;
	_performingLayout = NO;

}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	// Remember page index before rotation
	_pageIndexBeforeRotation = _currentPageIndex;
	_rotating = YES;

}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	// Perform layout
	_currentPageIndex = _pageIndexBeforeRotation;

    // Layout
    [self layoutVisiblePages];

}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	_rotating = NO;
}

#pragma mark - Data

- (NSUInteger)currentIndex
{
    return _currentPageIndex;
}

- (void)reloadData
{
    [self.browser reloadData];

    // Remove everything
    while (_pagingScrollView.subviews.count) {
        [[_pagingScrollView.subviews lastObject] removeFromSuperview];
    }

    // Update current page index
    _currentPageIndex = MAX(0, MIN(_currentPageIndex, [self numberOfPhotos] - 1));
    
    if (_gridController) {
        [_gridController.collectionView reloadData];
    }

    // Update
    [self performLayout];
    
    // Make sure to refresh the toolbar, incase or state changed (no photos)
    [self setupToolbarButtons:YES];
    
    // Layout
    [self.view setNeedsLayout];
}

- (NSUInteger)numberOfPhotos
{
    if (self.browser)
        return [self.browser.files count];
    
    return 0;
}

- (id<MWPhoto>)photoAtIndex:(NSUInteger)index
{
    id file = self.browser.files[index];
    id<MWPhoto> photo = [file conformsToProtocol:@protocol(MWPhoto)] ? file : nil;
    return photo;
}

- (id<MWPhoto>)thumbPhotoAtIndex:(NSUInteger)index
{
    id file = self.browser.files[index];
    SZLFileImage* photo = [file isKindOfClass:[SZLFileImage class]] ? (SZLFileImage*)file : nil;
    return photo.thumbnail;
}

- (BOOL)photoIsSelectedAtIndex:(NSUInteger)index
{
    return [self.browser.selection containsIndex:index];
}

- (BOOL)canSelectPhotoAtIndex:(NSUInteger)index error:(NSError**)error
{
	if (self.maxItems > 0)
	{
		if (self.browser.selection.count >= self.maxItems)
		{
			if (error)
			{
				*error = [NSError errorWithDomain:@"SZLPhotoBrowserVC" code:1 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:SecureCamLocalizedStringFromTable(@"A maximum of %d items can be selected.", @"securecam", @"Error format string for exceeding maxItems"), self.maxItems]}];
			}
			return NO;
		}
	}
	
	if (self.maxItemSize > 0)
	{
		SZLFileImage* photo = self.browser.files[index];
		NSInteger fileSize = photo.size;
		
		if (fileSize > self.maxItemSize)
		{
			if (error)
			{
				*error = [NSError errorWithDomain:@"SZLPhotoBrowserVC" code:2 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:SecureCamLocalizedStringFromTable(@"Selected photos must be at most %d bytes.", @"securecam", @"Error format string for exceeding maxItemSize"), self.maxItemSize]}];
			}
			return NO;
		}
	}
	
	if (self.maxTotalSize > 0)
	{
		SZLFileImage* photo = self.browser.files[index];
		NSInteger fileSize = photo.size + selectedFileTotalSize;
		
		if (fileSize > self.maxTotalSize)
		{
			if (error)
			{
				*error = [NSError errorWithDomain:@"SZLPhotoBrowserVC" code:3 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:SecureCamLocalizedStringFromTable(@"The sum of all selected photos must be at most %d bytes.", @"securecam", @"Error format string for exceeding maxTotalSize"), self.maxTotalSize]}];
			}
			return NO;
		}
	}
	
	return YES;
}

- (void)setPhotoSelected:(BOOL)selected atIndex:(NSUInteger)index
{
    SZLFileImage* photo = self.browser.files[index];
	
	if (selected)
	{
		[self.browser.selection addIndex:index];
		selectedFileTotalSize += photo.size;
	}
	else
	{
		[self.browser.selection removeIndex:index];
		selectedFileTotalSize -= photo.size;
	}
    
    // Update nav
	[self refreshButtons];
}

- (UIImage *)imageForPhoto:(id<MWPhoto>)photo
{
	if (photo) {
		// Get image or obtain in background
		if ([photo underlyingImage]) {
			return [photo underlyingImage];
		} else {
            [photo loadUnderlyingImageAndNotify];
		}
	}
	return nil;
}

- (void)loadAdjacentPhotosIfNecessary:(id<MWPhoto>)photo
{
    MWZoomingScrollView *page = [self pageDisplayingPhoto:photo];
    if (page) {
        // If page is current page then initiate loading of previous and next pages
        NSUInteger pageIndex = page.index;
        if (_currentPageIndex == pageIndex) {
            if (pageIndex > 0) {
                // Preload index - 1
                id <MWPhoto> photo = [self photoAtIndex:pageIndex-1];
                if (![photo underlyingImage]) {
                    [photo loadUnderlyingImageAndNotify];
                }
            }
            if (pageIndex < [self numberOfPhotos] - 1) {
                // Preload index + 1
                id <MWPhoto> photo = [self photoAtIndex:pageIndex+1];
                if (![photo underlyingImage]) {
                    [photo loadUnderlyingImageAndNotify];
                }
            }
        }
    }
}

#pragma mark - App LifeCycle Notification
- (void)didEnterBackground:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:kDidEnterBackground]) {
        [self closeActionSheet];
    }
}

#pragma mark - MWPhoto Loading Notification

- (void)handleMWPhotoLoadingDidEndNotification:(NSNotification *)notification
{
    id <MWPhoto> photo = [notification object];
    MWZoomingScrollView *page = [self pageDisplayingPhoto:photo];
    if (page) {
        if ([photo underlyingImage]) {
            // Successful load
            [page displayImage];
            [self loadAdjacentPhotosIfNecessary:photo];
        } else {
            // Failed to load
            [page displayImageFailure];
        }
    }
}

#pragma mark - Paging

- (void)tilePages
{
    if ([self numberOfPhotos] == 0) return;
    
	// Calculate which pages should be visible
	// Ignore padding as paging bounces encroach on that
	// and lead to false page loads
	CGRect visibleBounds = _pagingScrollView.bounds;
	NSInteger iFirstIndex = (NSInteger)floorf((CGRectGetMinX(visibleBounds)+PADDING*2) / CGRectGetWidth(visibleBounds));
	NSInteger iLastIndex  = (NSInteger)floorf((CGRectGetMaxX(visibleBounds)-PADDING*2-1) / CGRectGetWidth(visibleBounds));
    if (iFirstIndex < 0) iFirstIndex = 0;
    if (iFirstIndex > [self numberOfPhotos] - 1) iFirstIndex = [self numberOfPhotos] - 1;
    if (iLastIndex < 0) iLastIndex = 0;
    if (iLastIndex > [self numberOfPhotos] - 1) iLastIndex = [self numberOfPhotos] - 1;

	// Recycle no longer needed pages
    NSInteger pageIndex;
	for (MWZoomingScrollView *page in _visiblePages) {
        pageIndex = page.index;
		if (pageIndex < (NSUInteger)iFirstIndex || pageIndex > (NSUInteger)iLastIndex) {
			[_recycledPages addObject:page];
            [page.selectedButton removeFromSuperview];
            [page prepareForReuse];
			[page removeFromSuperview];
		}
	}
	[_visiblePages minusSet:_recycledPages];
    while (_recycledPages.count > 2) // Only keep 2 recycled pages
        [_recycledPages removeObject:[_recycledPages anyObject]];

	// Add missing pages
	for (NSUInteger index = (NSUInteger)iFirstIndex; index <= (NSUInteger)iLastIndex; index++) {
		if (![self isDisplayingPageForIndex:index]) {

            // Add new page
			MWZoomingScrollView *page = [self dequeueRecycledPage];
			if (!page) {
				page = [[MWZoomingScrollView alloc] initWithPhotoBrowser:self];
			}
			[_visiblePages addObject:page];
			[self configurePage:page forIndex:index];

			[_pagingScrollView addSubview:page];

            // Add selected button
            UIButton *selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [selectedButton setImage:[UIImage imageNamed:@"SafeZoneCommonBundle.bundle/ImageSelectedOff.png"] forState:UIControlStateNormal];
            [selectedButton setImage:[UIImage imageNamed:@"SafeZoneCommonBundle.bundle/ImageSelectedOn.png"] forState:UIControlStateSelected];
            [selectedButton sizeToFit];
            selectedButton.adjustsImageWhenHighlighted = NO;
            [selectedButton addTarget:self action:@selector(selectedButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            selectedButton.frame = [self frameForSelectedButton:selectedButton atIndex:index];
            [_pagingScrollView addSubview:selectedButton];
            page.selectedButton = selectedButton;
            selectedButton.selected = [self photoIsSelectedAtIndex:index];
		}
	}
}

- (void)updateVisiblePageStates
{
    NSSet *copy = [_visiblePages copy];
    for (MWZoomingScrollView *page in copy) {
        // Update selection
        page.selectedButton.selected = [self photoIsSelectedAtIndex:page.index];
    }
}

- (BOOL)isDisplayingPageForIndex:(NSUInteger)index
{
	for (MWZoomingScrollView *page in _visiblePages) {
		if (page.index == index) {
            return YES;
        }
    }
	return NO;
}

- (MWZoomingScrollView *)pageDisplayedAtIndex:(NSUInteger)index
{
	MWZoomingScrollView *thePage = nil;
	for (MWZoomingScrollView *page in _visiblePages) {
		if (page.index == index) {
			thePage = page; break;
		}
	}
	return thePage;
}

- (MWZoomingScrollView *)pageDisplayingPhoto:(id<MWPhoto>)photo
{
	MWZoomingScrollView *thePage = nil;
	for (MWZoomingScrollView *page in _visiblePages) {
		if (page.photo == photo) {
			thePage = page; break;
		}
	}
	return thePage;
}

- (void)configurePage:(MWZoomingScrollView *)page forIndex:(NSUInteger)index
{
	page.frame = [self frameForPageAtIndex:index];
    page.index = index;
    page.photo = [self photoAtIndex:index];
}

- (MWZoomingScrollView *)dequeueRecycledPage
{
	MWZoomingScrollView *page = [_recycledPages anyObject];
	if (page) {
		[_recycledPages removeObject:page];
	}
	return page;
}

// Handle page changes
- (void)didStartViewingPageAtIndex:(NSUInteger)index
{
    if ([self numberOfPhotos] == 0) {
        return;
    }

    // Release images further away than +/-1
    NSUInteger i;
    if (index > 0) {
        // Release anything < index - 1
        for (i = 0; i < index-1; i++) {
            [self.browser.files[i] unloadUnderlyingImage];
        }
    }
    
    if (index < [self numberOfPhotos] - 1) {
        // Release anything > index + 1
        for (i = index + 2; i < [self numberOfPhotos]; i++) {
            [self.browser.files[i] unloadUnderlyingImage];
        }
    }

    // Load adjacent images if needed and the photo is already
    // loaded. Also called after photo has been loaded in background
    id <MWPhoto> currentPhoto = [self photoAtIndex:index];
    if ([currentPhoto underlyingImage]) {
        // photo loaded so load ajacent now
        [self loadAdjacentPhotosIfNecessary:currentPhoto];
    }

    _previousPageIndex = index;
    
    [self setupToolbarButtons:YES];
    [self refreshTitle];
    [self refreshButtons];
}

#pragma mark - Frame Calculations

- (CGRect)frameForPagingScrollView
{
    CGRect frame = self.view.bounds;// [[UIScreen mainScreen] bounds];
    frame.origin.x += PADDING;
    frame.size.width -= (2 * PADDING);
    return CGRectIntegral(frame);
}

- (CGRect)frameForPageAtIndex:(NSUInteger)index
{
    // We have to use our paging scroll view's bounds, not frame, to calculate the page placement. When the device is in
    // landscape orientation, the frame will still be in portrait because the pagingScrollView is the root view controller's
    // view, so its frame is in window coordinate space, which is never rotated. Its bounds, however, will be in landscape
    // because it has a rotation transform applied.
    CGRect bounds = _pagingScrollView.bounds;
    CGRect pageFrame = bounds;
    pageFrame.size.width -= (2 * PADDING);
    pageFrame.origin.x = (bounds.size.width * index) + PADDING;
    return CGRectIntegral(pageFrame);
}

- (CGSize)contentSizeForPagingScrollView
{
    // We have to use the paging scroll view's bounds to calculate the contentSize, for the same reason outlined above.
    CGRect bounds = _pagingScrollView.bounds;
    NSInteger pageNum = [self numberOfPhotos] > 0 ? [self numberOfPhotos] : 1;
    return CGSizeMake(bounds.size.width * pageNum, bounds.size.height);
}

- (CGPoint)contentOffsetForPageAtIndex:(NSUInteger)index
{
	CGFloat pageWidth = _pagingScrollView.bounds.size.width;
	CGFloat newOffset = index * pageWidth;
	return CGPointMake(newOffset, 0);
}

- (CGRect)frameForSelectedButton:(UIButton *)selectedButton atIndex:(NSUInteger)index
{
    CGRect pageFrame = [self frameForPageAtIndex:index];
    CGFloat yOffset = 0;
    UINavigationBar *navBar = self.navigationController.navigationBar;
    yOffset = navBar.frame.origin.y + navBar.frame.size.height;
    
    CGRect frame = CGRectMake(pageFrame.origin.x + pageFrame.size.width - 20 - selectedButton.frame.size.width,
                                     20 + yOffset,
                                     selectedButton.frame.size.width,
                                     selectedButton.frame.size.height);
    return CGRectIntegral(frame);
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // Checks
	if (!_viewIsActive || _performingLayout || _rotating) return;

	// Tile pages
	[self tilePages];

	// Calculate current page
	CGRect visibleBounds = _pagingScrollView.bounds;
	NSInteger index = (NSInteger)(floorf(CGRectGetMidX(visibleBounds) / CGRectGetWidth(visibleBounds)));
    if (index < 0) index = 0;
	if (index > [self numberOfPhotos] - 1) index = [self numberOfPhotos] - 1;
	NSUInteger previousCurrentPage = _currentPageIndex;
	_currentPageIndex = index;
	if (_currentPageIndex != previousCurrentPage) {
        [self didStartViewingPageAtIndex:index];
    }

}

#pragma mark - Navigation

- (void)jumpToPageAtIndex:(NSUInteger)index animated:(BOOL)animated
{
	// Change page
	if (index < [self numberOfPhotos]) {
		CGRect pageFrame = [self frameForPageAtIndex:index];
        [_pagingScrollView setContentOffset:CGPointMake(pageFrame.origin.x - PADDING, 0) animated:animated];
        
        // Update nav
        [self refreshButtons];
        [self refreshTitle];
	}
}

- (void)gotoPreviousPage
{
    [self showPreviousPhotoAnimated:NO];
}
- (void)gotoNextPage
{
    [self showNextPhotoAnimated:NO];
}

- (void)showPreviousPhotoAnimated:(BOOL)animated
{
    [self jumpToPageAtIndex:_currentPageIndex-1 animated:animated];
}

- (void)showNextPhotoAnimated:(BOOL)animated
{
    [self jumpToPageAtIndex:_currentPageIndex+1 animated:animated];
}

#pragma mark - Interactions

- (void)selectedButtonTapped:(id)sender
{
	UIButton *selectedButton = (UIButton *)sender;
	NSUInteger index = NSUIntegerMax;
	for (MWZoomingScrollView *page in _visiblePages)
	{
		if (page.selectedButton == selectedButton)
		{
			index = page.index;
			break;
		}
	}
	
	if ([self attemptToToggleSelectionAtIndex:index])
	{
		selectedButton.selected = !selectedButton.selected;
	}
}

- (BOOL)attemptToToggleSelectionAtIndex:(NSUInteger)index
{
	BOOL canUpdateSelection = YES;
	NSError* error = nil;
	
	if (![self photoIsSelectedAtIndex:index] && self.canSelect)
	{
		canUpdateSelection = [self canSelectPhotoAtIndex:index error:&error];
	}
	
	if (canUpdateSelection)
	{
		[self setPhotoSelected:![self photoIsSelectedAtIndex:index] atIndex:index];
	}
	else
	{
		if (error)
		{
			UIAlertView *av = [[UIAlertView alloc] initWithTitle: SecureCamLocalizedStringFromTable(@"Cannot select photo", @"securecam", @"Cannot select photo") message:[error localizedDescription] delegate:nil cancelButtonTitle:SecureCamLocalizedStringFromTable(@"OK", @"securecam", @"OK") otherButtonTitles:nil];
			[av show];
		}
	}
	
	return canUpdateSelection;
}

#pragma mark - Grid

- (void)showGridAnimated
{
    [self showGrid:YES];
}

- (void)showGrid:(BOOL)animated
{
    if (_gridController) return;

    // Init grid controller
    _gridController = [[MWGridViewController alloc] init];
    _gridController.initialContentOffset = _currentGridContentOffset;
    _gridController.browser = self;
    _gridController.selectionMode = YES;
    _gridController.view.frame = self.view.bounds;
    _gridController.view.frame = CGRectOffset(_gridController.view.frame, 0, self.view.bounds.size.height);

    // Stop specific layout being triggered
    _skipNextPagingScrollViewPositioning = YES;

    // Add as a child view controller
    [self addChildViewController:_gridController];
    [self.view addSubview:_gridController.view];

    // Animate grid in and photo scroller out
    [UIView animateWithDuration:animated ? 0.3 : 0 animations:^(void) {
        _gridController.view.frame = self.view.bounds;
        CGRect newPagingFrame = [self frameForPagingScrollView];
        newPagingFrame = CGRectOffset(newPagingFrame, 0, -newPagingFrame.size.height);
        _pagingScrollView.frame = newPagingFrame;
    } completion:^(BOOL finished) {
        [_gridController didMoveToParentViewController:self];
        // Update nav
        [self setupToolbarButtons:animated];
        [self refreshButtons];
        [self refreshTitle];
    }];
}

- (void)hideGrid
{
    if (!_gridController) return;

    // Remember previous content offset
    _currentGridContentOffset = _gridController.collectionView.contentOffset;

    // Position prior to hide animation
    CGRect newPagingFrame = [self frameForPagingScrollView];
    newPagingFrame = CGRectOffset(newPagingFrame, 0, -newPagingFrame.size.height);
    _pagingScrollView.frame = newPagingFrame;

    // Remember and remove controller now so things can detect a nil grid controller
    MWGridViewController *tmpGridController = _gridController;
    _gridController = nil;

    // Update
    [self updateVisiblePageStates];

    // Animate, hide grid and show paging scroll view
    [UIView animateWithDuration:0.3 animations:^{
        tmpGridController.view.frame = CGRectOffset(self.view.bounds, 0, self.view.bounds.size.height);
        _pagingScrollView.frame = [self frameForPagingScrollView];
    } completion:^(BOOL finished) {
        [tmpGridController willMoveToParentViewController:nil];
        [tmpGridController.view removeFromSuperview];
        [tmpGridController removeFromParentViewController];
        // Update nav
        [self setupToolbarButtons:YES];
        [self refreshButtons];
        [self refreshTitle];
    }];
}

#pragma mark - Properties

// Handle depreciated method
- (void)setInitialPageIndex:(NSUInteger)index
{
    [self setCurrentPhotoIndex:index];
}

- (void)setCurrentPhotoIndex:(NSUInteger)index
{
    // Validate
    if (index > 0 && index >= [self numberOfPhotos])
        index = [self numberOfPhotos]-1;
    _currentPageIndex = index;
	if ([self isViewLoaded]) {
        [self jumpToPageAtIndex:index animated:NO];
        if (!_viewIsActive)
            [self tilePages]; // Force tiling if view is not visible
    }
}

- (SZLPhotoBrowser*)browser
{
    if (!_browser) {
        _browser = [[SZLPhotoBrowser alloc] init];
    }
    
    return _browser;
}

#pragma mark - Actions

- (void)closeActionSheet
{
    if (self.actionSheet) {
        [self.actionSheet dismissWithClickedButtonIndex:self.actionSheet.cancelButtonIndex animated:NO];
    }
}

- (void)doneButtonPressed:(id)sender
{
	[self closeActionSheet];
	NSArray* selectedPhotos = [self.browser.files objectsAtIndexes:self.browser.selection];

	[self.navigationController popViewControllerAnimated:NO];
	
	// Do this on the next event cycle to allow VC stack to be updated fully.
	dispatch_async(dispatch_get_main_queue(),
	^{
		[self.delegate photoBrowser:self didSelect:selectedPhotos];
	});
}

- (void)importImages:(id)sender
{
	[self.delegate photoBrowserDidRequestFiles:self fromBarButton:self.importButton];
}

- (void)emailButtonPressed:(id)sender
{
    [self closeActionSheet];
    NSString *title;
    NSArray* selectedPhotos = [self.browser.files objectsAtIndexes:self.browser.selection];
    
    // Delegate if there are selected photos only
    if (self.gridController && [selectedPhotos count] == 0) return;
    
    // I'm not in a grid view and nothing is selected, use current
    if ([selectedPhotos count] == 0) {
        title = Localized(@"Action for current photo");
        selectedPhotos = @[self.browser.files[self.currentIndex]];
    } else {
        title = [NSString stringWithFormat:Localized(@"Action for selected photos (%d)"), [selectedPhotos count]];
    }
    
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:title];
	
	if (self.canEmail)
	{
		[actionSheet addButtonWithTitle:Localized(@"Email") block:
		^{
			[self.delegate photoBrowser:self didEmail:selectedPhotos fromBarButton:self.emailButton];
		}];
	}
	if ([self.delegate canTransferPhotosWithCount:[selectedPhotos count]])
	{
		[actionSheet addButtonWithTitle:Localized(@"Transfer") block:
		^{
			[self.delegate photoBrowser:self didTransfer:selectedPhotos fromBarButton:self.emailButton];
		}];
	}
	
    [actionSheet setCancelButtonWithTitle:Localized(@"Cancel") block:nil];
    self.actionSheet = actionSheet;
    [actionSheet showFromBarButtonItem:(UIBarButtonItem*)sender animated:YES];
}

- (void)deleteButtonPressed:(id)sender
{
    [self closeActionSheet];
    NSString *title;
    NSArray* selectedPhotos = [self.browser.files objectsAtIndexes:self.browser.selection];
    
    // Delegate if there are selected photos only
    if (self.gridController && [selectedPhotos count] == 0) return;
    
    // I'm not in a grid view and nothing is selected, use current
    if ([selectedPhotos count] == 0) {
        title = Localized(@"Are you sure you would like to delete the current photo");
        selectedPhotos = @[self.browser.files[self.currentIndex]];
    } else {
        title = [NSString stringWithFormat:Localized(@"Are you sure you would like to delete the selected photos (%d)"), ([selectedPhotos count])];
    }
    
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:title];
    [actionSheet setDestructiveButtonWithTitle:Localized(@"Delete") block:^{
        if ([self.delegate respondsToSelector:@selector(photoBrowser:didDelete:)]) {
            [self.delegate photoBrowser:self didDelete:selectedPhotos];
            [self reloadData];
        }
    }];
    
    [actionSheet setCancelButtonWithTitle:Localized(@"Cancel") block:nil];
    self.actionSheet = actionSheet;
    [actionSheet showFromBarButtonItem:(UIBarButtonItem*)sender animated:YES];
}

@end

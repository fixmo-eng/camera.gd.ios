//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLPhotoBrowser.h"
#import "GoodSecureFileManager.h"
#import "SZLFileImage.h"

#import "PSPDFActionSheet.h"
#import "PSPDFAlertView.h"

#define Localized(key) \
[[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"SafeZoneCommonBundle" ofType:@"bundle"]] localizedStringForKey:(key) value:@"" table:@"photobrowser"]

@implementation SZLPhotoBrowser

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSString* photosFolder = [[[SZLFileImage alloc] init] defaultDirectory];
        NSString* documentsFolder = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        self.folders = @[photosFolder, documentsFolder];
        
        NSSet* imageExtensions = [NSSet setWithArray:[[[SZLFileImage class] supportedExtensions] allKeys]];
        self.extensionWhitelist = imageExtensions;
		self.extensionBlacklist = nil;
    }
    return self;
}

- (void)releaseAllUnderlyingPhotos
{
    // Create a copy in case this array is modified while we are looping through
    // Release photos
    NSArray *files = [self.files copy];
    for (id file in files) {
        SZLFileImage *imageFile = [file isKindOfClass: [SZLFileImage class]] ? (SZLFileImage *)file : nil;
        if (imageFile) {
            [imageFile.thumbnail unloadUnderlyingImage];
            [imageFile unloadUnderlyingImage];
        }
    }
}

@end

//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import <UIKit/UIKit.h>
@class SZLFileBrowserController;
@class SZLFile;

@protocol SZLFileBrowserDelegate <NSObject>
@optional
- (void)fileBrowserDidCancel:(SZLFileBrowserController*)filebrowser;
- (void)fileBrowserDidCreateItem:(SZLFileBrowserController*)filebrowser;
- (void)fileBrowser:(SZLFileBrowserController*)filebrowser didSelectFiles:(NSArray *)files;
- (void)fileBrowser:(SZLFileBrowserController*)filebrowser didEditFile:(SZLFile *)file;
- (void)fileBrowser:(SZLFileBrowserController*)filebrowser didDeleteFiles:(NSArray *)files;
- (void)fileBrowser:(SZLFileBrowserController*)filebrowser didEmailFiles:(NSArray *)files;
@end

@interface SZLFileBrowserController : UITableViewController <UISearchBarDelegate>
@property (assign) id delegate;
@property (nonatomic) BOOL canView;
@property (nonatomic) BOOL canEdit;
@property (nonatomic) BOOL canSelect;
@property (nonatomic) BOOL canDelete;
@property (nonatomic) BOOL canEmail;
@property (nonatomic) BOOL canCreate;

- (void)reloadData;
@end

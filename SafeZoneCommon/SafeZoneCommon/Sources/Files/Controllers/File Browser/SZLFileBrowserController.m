//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLFileBrowserController.h"
#import "SZCFilePreviewController.h"
#import "SZLPhotoBrowserVC.h"
#import "SZLFileFactory.h"
#import "SZLFileBrowser.h"
#import "SZLFile.h"
#import "SZLFileBinary.h"
#import "SZLFileCode.h"
#import "SZLFileImage.h"
#import "SZLFileDocument.h"
#import "SZLSortView.h"
#import "SZLFileTableViewCell.h"
#import "PSPDFActionSheet.h"
#import <UIKit/UIKit.h>

#define Localized(key) \
[[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"SafeZoneCommonBundle" ofType:@"bundle"]] localizedStringForKey:(key) value:@"" table:@"filebrowser"]

static const int kScopeBarIndexAll = 0;
static const int kScopeBarIndexDocs  = 1;
static const int kScopeBarIndexBinary  = 2;
static const int kScopeBarIndexCode  = 3;
static const int kUIPickerComponentSortBy  = 0;
static const int kUIPickerComponentSortOrder  = 1;

@interface SZLFileBrowserController()<UIPickerViewDelegate, UIPickerViewDataSource, SZLSortViewDelegate, SZLFileTableViewCellDelegate>

@property (nonatomic) SZLFileBrowser* fileBrowser;
@property (nonatomic) UISearchBar *searchBar;
@property (nonatomic) UITapGestureRecognizer *tableGesture;
@property (nonatomic) UITapGestureRecognizer *navigationGesture;

@property (nonatomic) UISegmentedControl *filterSegments;
@property (nonatomic) SZLSortView *sortView;
@property (nonatomic) UIBarButtonItem *sortOrderButton;

@property (nonatomic) UIBarButtonItem *selectButton;
@property (nonatomic) UIBarButtonItem *addButton;
@property (nonatomic) UIBarButtonItem *deleteButton;
@property (nonatomic) UIBarButtonItem *emailButton;
@property (nonatomic) UIBarButtonItem *photosButton;

@property (nonatomic) PSPDFActionSheet *actionSheet;
@end

@implementation SZLFileBrowserController

-(id)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        // Defaults
        self.canView = YES;
        self.canSelect = YES;
        self.canEdit = NO;
        self.canDelete = NO;
        self.canEmail = NO;
        self.canCreate = NO;
    }
    return self;
}

- (void)dealloc
{
    [self destoryGestureRecognizers];   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Adjust look, height and make seperator go all the way across
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    self.tableView.rowHeight = 60.0f;

    // Registor our custom cell
    [[self tableView] registerClass:[SZLFileTableViewCell class] forCellReuseIdentifier:@"FileCell"];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.tableView), 44)];
    self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    self.searchBar.barTintColor = [UIColor colorWithWhite:0.96 alpha:1];
    self.searchBar.backgroundColor = [UIColor colorWithWhite:0.96 alpha:1];
    
    // Actually insert the view into the tables header
    self.tableView.tableHeaderView = self.searchBar;
    
    // Setup the buttons
    [self setupNavigationBar];
    [self setupToolbar];
    
    self.searchBar.delegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.title = Localized(@"FILES");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshButtons];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	[self.sortView setNeedsLayout];
}

- (void)setupToolbar
{
    // Make sure it's visible
    self.navigationController.toolbarHidden = NO;

    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
	self.sortOrderButton = [[UIBarButtonItem alloc] initWithImage:nil style:UIBarButtonItemStyleBordered target:self action:@selector(sortButtonPressed:)];

    // Delete button
    if (self.canDelete) {
        self.deleteButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                                                          target:self
                                                                          action:@selector(deleteButtonPressed:)];
    } else {
        self.deleteButton = fixedSpace;
    }
    
    self.photosButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"SafeZoneCommonBundle.bundle/photoStack.png"]
                                                         style:UIBarButtonItemStyleBordered
                                                        target:self
                                                        action:@selector(photosButtonPressed:)];
    
    self.toolbarItems = @[self.deleteButton, flexibleSpace, self.photosButton, flexibleSpace, self.sortOrderButton];
}

- (void)setupNavigationBar
{
    // Make sure it's not hidden
    self.navigationController.navigationBarHidden = NO;
    
    // Do not override the left button
    if (!self.navigationItem.leftBarButtonItem)
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                              target:self
                                                                                              action:@selector(cancelButtonPressed:)];
    NSMutableArray *rightButtons = [[NSMutableArray alloc] init];
    
    // Only allow selection or creation, not both.
    if (self.canSelect) {
        _selectButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                   target:self
                                                                      action:@selector(doneButtonPressed:)];
        [rightButtons addObject:_selectButton];
    } else if (self.canCreate) {
        _addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                   target:self
                                                                   action:@selector(addButtonPressed:)];
        [rightButtons addObject:_addButton];
    }

    // Email button, currently has action icon
    if (self.canEmail){
        _emailButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                     target:self
                                                                     action:@selector(emailButtonPressed:)];
        [rightButtons addObject:_emailButton];
    }
    
    self.navigationItem.rightBarButtonItems = rightButtons;
}

- (void)refreshButtons
{
    // Show ascending or descending icon
	if (self.fileBrowser.sortOrder == kSZLFileSortOrderAsc) {
        self.sortOrderButton.image = [UIImage imageNamed:@"SafeZoneCommonBundle.bundle/sort_asc.png"];
    } else {
       self.sortOrderButton.image = [UIImage imageNamed:@"SafeZoneCommonBundle.bundle/sort_des.png"];
    }
    
    // Set the enablement of the buttons, depending if files are selected
    if ([self.fileBrowser.selection count] > 0) {
        self.emailButton.enabled = YES;
        self.selectButton.enabled = YES;
        self.deleteButton.enabled = YES;
    } else {
        self.emailButton.enabled = NO;
        self.selectButton.enabled = NO;
        self.deleteButton.enabled = NO;
    }
    
    self.navigationController.toolbarHidden = NO;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return YES;
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
    }
}

#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.fileBrowser.files count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= [self.fileBrowser.files count])
	{
        return nil;
	}
    
    // Create our custom cell and give it the model
    SZLFileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FileCell"];
    SZLFile* file = self.fileBrowser.files[indexPath.row];
    cell.file = file;
	cell.checked = [self.fileBrowser.selection containsIndex:indexPath.row];
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= [self.fileBrowser.files count])
        return;
    
    // Stop search bar from keeping keyboard up
    [self.searchBar endEditing:YES];
    
    // Close any open action sheets
    [self closeActionSheet];
    
    // Only one file selected so create a fake array with our file
    SZLFile* file = self.fileBrowser.files[indexPath.row];
    NSArray* selectedFiles = @[file];
    
    NSString * title = [NSString stringWithFormat:Localized(@"ACTION_FOR_SELECTED_FILE"), file.name];
    
    // If only previewable, preview right away
    if (self.canView && file.isPreviewable && !(self.canDelete || self.canEdit || self.canEmail)) {
        SZCFilePreviewController *theController = [SZCFilePreviewController createPreviewer:[file path]
                                                                                       name:[file name]
                                                                                 passPhrase:nil];
        [self.navigationController pushViewController:theController animated:true];
    } else if (self.canView || self.canDelete || self.canEdit || self.canEmail) {
        
        // Show all of the options in action sheet
        PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:title];
        __weak SZLFileBrowserController *weakSelf = self;
        
        if (self.canView && file.isPreviewable) {
            [actionSheet addButtonWithTitle:Localized(@"VIEW_BUTTON") block:^{
                if (file.isPreviewable) {
                    SZCFilePreviewController *theController = [SZCFilePreviewController createPreviewer:[file path]
                                                                                                   name:[file name]
                                                                                             passPhrase:nil];
                    [self.navigationController pushViewController:theController animated:true];
                }
            }];
        }
        
        if (self.canEdit && file.isEditable) {
            [actionSheet addButtonWithTitle:Localized(@"EDIT_BUTTON") block:^{
                if ([self.delegate respondsToSelector:@selector(fileBrowser:didEditFile:)])
                    [self.delegate fileBrowser:self didEditFile:file];
            }];
        }
        
        if (self.canEmail) {
            [actionSheet addButtonWithTitle:Localized(@"EMAIL_BUTTON") block:^{
                // This can be done with normal buttons, reuse functions
                [weakSelf emailButtonPressed:selectedFiles];
            }];
        }
        
        if (self.canDelete) {
            [actionSheet setDestructiveButtonWithTitle:Localized(@"DELETE_BUTTON") block:^{
                // This can be done with normal buttons, reuse functions
                [weakSelf deleteButtonPressed:selectedFiles];
            }];
        }
        
        [actionSheet setCancelButtonWithTitle:Localized(@"CANCEL_BUTTON") block:nil];
        self.actionSheet = actionSheet;
        [actionSheet showInView:self.view];
    }
}

-(void)reloadData
{
    [self.fileBrowser reloadData];
    [self refreshButtons];
    [self.tableView reloadData];
}

#pragma mark - SZLFileTableViewCell Delegate

- (BOOL)SZLFileTableViewCellShouldShowCheckmark:(SZLFileTableViewCell*)cell
{
    // Show the checkmarks when we do multiple actions
    return self.canSelect || self.canDelete || self.canEmail;
}

- (BOOL)SZLFileTableViewCellShouldShowArrow:(SZLFileTableViewCell*)cell
{
    // Show the arrow if selecting the cell takes action
    return ((self.canView && cell.file.isPreviewable) ||
            (self.canEdit && cell.file.isEditable) ||
            self.canEmail || self.canDelete);
}

- (void)SZLFileTableViewCellDidSelectCheckmark:(SZLFileTableViewCell*)cell
{
    NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
    BOOL selected = [self.fileBrowser.selection containsIndex:indexPath.row];
    cell.checked = !selected;
    
    if (selected) {
        [self.fileBrowser.selection removeIndex:indexPath.row];
    } else {
        [self.fileBrowser.selection addIndex:indexPath.row];
    }
    
    [self refreshButtons];
}

#pragma mark - Search Delegate

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    self.fileBrowser.searchString = text;
    [self.searchBar setShowsCancelButton:([text length] > 0) animated:YES];
    [self reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.fileBrowser.searchString = nil;
    self.searchBar.text = nil;
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    switch (selectedScope) {
        case kScopeBarIndexBinary: {
            NSArray *extensions = [[SZLFileBinary supportedExtensions] allKeys];
            self.fileBrowser.extensionWhitelist = [[NSSet alloc] initWithArray:extensions];
            self.title = Localized(@"FILES_BINARY");
            break;
        }
        case kScopeBarIndexCode: {
            NSArray *extensions = [[SZLFileCode supportedExtensions] allKeys];
            self.fileBrowser.extensionWhitelist = [[NSSet alloc] initWithArray:extensions];
            self.title = Localized(@"FILES_CODE");
            break;
        }
        case kScopeBarIndexDocs: {
            NSArray *extensions = [[SZLFileDocument supportedExtensions] allKeys];
            self.fileBrowser.extensionWhitelist = [[NSSet alloc] initWithArray:extensions];
            self.title = Localized(@"FILES_DOCS");
            break;
        }
        case kScopeBarIndexAll:
        default: {
            self.fileBrowser.extensionWhitelist = nil;
            self.title = Localized(@"FILES");
            break;
        }
    }
    
    [self reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self setupGestureRecognizers];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self destoryGestureRecognizers];
}

- (void)setupGestureRecognizers
{
    self.tableGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    self.tableGesture.cancelsTouchesInView = YES;
    [self.tableView addGestureRecognizer:self.tableGesture];
    
    self.navigationGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    self.navigationGesture.cancelsTouchesInView = YES;
    [self.tableView addGestureRecognizer:self.navigationGesture];
    [self.navigationController.navigationBar addGestureRecognizer:self.navigationGesture];
}

- (void)destoryGestureRecognizers
{
    [self.tableView removeGestureRecognizer:self.tableGesture];
    [self.navigationController.navigationBar removeGestureRecognizer:self.navigationGesture];
    self.tableGesture = nil;
    self.navigationGesture = nil;
}

- (void) hideKeyboard
{
    [self destoryGestureRecognizers];
    [self.view endEditing:YES];
}

///////////////////////////////////////////////////////////////////////////////
// SortView
///////////////////////////////////////////////////////////////////////////////
#pragma mark - UIPicker DataSource & Delegate methods

- (void) sortViewDidFinish:(SZLSortView *)sortView
{
    self.fileBrowser.sortMethod = [self.sortView.picker selectedRowInComponent:kUIPickerComponentSortBy];
    self.fileBrowser.sortOrder = [self.sortView.picker selectedRowInComponent:kUIPickerComponentSortOrder];
    [self reloadData];
    [self sortViewDidCancel:sortView];
}

- (void)sortViewDidCancel:(SZLSortView *)sortView
{
    [self.sortView hideWithCompletion:^{
        self.sortView = nil;
        [self.navigationController setToolbarHidden:NO animated:NO];
		self.tableView.scrollEnabled = YES;
        [self refreshButtons];
    }];
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == kUIPickerComponentSortOrder) return 2;
    return kSZLFileSortByEnumCount;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (component == kUIPickerComponentSortOrder) {
        switch (row) {
            case kSZLFileSortOrderAsc:
                return Localized(@"ASCENDING_SORT_ORDER");
            case kSZLFileSortOrderDesc:
                return Localized(@"DESCENDING_SORT_ORDER");
        }
    }
    
    if (component == kUIPickerComponentSortBy) {
        switch (row) {
            case kSZLFileSortByNone:
                return Localized(@"SORT_BY_NONE");
            case kSZLFileSortByName:
                return Localized(@"SORT_BY_NAME");
            case kSZLFileSortBySize:
                return Localized(@"SORT_BY_SIZE");
            case kSZLFileSortByDateModified:
                return Localized(@"SORT_BY_DATE");
        }
    }
    return nil;
}

#pragma mark - Model

-(SZLFileBrowser*)fileBrowser
{
    if (!_fileBrowser) {
        _fileBrowser = [[SZLFileBrowser alloc] init];
        _fileBrowser.sortMethod = kSZLFileSortByNone;
    }

    return _fileBrowser;
}


#pragma mark - Targets

- (void)closeActionSheet
{
    if (self.actionSheet) {
        [self.actionSheet dismissWithClickedButtonIndex:self.actionSheet.cancelButtonIndex animated:NO];
    }
}

-(IBAction)cancelButtonPressed:(id)inSender
{
    if ([self.delegate respondsToSelector:@selector(fileBrowserDidCancel:)])
        [self.delegate fileBrowserDidCancel:self];
}

- (IBAction)doneButtonPressed:(id)inSender
{
    if ([self.delegate respondsToSelector:@selector(fileBrowser:didSelectFiles:)]) {
        [self.delegate fileBrowser:self didSelectFiles:[self.fileBrowser.files objectsAtIndexes:self.fileBrowser.selection]];
    }
}

- (IBAction)addButtonPressed:(id)inSender
{
    if ([self.delegate respondsToSelector:@selector(fileBrowserDidCreateItem:)])
        [self.delegate fileBrowserDidCreateItem:self];
}

- (IBAction)sortButtonPressed:(id)inSender
{
    [self.navigationController setToolbarHidden:YES animated:NO];
	// Stop it from scrolling
	self.tableView.scrollEnabled = NO;
	[self.tableView setContentOffset:self.tableView.contentOffset animated:NO];
	
	self.sortView = [[SZLSortView alloc] initWithDelegate:self];
	self.sortView.title = Localized(@"SORT_FILES_BY");
    [self.sortView showInView:self.tableView withCompletion:nil];
    // ASC / DESC selection
    [self.sortView.picker selectRow:self.fileBrowser.sortMethod inComponent:0 animated:NO];
    [self.sortView.picker selectRow:self.fileBrowser.sortOrder inComponent:1 animated:NO];
}


- (IBAction)photosButtonPressed:(id)sender
{
    [self closeActionSheet];
    if ([self.delegate conformsToProtocol:@protocol(SZLPhotoBrowserDelegate)]) {
        SZLPhotoBrowserVC* photoVC = [[SZLPhotoBrowserVC alloc] initWithDelegate:self.delegate];
        photoVC.canSelect = self.canSelect;
        photoVC.canEmail = self.canEmail;
        photoVC.canDelete = self.canDelete;
        [self.navigationController pushViewController:photoVC animated:YES];
    }
}


- (IBAction)emailButtonPressed:(id)sender
{
    [self closeActionSheet];
    
    NSArray* selectedFiles;
    
    // Sometimes we pass in the specific paths
    if ([sender isKindOfClass:[NSArray class]]) {
        selectedFiles = sender;
    } else {
        selectedFiles = [self.fileBrowser.files objectsAtIndexes:self.fileBrowser.selection];
    }
    
    if ([selectedFiles count] == 0)
        return;
    
    // Get the proper title, depending on count of select files
    NSString *title;
    if ([selectedFiles count] > 1) {
        title = [NSString stringWithFormat:Localized(@"EMAIL_SELECTED_FILES"), [selectedFiles count]];
    } else {
        id file = [selectedFiles firstObject];
        if ([file respondsToSelector:@selector(name)]) {
            title = [NSString stringWithFormat:Localized(@"EMAIL_FILE"), [file name]];
        } else if ([file respondsToSelector:@selector(lastPathComponent)]) {
            title = [NSString stringWithFormat:Localized(@"EMAIL_FILE"), [file lastPathComponent]];
        }
    }
    
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:title];
    [actionSheet addButtonWithTitle:Localized(@"EMAIL_BUTTON") block:^{
        if ([self.delegate respondsToSelector:@selector(fileBrowser:didEmailFiles:)])
            [self.delegate fileBrowser:self didEmailFiles:selectedFiles];
    }];
    [actionSheet setCancelButtonWithTitle:Localized(@"CANCEL_BUTTON") block:nil];
    self.actionSheet = actionSheet;
    [actionSheet showWithSender:sender fallbackView:self.view animated:YES];
}

- (IBAction)deleteButtonPressed:(id)sender
{
    [self closeActionSheet];
    
    // Sometimes we pass in the specific paths
    NSArray* selectedFiles;
    if ([sender isKindOfClass:[NSArray class]]) {
        selectedFiles = sender;
    } else {
        selectedFiles = [self.fileBrowser.files objectsAtIndexes:self.fileBrowser.selection];
    }
    
    // Get the proper title, depending on count of select files
    NSString *title;
    if ([selectedFiles count] > 1) {
        title = [NSString stringWithFormat:Localized(@"DELETE_SELECTED_FILES"), [selectedFiles count]];
    } else {
        id file = [selectedFiles firstObject];
        if ([file respondsToSelector:@selector(name)]) {
            title = [NSString stringWithFormat:Localized(@"DELETE_FILE"), [file name]];
        } else if ([file respondsToSelector:@selector(lastPathComponent)]) {
            title = [NSString stringWithFormat:Localized(@"DELETE_FILE"), [file lastPathComponent]];
        }
    }
    
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:title];
    [actionSheet setDestructiveButtonWithTitle:Localized(@"DELETE_BUTTON") block:^{
        if ([self.delegate respondsToSelector:@selector(fileBrowser:didDeleteFiles:)]) {
            [self.delegate fileBrowser:self didDeleteFiles:selectedFiles];
            [self reloadData];
        }
    }];
    
    [actionSheet setCancelButtonWithTitle:Localized(@"CANCEL_BUTTON") block:nil];
    self.actionSheet = actionSheet;
    [actionSheet showWithSender:sender fallbackView:self.view animated:YES];
}

@end

//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLFileTableViewCell.h"
#import "SZLFileImage.h"
#import "SZLFile.h"

#define Localized(key) \
[[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"SafeZoneCommonBundle" ofType:@"bundle"]] localizedStringForKey:(key) value:@"" table:@"filebrowser"]

const static BOOL thumbnailEnabled = NO;

@interface SZLFileTableViewCell ()
@property (nonatomic, strong) UIButton *checkButton;
@property (nonatomic, strong) UIImageView *previewArrow;
@property (nonatomic, strong) SZLFileImage *thumbnailFile;
@end

@implementation SZLFileTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.textLabel.backgroundColor = [UIColor clearColor];
    self.textLabel.textColor = [UIColor blackColor];
    self.textLabel.font = [UIFont systemFontOfSize:12.0f];
    self.textLabel.textAlignment = NSTextAlignmentLeft;
    self.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
    self.textLabel.numberOfLines = 0;
    self.textLabel.adjustsFontSizeToFitWidth = YES;
    self.textLabel.minimumScaleFactor = 0.7f;
    
    self.checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.checkButton.contentMode = UIViewContentModeTopRight;
    self.checkButton.adjustsImageWhenHighlighted = NO;
    self.checkButton.alpha = 0.7f;
    [self.checkButton setImage:[UIImage imageNamed:@"SafeZoneCommonBundle.bundle/ImageSelectedSmallOff.png"] forState:UIControlStateNormal];
    [self.checkButton setImage:[UIImage imageNamed:@"SafeZoneCommonBundle.bundle/ImageSelectedSmallOn.png"] forState:UIControlStateSelected];
    [self.checkButton addTarget:self action:@selector(checkmarkSelected:) forControlEvents:UIControlEventTouchUpInside];
    self.checkButton.hidden = YES;
    [self.contentView addSubview:self.checkButton];
    
    self.previewArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SafeZoneCommonBundle.bundle/UIBarButtonItemArrowRight"]];
    self.previewArrow.alpha = 0.2f;
    self.previewArrow.hidden = YES;
    
    [self.contentView addSubview:self.previewArrow];
    
    // Listen for MWPhoto notifications if we support thumbnails
    if (thumbnailEnabled) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleMWPhotoLoadingDidEndNotification:)
                                                     name:MWPHOTO_LOADING_DID_END_NOTIFICATION
                                                   object:nil];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
    self.checkButton.hidden = ![self.delegate SZLFileTableViewCellShouldShowCheckmark:self];
    self.previewArrow.hidden = ![self.delegate SZLFileTableViewCellShouldShowArrow:self];
    
    // Check and Preview arrow
    float height = self.contentView.bounds.size.height;
    float x = self.contentView.bounds.size.width;
    
    if (!self.checkButton.hidden) {
        float width = 44.0f;
        x -= width;
        CGRect checkFrame = CGRectMake(x, 0, width, height);
        self.checkButton.frame = checkFrame;
		self.checkButton.selected = self.isChecked;
    }

    CGRect previewFrame = self.previewArrow.frame;
    x -= (previewFrame.size.width + 5);
    previewFrame.origin.x = x;
    previewFrame.origin.y = CENTER_VERTICALLY(self.contentView, self.previewArrow);
    self.previewArrow.frame = previewFrame;
    
    // Icon Size
    CGRect iconframe = self.imageView.frame;
    iconframe.size = CGSizeMake(40, 40);
    self.imageView.frame = iconframe;
    
    // Make Sure it's centered
    iconframe.origin.y = CENTER_VERTICALLY(self.contentView, self.imageView);
    self.imageView.frame = iconframe;
}

-(void)prepareForReuse
{
    [super prepareForReuse];
}

#pragma mark - Button
- (void)checkmarkSelected:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(SZLFileTableViewCellDidSelectCheckmark:)]) {
        [self.delegate SZLFileTableViewCellDidSelectCheckmark:self];
    }
}

#pragma mark - Date formatting
- (NSDateFormatter*)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setDoesRelativeDateFormatting:YES];
    });
    return dateFormatter;
}

#pragma mark - Properties

- (void)setFile:(SZLFile *)file
{
    if (file) {
        NSMutableAttributedString *mutableAttString = [[NSMutableAttributedString alloc] init];
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", file.name]
                                                                         attributes: @{NSFontAttributeName : [UIFont boldSystemFontOfSize:13.0f]}];
        
        [mutableAttString appendAttributedString:attrString];
        attrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:Localized(@"FILE_CELL_SIZE"), [file sizeAsString]]
                                                     attributes: @{NSFontAttributeName : [UIFont systemFontOfSize:12.0f],
                                                                   NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
        
        [mutableAttString appendAttributedString:attrString];
        attrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:Localized(@"FILE_CELL_DATE_MODIFIED"), [[self dateFormatter] stringFromDate:file.dateModified]]
                                                     attributes: @{NSFontAttributeName : [UIFont systemFontOfSize:12.0f],
                                                                   NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
        [mutableAttString appendAttributedString:attrString];
        
        self.textLabel.attributedText = [mutableAttString copy];
        self.imageView.image = [file iconImage];

        // FIXME: Not good, should be async. Turned off by default right now.
        if (thumbnailEnabled && [file isKindOfClass:[SZLFileImage class]]) {
            SZLFileImage* fileImage = (SZLFileImage*)file;
            self.thumbnailFile = fileImage.thumbnail;
            [self.thumbnailFile loadUnderlyingImageAndNotify];
        }
    }
    self.checked = NO;
    _file = file;
}

-(void)setChecked:(BOOL)checked
{
    [self.checkButton setSelected:checked];
    _checked = checked;
}

#pragma mark - MWPhoto Loading Notification

- (void)handleMWPhotoLoadingDidEndNotification:(NSNotification *)notification
{
    id <MWPhoto> photo = [notification object];
    if (photo == self.thumbnailFile) {
        if ([photo underlyingImage]) {
            self.imageView.image = [photo underlyingImage];
            [self setNeedsLayout];
            [[NSNotificationCenter defaultCenter] removeObserver:self];
        }
    }
}

@end

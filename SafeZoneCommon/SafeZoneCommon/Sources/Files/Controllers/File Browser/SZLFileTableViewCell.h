//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import <UIKit/UIKit.h>

@class SZLFile;
@class SZLFileTableViewCell;

@protocol SZLFileTableViewCellDelegate <NSObject>
- (BOOL)SZLFileTableViewCellShouldShowCheckmark:(SZLFileTableViewCell*)cell;
- (BOOL)SZLFileTableViewCellShouldShowArrow:(SZLFileTableViewCell*)cell;
- (void)SZLFileTableViewCellDidSelectCheckmark:(SZLFileTableViewCell*)cell;
@end

@interface SZLFileTableViewCell : UITableViewCell
@property (nonatomic, weak)                 SZLFile* file;
@property (nonatomic, getter = isChecked)   BOOL checked;
@property (nonatomic, weak)                 id<SZLFileTableViewCellDelegate> delegate;
@end



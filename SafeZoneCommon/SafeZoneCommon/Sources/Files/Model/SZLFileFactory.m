//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLFileFactory.h"
#import "SZLFileBinary.h"
#import "SZLFileDocument.h"
#import "SZLFileImage.h"
#import "SZLFileCert.h"
#import "SZLFileCode.h"

@implementation SZLFileFactory

+(SZLFileFactory*)instance
{
    static SZLFileFactory *singleton = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        singleton = [[SZLFileFactory alloc] init];
    });
	
	return singleton;
}

-(SZLFile*) fileFromPath:(NSString*)path
{
    NSString *ext = [[path pathExtension] lowercaseString];
    
    if ([SZLFileDocument supportsExtension:ext])
        return [[SZLFileDocument alloc] initFromFilepath:path];
    
    if ([SZLFileImage supportsExtension:ext])
        return [[SZLFileImage alloc] initFromFilepath:path];
    
    if ([SZLFileBinary supportsExtension:ext])
        return [[SZLFileBinary alloc] initFromFilepath:path];
    
    if ([SZLFileCert supportsExtension:ext])
        return [[SZLFileCert alloc] initFromFilepath:path];
    
    if ([SZLFileCode supportsExtension:ext])
        return [[SZLFileCode alloc] initFromFilepath:path];
    
    return [[SZLFile alloc] initFromFilepath:path];
    
}

@end

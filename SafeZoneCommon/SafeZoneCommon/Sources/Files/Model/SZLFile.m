//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLFile.h"
#import "GoodSecureFileManager_SZL.h"
#import "PSPDFAlertView.h"

#define Localized(key) \
[[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"SafeZoneCommonBundle" ofType:@"bundle"]] localizedStringForKey:(key) value:@"" table:@"filebrowser"]

NSString *const kPdfMime = @"application/pdf";
NSString *const kJpegMime = @"image/jpeg";
NSString *const kPngMime = @"image/png";
NSString *const kGifMime = @"image/gif";
NSString *const kTiffMime = @"image/tiff";
NSString *const kBmpMime = @"image/bmp";
NSString *const kDocMime = @"application/msword";
NSString *const kDocxMime = @"application/vnd.openxmlformats-officedocument.wordprocessingml.document";
NSString *const kXlsMime = @"application/msexcel";
NSString *const kXlsxMime = @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
NSString *const kPptMime = @"application/mspowerpoint";
NSString *const kPptxMime = @"application/vnd.openxmlformats-officedocument.presentationml.presentation";
NSString *const kRtfMime = @"text/richtext";
NSString *const kTextMime = @"text/plain";
NSString *const kCalendarMime = @"text/calendar";
NSString *const kHtmlMime = @"text/html";
NSString *const kCssMime = @"text/css";
NSString *const kEmlMime = @"message/rfc822";

NSString *const kBinaryMime = @"application/octet-stream";

NSString *const kPEMMIME = @"application/x-pem-file";
NSString *const kP12MIME = @"application/x-pkcs12";
NSString *const kP7SMIME = @"application/x-pkcs7-signature";
NSString *const kCertMIME = @"application/x-x509-ca-cert";

NSString *const kCertIconFile = @"attachmentIconCertificate";
NSString *const kImageIconFile = @"attachmentIconImage";
NSString *const kTextIconFile = @"attachmentIconText";
NSString *const kCodeIconFile = @"attachmentIconCode";
NSString *const kWordIconFile = @"attachmentIconMSWord";
NSString *const kExcelIconFile = @"attachmentIconMSExcel";
NSString *const kPowerPointIconFile = @"attachmentIconMSPowerPoint";
NSString *const kHtmlIconFile = @"attachmentIconHTML";
NSString *const kCssIconFile = @"attachmentIconCSS";
NSString *const kCalendarIconFile = @"attachmentIconICS";
NSString *const kPdfIconFile = @"attachmentIconPDF";
NSString *const kZipIconFile = @"attachmentIconZIP";
NSString *const kNumbersIconFile = @"attachmentIconNumbers";
NSString *const kPagesIconFile = @"attachmentIconPages";
NSString *const kKeynoteIconFile = @"attachmentIconKeynote";

NSString *const kGenericIconFile = @"attachmentIconGeneric";

@interface SZLFile ()

@property (readwrite) NSString* name;
@property (readwrite) NSString* extension;
@property (readwrite) long size;
@property (readwrite) NSDate* dateModified;
@property (readwrite) BOOL isEditable;
@property (readwrite) BOOL isPreviewable;
@end

@implementation SZLFile

-(id)init
{
    self = [super init];
    
    // Defaults
    if (self) {
        self.iconName = kGenericIconFile;
        self.mime = kBinaryMime;
        self.isEditable = NO;
        self.isPreviewable = NO;
    }
    
    return self;
}

-(id)initFromFilepath:(NSString*)filePath
{
    self = [self init];
    if (self) {
        _path = [filePath copy];
        NSString* ext = [self extension];
        NSArray* vals = [[[self class] supportedExtensions] valueForKey:ext];
        if ([vals count] >= 4) {
            self.mime = vals[0];
            self.iconName = vals[1];
            self.isPreviewable = [vals[2] boolValue];
            self.isEditable = [vals[3] boolValue];
        }
    }

    return self;
}

#pragma mark - Properties

-(UIImage*)iconImage
{
    if (_iconName)
        return [UIImage imageNamed:[NSString stringWithFormat:@"SafeZoneCommonBundle.bundle/%@", self.iconName]];
    return nil;
}

-(NSString*)extension
{
    return [[self.path pathExtension] lowercaseString];
}

-(NSString*)name
{
    if (!_name) {
        _name = [self.path lastPathComponent];
    }
    return _name;
}

-(long)size
{
    if (_size == 0) {
        // it exists, find it's size
        _size = [[GoodSecureFileManager defaultManager] fileSizeForPath:self.path];
    }
    return _size;
}

-(NSDate*)dateModified
{
    if (!_dateModified) {
        // it exists, find it's size
        _dateModified = [[GoodSecureFileManager defaultManager] modificationDateForPath:self.path];
    }
    return _dateModified;
}

#pragma mark - Utility

- (NSString *)sizeAsString
{
    NSString *theString = @"";
    
    // Dealing with filesystem not actual bits, so 1000 bytes = kilobyte.
	if (self.size < 1000) {
		theString = [NSString stringWithFormat:@"%ld bytes", self.size];
	} else if (self.size < (1000*1000)) {
		theString = [NSString stringWithFormat:@"%.1f KB", (self.size/1000.0)];
	} else {
		theString = [NSString stringWithFormat:@"%.1f MB", (self.size/(1000.0*1000.0))];
	}
    
	return theString;
}

-(void)copyToDefaultDirectoryWithCompletion:(void (^)(SZLFile* file))completion
{
    NSString *directory = [self defaultDirectory];
    
    // copy it over
    NSString *fromPath = self.path;
    NSString *toPath = [directory stringByAppendingPathComponent:self.name];
	
	if ([fromPath caseInsensitiveCompare:toPath] == NSOrderedSame) {
		SZLFile* file = [[[self class] alloc] initFromFilepath:toPath];
		if (completion) completion(file);
		return;
	}
	
	if (![[SZLSecureFileManager defaultManager] fileExistsAtPath:directory])
	{
		[[SZLSecureFileManager defaultManager] createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
    NSString* replacementPath = [SZLSecureFileManager createUniquePathForFile:self.name in:directory];
    // have to check if the file name already exists and ask to replace existing or rename it with
    // a numbered extension, eg, text.doc -> text(1).doc -> text(2).doc
    if ([[SZLSecureFileManager defaultManager] fileExistsAtPath:toPath]) {
        PSPDFAlertView *alert = [[PSPDFAlertView alloc] initWithTitle:Localized(@"DUPLICATE_FILENAME_FOUND")];
        [alert setMessage:Localized(@"SHOULD_REPLACE_OR_RENAME")];
        [alert setCancelButtonWithTitle:Localized(@"CANCEL") block:nil];
        
        [alert addButtonWithTitle:Localized(@"REPLACE") block:^{
            [[SZLSecureFileManager defaultManager] copyItemAtPath:fromPath toPath:toPath error:nil];
            SZLFile* newFile = [[[self class] alloc] initFromFilepath:toPath];
            if (completion) completion(newFile);
        }];
        
        [alert addButtonWithTitle:Localized(@"RENAME") block:^{
            [[SZLSecureFileManager defaultManager] copyItemAtPath:fromPath toPath:replacementPath error:nil];
            SZLFile* newFile = [[[self class] alloc] initFromFilepath:replacementPath];
            if (completion) completion(newFile);
        }];
        
        [alert show];
    } else {
        [[SZLSecureFileManager defaultManager] copyItemAtPath:fromPath toPath:toPath error:nil];
        SZLFile* newFile = [[[self class] alloc] initFromFilepath:toPath];
        if (completion) completion(newFile);
    }
}

-(void)moveToDefaultDirectoryWithCompletion:(void (^)(SZLFile* file))completion
{
    NSString *directory = [self defaultDirectory];
    
    // copy it over
    NSString *fromPath = self.path;
    NSString *toPath = [directory stringByAppendingPathComponent:self.name];
	
	if ([fromPath caseInsensitiveCompare:toPath] == NSOrderedSame) {
		SZLFile* file = [[[self class] alloc] initFromFilepath:toPath];
		if (completion) completion(file);
		return;
	}
	
	if (![[SZLSecureFileManager defaultManager] fileExistsAtPath:directory])
	{
		[[SZLSecureFileManager defaultManager] createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
    NSString* replacementPath = [SZLSecureFileManager createUniquePathForFile:self.name in:directory];
    // have to check if the file name already exists and ask to replace existing or rename it with
    // a numbered extension, eg, text.doc -> text(1).doc -> text(2).doc
    if ([[SZLSecureFileManager defaultManager] fileExistsAtPath:toPath]) {
        PSPDFAlertView *alert = [[PSPDFAlertView alloc] initWithTitle:Localized(@"DUPLICATE_FILENAME_FOUND")];
        [alert setMessage:Localized(@"SHOULD_REPLACE_OR_RENAME")];
        [alert setCancelButtonWithTitle:Localized(@"CANCEL") block:nil];
        
        [alert addButtonWithTitle:Localized(@"REPLACE") block:^{
			[[SZLSecureFileManager defaultManager] removeItemAtPath:toPath error:nil];
            [[SZLSecureFileManager defaultManager] moveItemAtPath:fromPath toPath:toPath error:nil];
            SZLFile* newFile = [[[self class] alloc] initFromFilepath:toPath];
            if (completion) completion(newFile);
        }];
        
        [alert addButtonWithTitle:Localized(@"RENAME") block:^{
            [[SZLSecureFileManager defaultManager] moveItemAtPath:fromPath toPath:replacementPath error:nil];
            SZLFile* newFile = [[[self class] alloc] initFromFilepath:replacementPath];
            if (completion) completion(newFile);
        }];

		[alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    } else {
        [[SZLSecureFileManager defaultManager] moveItemAtPath:fromPath toPath:toPath error:nil];
        SZLFile* newFile = [[[self class] alloc] initFromFilepath:toPath];
        if (completion) completion(newFile);
    }
}


- (NSString*)defaultDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"SZLFile(%@)\n\tName:%@\n\tType:%@\n\tExtension:%@\n\tSize:%@\n\tDateModified:%@\n\t",
            [self class], self.name, [[self class] friendlyFileTypeFromExtension:self.extension], self.extension, [self sizeAsString], self.dateModified];
}

+(BOOL)supportsExtension:(NSString*)extension
{
    if ([self supportedExtensions]) {
        return ([[self supportedExtensions] valueForKey:extension] != nil);
    }

    return NO;
}

+(NSDictionary *)supportedExtensions
{
    return nil;
}

+(NSString*)friendlyFileTypeFromExtension:(NSString *)extension
{
    NSString *friendlyType = [NSString stringWithFormat:@"FRIENDLY_%@", [extension uppercaseString]];
    return [NSString stringWithFormat:Localized(friendlyType), extension];
}

+ (NSString*)documentsFolder
{
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+ (NSString*)photosFolder
{
	return [[SZLFile documentsFolder] stringByAppendingPathComponent:@"photos"];
}

+ (NSString*)thumbnailsFolder
{
	return [[SZLFile photosFolder] stringByAppendingPathComponent:@"thumbnails"];
}

@end

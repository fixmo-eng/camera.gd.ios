//
//  SZLFileImage+previewer.m
//  SafeZoneCommon
//
//  Created by Mike Wang on 2014-06-30.
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//

#import "SZLFileImage+previewer.h"
#import "SZCImagePreviewController.h"

@implementation SZLFileImage (previewer)

+ (SZCFilePreviewController *)previewerWithPassPhrase:(NSString *)inPassPhrase {
	return [SZCImagePreviewController createImagePreviewer];
}
@end

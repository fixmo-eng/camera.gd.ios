//
//  SZLFile+previewer.h
//  SafeZoneCommon
//
//  Created by Mike Wang on 2014-06-30.
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//

#import "SZLFile.h"

@class SZCFilePreviewController;

@interface SZLFile (previewer)
+ (SZCFilePreviewController *) previewerWithPassPhrase:(NSString *)inPassPhrase;
@end

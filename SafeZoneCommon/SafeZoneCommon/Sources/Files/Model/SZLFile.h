//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import <UIKit/UIKit.h>

typedef enum {
	fileTypeUnknown = 0,
	fileTypeDoc,
    fileTypeImage,
	fileTypeCert,
	fileTypeCode,
	fileTypeBinary,
} EFileType;


extern NSString *const kPdfMime;
extern NSString *const kJpegMime;
extern NSString *const kPngMime;
extern NSString *const kGifMime;
extern NSString *const kTiffMime;
extern NSString *const kBmpMime;
extern NSString *const kDocMime;
extern NSString *const kDocxMime;
extern NSString *const kXlsMime;
extern NSString *const kXlsxMime;
extern NSString *const kPptMime;
extern NSString *const kPptxMime;
extern NSString *const kRtfMime;
extern NSString *const kTextMime;
extern NSString *const kCalendarMime;
extern NSString *const kHtmlMime;
extern NSString *const kCssMime;
extern NSString *const kEmlMime;

extern NSString *const kBinaryMime;

extern NSString *const kPEMMIME;
extern NSString *const kP12MIME;
extern NSString *const kP7SMIME;
extern NSString *const kCertMIME;

extern NSString *const kCertIconFile;
extern NSString *const kImageIconFile;
extern NSString *const kTextIconFile;
extern NSString *const kCodeIconFile;
extern NSString *const kWordIconFile;
extern NSString *const kExcelIconFile;
extern NSString *const kPowerPointIconFile;
extern NSString *const kHtmlIconFile;
extern NSString *const kCssIconFile;
extern NSString *const kCalendarIconFile;
extern NSString *const kPdfIconFile;
extern NSString *const kZipIconFile;
extern NSString *const kNumbersIconFile;
extern NSString *const kPagesIconFile;
extern NSString *const kKeynoteIconFile;

extern NSString *const kGenericIconFile;

@interface SZLFile : NSObject

@property (nonatomic) NSString* mime;
@property (nonatomic) NSString* iconName;
@property (nonatomic) NSString* path;

@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSString* extension;
@property (nonatomic, readonly) long size;
@property (nonatomic, readonly) NSDate* dateModified;
@property (nonatomic, readonly) BOOL isEditable;
@property (nonatomic, readonly) BOOL isPreviewable;

-(id)initFromFilepath:(NSString*)filePath;

-(UIImage*)iconImage;
-(NSString*)sizeAsString;

-(void)copyToDefaultDirectoryWithCompletion:(void (^)(SZLFile* file))completion;
-(void)moveToDefaultDirectoryWithCompletion:(void (^)(SZLFile* file))completion;
-(NSString*)defaultDirectory;

+(BOOL)supportsExtension:(NSString*)extension;
+(NSDictionary *)supportedExtensions;
+(NSString*)friendlyFileTypeFromExtension:(NSString*)extension;

+(NSString*)photosFolder;
+(NSString*)documentsFolder;
+(NSString*)thumbnailsFolder;

@end

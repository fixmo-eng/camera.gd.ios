//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

// Sorting the filemanager by these methods
typedef NS_ENUM(NSUInteger, SZLFileSortBy)
{
	kSZLFileSortByNone = 0,
    kSZLFileSortByName,
    kSZLFileSortByDateModified,
    kSZLFileSortBySize,
    kSZLFileSortByEnumCount,
};

// Which direction to sort by
typedef NS_ENUM(NSUInteger, SZLFileSortOrder)
{
	kSZLFileSortOrderAsc = 0,
    kSZLFileSortOrderDesc,
    kSZLFileSortOrderCount,
};

@class SZLFile;

@interface SZLFileBrowser : NSObject

// Filtering and sorting
@property (nonatomic) NSString* searchString;
@property (nonatomic) NSSet* extensionWhitelist;
@property (nonatomic) NSSet* extensionBlacklist;
@property (nonatomic) SZLFileSortBy sortMethod;
@property (nonatomic) SZLFileSortOrder sortOrder;

@property (nonatomic, readonly) NSArray* files;
@property (nonatomic) NSMutableIndexSet* selection;
@property (nonatomic) NSArray* folders;

// Refresh data
-(void)reloadData;
@end

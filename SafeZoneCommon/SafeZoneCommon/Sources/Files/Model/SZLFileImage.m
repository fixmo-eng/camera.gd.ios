//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLFileImage.h"
#import "GoodSecureFileManager_SZL.h"
#import "SZLThumbnailGenerator.h"

const static int kThumbnailSize = 100;

static NSDictionary *sFileDictionary = nil;

@interface SZLFileImage ()
@property (strong, nonatomic) NSBlockOperation* loadImageOperation;
@end

@implementation SZLFileImage
@synthesize underlyingImage = _underlyingImage; // synth property from protocol
static NSOperationQueue *sOperationQueue = nil;

+(NSDictionary *)supportedExtensions
{
	if (sFileDictionary == nil) {
		sFileDictionary =  @{
             // extension: [MIMETYPE, ICON, PREVIEWABLE, EDITTABLE]
             @"png":	@[ kPngMime,	kImageIconFile, @YES, @NO ],
             @"jpg":	@[ kJpegMime,   kImageIconFile, @YES, @NO ],
             @"jpeg":   @[ kJpegMime,   kImageIconFile, @YES, @NO ],
             @"gif":	@[ kGifMime,	kImageIconFile, @YES, @NO ],
             @"tiff":   @[ kTiffMime,   kImageIconFile, @YES, @NO ],
             @"tif":	@[ kTiffMime,   kImageIconFile, @YES, @NO ],
             @"bmp":	@[ kBmpMime,	kImageIconFile, @YES, @NO ],
        };
    }
    return sFileDictionary;
}

+ (NSOperationQueue *) operationQueue
{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sOperationQueue = [[NSOperationQueue alloc] init];
        [sOperationQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
    });
    return sOperationQueue;
}

#pragma mark - MWPhoto Protocol Methods

- (UIImage *)underlyingImage {
    return _underlyingImage;
}

- (void)loadUnderlyingImageAndNotify {
    NSAssert([[NSThread currentThread] isMainThread], @"This method must be called on the main thread.");
    if (_loadingInProgress) return;
    _loadingInProgress = YES;
    @try {
        if (self.underlyingImage) {
            [self imageLoadingComplete];
        } else {
            [self performLoadUnderlyingImageAndNotify];
        }
    }
    @catch (NSException *exception) {
        self.underlyingImage = nil;
        _loadingInProgress = NO;
        [self imageLoadingComplete];
    }
    @finally {
    }
}

// Set the underlyingImage and call decompressImageAndFinishLoading on the main thread when complete.
// On error, set underlyingImage to nil and then call decompressImageAndFinishLoading on the main thread.
- (void)performLoadUnderlyingImageAndNotify {
    if ([[SZLSecureFileManager defaultManager] fileExistsAtPath:self.path]) {
        __weak SZLFileImage* weakSelf = self;
        // Load from asset library async
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            _loadImageOperation = [[NSBlockOperation alloc] init];
            [_loadImageOperation addExecutionBlock:^{
                @autoreleasepool {
                    NSData* imageData = [[SZLSecureFileManager defaultManager] contentsAtPath:weakSelf.path];
                    weakSelf.underlyingImage = [[UIImage alloc] initWithData:imageData];
                    if (!weakSelf.underlyingImage) {
                        NSLog(@"Error loading photo from path: %@", weakSelf.path);
                    }
                }
            }];
            [_loadImageOperation setCompletionBlock:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf imageLoadingComplete];
                });
                weakSelf.loadImageOperation = nil;
            }];
            
            [[SZLFileImage operationQueue] addOperation:_loadImageOperation];
        });
    }
    
}

// Release if we can get it again from path or url
- (void)unloadUnderlyingImage {
    _loadingInProgress = NO;
	if (self.underlyingImage) {
		self.underlyingImage = nil;
	}
}

- (void)imageLoadingComplete {
    NSAssert([[NSThread currentThread] isMainThread], @"This method must be called on the main thread.");
    // Complete so notify
    self.loadImageOperation = NO;
    // Notify on next run loop
    [self performSelector:@selector(postCompleteNotification) withObject:nil afterDelay:0];
}

- (void)postCompleteNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:MWPHOTO_LOADING_DID_END_NOTIFICATION
                                                        object:self];
}

- (void)cancelAnyLoading {
    if (self.loadImageOperation) {
        [self.loadImageOperation cancel];
        self.loadImageOperation = NO;
    }
}

#pragma mark - Thumbnails
- (BOOL)isThumbnail
{
    NSArray *pathComponents = [self.path pathComponents];
    if ([pathComponents count] > 1) {
        NSString* folder = [pathComponents objectAtIndex:([pathComponents count] - 2)];
        if ([folder caseInsensitiveCompare:@"thumbnails"]  == NSOrderedSame) return YES;
		// Old thumbnail folder
		if ([folder caseInsensitiveCompare:@"tn"]  == NSOrderedSame) return YES;
	}
    
    return NO;
}

- (NSString*)thumbnailName
{
    if (self.isThumbnail) return self.name;
    return [[[self name] stringByDeletingPathExtension] stringByAppendingPathExtension:@"png"];
}

- (BOOL)thumbnailExists
{
	NSString* newPath = [[SZLFile thumbnailsFolder] stringByAppendingPathComponent:[self thumbnailName]];
    return [[SZLSecureFileManager defaultManager] fileExistsAtPath:newPath];
}

- (SZLFileImage*)thumbnail
{
    if ([self isThumbnail])
	{
        return self;
	}
    
    if ([self thumbnailExists])
	{
		NSString* newPath = [[SZLFile thumbnailsFolder] stringByAppendingPathComponent:[self thumbnailName]];
        return [[SZLFileImage alloc] initFromFilepath:newPath];
	}
    
    return [self generateAndSaveThumbnail];
}

- (SZLFileImage*)generateAndSaveThumbnail
{
	if ([self isThumbnail])
	{
		return self;
	}
	
    NSData* imageData;
	NSString* thumbPath = [[SZLFile thumbnailsFolder] stringByAppendingPathComponent:[self thumbnailName]];
	
    if (_underlyingImage)
	{
        CGDataProviderRef provider = CGImageGetDataProvider(_underlyingImage.CGImage);
		imageData = (id)CFBridgingRelease(CGDataProviderCopyData(provider));
    }
	else
	{
         imageData = [[SZLSecureFileManager defaultManager] contentsAtPath:self.path];
    }

	NSData *thumbData = [SZLThumbnailGenerator thumbnailDataForImageWithData:imageData forSize:kThumbnailSize];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSString *folder = [SZLFile thumbnailsFolder];
        if (![[SZLSecureFileManager defaultManager] fileExistsAtPath:folder])
            [[SZLSecureFileManager defaultManager] createDirectoryAtPath:folder withIntermediateDirectories:YES attributes:nil error:nil];
        
        [[SZLSecureFileManager defaultManager] createFileAtPath:thumbPath contents:thumbData attributes:nil];
    });
    
    SZLFileImage *thumb = [[SZLFileImage alloc] initFromFilepath:thumbPath];
    thumb.underlyingImage = [UIImage imageWithData:thumbData];
    return thumb;
}

#pragma mark - Override default file settings

-(void)copyToDefaultDirectoryWithCompletion:(void (^)(SZLFile* file))completion
{
    void (^block)(SZLFile*) = [self processCompletetionBlock:completion];
    [super copyToDefaultDirectoryWithCompletion:block];
}

-(void)moveToDefaultDirectoryWithCompletion:(void (^)(SZLFile* file))completion
{
	void (^block)(SZLFile*) = [self processCompletetionBlock:completion];
    [super moveToDefaultDirectoryWithCompletion:block];
}

- (void (^)(SZLFile* file))processCompletetionBlock:(void (^)(SZLFile* file))completion
{
	// Add our own task to the completion block
	return ^(SZLFile* imageFile)
	{
        if ([imageFile respondsToSelector:@selector(thumbnail)])
		{
            [imageFile performSelector:@selector(thumbnail)];
		}
		
		if (completion)
		{
			completion(imageFile);
		}
    };
}

- (NSString*)defaultDirectory
{
	if (self.isThumbnail)
	{
		return [SZLFile thumbnailsFolder];
	}
	
    return [SZLFile photosFolder];
}

@end

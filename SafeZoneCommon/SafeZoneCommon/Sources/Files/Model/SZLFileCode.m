//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLFileCode.h"

static NSDictionary *sFileDictionary = nil;

@implementation SZLFileCode

+(NSDictionary *)supportedExtensions
{
	if (sFileDictionary == nil) {
		sFileDictionary =  @{
             // extension: [MIMETYPE, ICON, PREVIEWABLE, EDITTABLE]
             @"c":		  @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"h":		  @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"m":		  @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"cpp":      @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"cp":		  @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"c++":      @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"h++":      @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"mm":		  @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"java":	  @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"py":		  @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"rb":		  @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"ruby":	  @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"sh":		  @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"csh":      @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"bsh":      @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             @"xml":      @[ kTextMime,     kCodeIconFile, @YES, @NO ],
             
             @"htm":	  @[ kHtmlMime,     kHtmlIconFile, @YES, @NO ],
             @"html":	  @[ kHtmlMime,     kHtmlIconFile, @YES, @NO ],
             @"css":	  @[ kCssMime,      kCssIconFile, @YES, @NO ],
            };
    }
    return sFileDictionary;
}


@end

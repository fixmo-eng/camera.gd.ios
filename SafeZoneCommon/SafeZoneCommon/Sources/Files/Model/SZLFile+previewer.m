//
//  SZLFile+previewer.m
//  SafeZoneCommon
//
//  Created by Mike Wang on 2014-06-30.
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//

#import "SZLFile+previewer.h"
#import "SZCWebPreviewController.h"

@implementation SZLFile (previewer)
+ (SZCFilePreviewController *)previewerWithPassPhrase:(NSString *)inPassPhrase {
	return [SZCWebPreviewController createWebPreviewer];
}
@end

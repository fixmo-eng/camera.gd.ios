//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLFileCert.h"

static NSDictionary *sFileDictionary = nil;

@implementation SZLFileCert

+(NSDictionary *)supportedExtensions
{
	if (sFileDictionary == nil) {
		sFileDictionary =  @{
             // extension: [MIMETYPE, ICON, PREVIEWABLE, EDITTABLE]
             @"pem":	@[ kPEMMIME,	kCertIconFile, @YES, @NO ],
             @"p12":	@[ kP12MIME,	kCertIconFile, @YES, @NO ],
             @"cer":	@[ kCertMIME,   kCertIconFile, @YES, @NO ],
             @"cert":	@[ kCertMIME,   kCertIconFile, @YES, @NO ],
             @"p7s":	@[ kP7SMIME,	kCertIconFile, @YES, @NO ],
        };
    }
    return sFileDictionary;
}

@end

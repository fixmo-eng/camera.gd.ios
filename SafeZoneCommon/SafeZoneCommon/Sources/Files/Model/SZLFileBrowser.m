//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLFileBrowser.h"
#import "GoodSecureFileManager_SZL.h"
#import "SZLFileFactory.h"
#import "SZLFileImage.h"
#import "SZLFile.h"

#import "SZLLogger.h"

@interface SZLFileBrowser ()
@property (readwrite) NSArray* files;
@end

@implementation SZLFileBrowser

@synthesize extensionBlacklist = _extensionBlacklist;
@synthesize extensionWhitelist = _extensionWhitelist;

-(id)init
{
    self = [super init];
    if (self) {
        // Defaults
        self.sortMethod = kSZLFileSortByDateModified;
        self.sortOrder = kSZLFileSortOrderDesc;
        self.searchString = @"";
		self.extensionBlacklist = [[NSSet alloc] initWithArray:[[[SZLFileImage class] supportedExtensions] allKeys]];
		
		[self cleanDirectories];
    }
    return self;
}

-(NSArray*)files
{
    if (!_files) {
        
        // Get all of the files we support, unsorted
        NSError *error = nil;
        
        NSMutableArray *newFiles = [[NSMutableArray alloc] init];
        
        for (NSString* folder in self.folders) {
            NSMutableArray *files = [[[SZLSecureFileManager defaultManager] contentsOfDirectoryAtPath:folder error:&error] mutableCopy];
            [files filterUsingPredicate:[self generateFilterWithSearchString:self.searchString whitelist:self.extensionWhitelist blacklist:self.extensionBlacklist]];
            
            for (NSString* fileName in files) {
                NSString* fullPath = [folder stringByAppendingPathComponent:fileName];
                SZLFile* file = [[SZLFileFactory instance] fileFromPath:fullPath];
                [newFiles addObject:file];
            }
        }
        
        SZLFileSortOrder sort = self.sortOrder;
        
        switch (self.sortMethod) {
            case kSZLFileSortByName:
                [newFiles sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    SZLFile *left  = (SZLFile*)obj1;
                    SZLFile *right  = (SZLFile*)obj2;
                    return (sort == kSZLFileSortOrderAsc) ? [left.name localizedCaseInsensitiveCompare:right.name] : [right.name localizedCaseInsensitiveCompare:left.name];
                }];
                break;
            case kSZLFileSortBySize:
                [newFiles sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    SZLFile *left  = (SZLFile*)obj1;
                    SZLFile *right  = (SZLFile*)obj2;
                    
                    if (left.size < right.size) {
                        return (sort == kSZLFileSortOrderAsc) ? NSOrderedAscending : NSOrderedDescending;
                    } else if (left.size > right.size ) {
                        return (sort == kSZLFileSortOrderAsc) ? NSOrderedDescending : NSOrderedAscending;
                    }
                    
                    return NSOrderedSame;
                }];
                break;
            case kSZLFileSortByDateModified:
                [newFiles sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    SZLFile *left  = (SZLFile*)obj1;
                    SZLFile *right  = (SZLFile*)obj2;
                    return (sort == kSZLFileSortOrderAsc) ? [left.dateModified compare:right.dateModified] : [right.dateModified compare:left.dateModified];
                }];
                break;
            case kSZLFileSortByNone:
            default:
                break;
        }
        _files = newFiles;
    }
    return _files;
}

-(NSArray*)folders
{
    if (!_folders)
    {
        _folders = @[[SZLFile documentsFolder]];
    }
    return _folders;
}

- (NSMutableIndexSet*)selection
{
    if (!_selection) {
        _selection = [[NSMutableIndexSet alloc] init];
    }
    return _selection;
}

- (void)setSortOrder:(SZLFileSortOrder)sortOrder
{
    if (_sortOrder != sortOrder) {
        _sortOrder = sortOrder;
        [self reloadData];
    }
}

-(void)setSortMethod:(SZLFileSortBy)sortMethod
{
    if (_sortMethod != sortMethod) {
        _sortMethod = sortMethod;
        [self reloadData];
    }
}

-(void)setSearchString:(NSString *)searchString
{
    if (![searchString isEqualToString:_searchString]) {
        [self reloadData];
        _searchString = searchString;
    }
}

-(void)setExtensionWhitelist:(NSSet*)extensionFilter
{
    if (![extensionFilter isEqualToSet:_extensionWhitelist]) {
        _extensionWhitelist = extensionFilter;
        [self reloadData];
    }
}

-(NSSet*)extensionBlacklist
{
	if (!_extensionBlacklist) {
		_extensionBlacklist = [[NSSet alloc] init];
	}
	
	// Always add these to the blacklist
    return [_extensionBlacklist setByAddingObjectsFromArray:@[@"tdb", @"sqlite3", @"plist"]];
}

-(void)setExtensionBlacklist:(NSSet*)extensionFilter
{
    if (![extensionFilter isEqualToSet:_extensionBlacklist]) {
        _extensionBlacklist = extensionFilter;
        [self reloadData];
    }
}

-(NSPredicate*)generateFilterWithSearchString:(NSString*)query whitelist:(NSSet*)whitelist blacklist:(NSSet*)blacklist
{
    NSMutableArray* predicates = [[NSMutableArray alloc] init];
    
    // No Folders
    [predicates addObject:[NSPredicate predicateWithFormat:@"pathExtension.length > 0"]];
    
    // If we want certain files get only them
    NSMutableArray* whiteListPredicates = [[NSMutableArray alloc] init];
    
    // Have to compile ourselves due to IN not supporting case insensitivity
    for (NSString *currentWhitelist in whitelist) {
        [whiteListPredicates addObject:[NSPredicate predicateWithFormat:@"SELF ENDSWITH[c] %@", [@"." stringByAppendingString:currentWhitelist]]];
    }

    if ([whiteListPredicates count] > 0) {
        [predicates addObject:[NSCompoundPredicate orPredicateWithSubpredicates:whiteListPredicates]];
    }
    
    // Again we have to compile ourselves due to IN not supporting case insensitivity
    for (NSString *currentBlacklist in blacklist) {
        [predicates addObject:[NSPredicate predicateWithFormat:@"!(SELF ENDSWITH[c] %@)", [@"." stringByAppendingString:currentBlacklist]]];
    }
    
    // Contains is better than LIKE
    if ([self.searchString length] > 0) {
        [predicates addObject:[NSPredicate predicateWithFormat:@"SELF CONTAINS[c] %@", query]];
    }
    
    return [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
}

-(void)reloadData
{
    _files = nil;
    _selection = nil;
}

#pragma mark - Utilities

- (void)cleanDirectories
{
	[self moveOldThumbnails];

	NSError *error;
	
	// Get all of the files in the Documents directory
	NSArray *files = [[SZLSecureFileManager defaultManager] contentsOfDirectoryAtPath:[SZLFile documentsFolder] error:&error];
	
	if (error)
	{
		DLog(@"Error getting contents of: %@", [SZLFile documentsFolder] );
		DLog(@"Error returned: %@", [error localizedDescription]);
	}
	else
	{
		// Only use the valid files
		NSSet *blacklist = [NSSet setWithArray:@[@"tdb", @"sqlite3", @"plist"]];
		NSPredicate *predicate = [self generateFilterWithSearchString:nil whitelist:nil blacklist:blacklist];
		files = [files filteredArrayUsingPredicate:predicate];
		
		for (NSString* file in files)
		{
			NSString* fullPath = [[SZLFile documentsFolder] stringByAppendingPathComponent:file];
			SZLFile* file = [[SZLFileFactory instance] fileFromPath:fullPath];
			[file moveToDefaultDirectoryWithCompletion:nil];
		}

	}
	
}

- (void)moveOldThumbnails
{
	NSError *error;
	
	NSString* oldThumbnailFolder = [[SZLFile documentsFolder] stringByAppendingPathComponent:@"tn"];
	
	if (![[SZLSecureFileManager defaultManager] fileExistsAtPath:oldThumbnailFolder])
	{
		return;
	}
	
	NSArray *files = [[SZLSecureFileManager defaultManager] contentsOfDirectoryAtPath:oldThumbnailFolder error:&error];
	
	if (error)
	{
		DLog(@"Error getting contents of: %@", oldThumbnailFolder);
		DLog(@"Error returned: %@", [error localizedDescription]);
	}
	else
	{
		for (NSString* file in files)
		{
			NSString* fullPath = [oldThumbnailFolder stringByAppendingPathComponent:file];
			SZLFile* file = [[SZLFileFactory instance] fileFromPath:fullPath];
			[file moveToDefaultDirectoryWithCompletion:nil];
		}
		[[SZLSecureFileManager defaultManager] removeItemAtPath:oldThumbnailFolder error:&error];
		
		if (error)
		{
			DLog(@"Error removing: %@", oldThumbnailFolder);
			DLog(@"Error returned: %@", [error localizedDescription]);
		}
	}
}

@end

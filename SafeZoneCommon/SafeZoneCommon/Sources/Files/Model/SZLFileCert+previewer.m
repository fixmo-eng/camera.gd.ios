//
//  SZLFileCert+previewer.m
//  SafeZoneCommon
//
//  Created by Mike Wang on 2014-06-30.
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//

#import "SZLFileCert+previewer.h"
#import "SZCCertPreviewController.h"

@implementation SZLFileCert (previewer)

+ (SZCFilePreviewController *)previewerWithPassPhrase:(NSString *)inPassPhrase {
	return [SZCCertPreviewController createPEMPreviewer:inPassPhrase];
}
@end

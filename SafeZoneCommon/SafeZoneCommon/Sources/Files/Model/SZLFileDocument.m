//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import "SZLFileDocument.h"

static NSDictionary *sFileDictionary = nil;

@implementation SZLFileDocument

+(NSDictionary *)supportedExtensions
{
	if (sFileDictionary == nil) {
		sFileDictionary =  @{
             // extension: [MIMETYPE, ICON, PREVIEWABLE, EDITTABLE]
             @"pdf":   @[ kPdfMime,	kPdfIconFile, @YES, @NO ],
             
             // iWork suite
             @"numbers":@[ kBinaryMime, kNumbersIconFile, @YES, @NO ],
             @"pages":	@[ kBinaryMime,	kPagesIconFile, @YES, @NO ],
             @"keynote":@[ kBinaryMime,	kKeynoteIconFile, @YES, @NO ],
             
             // microsoft
             @"doc":	@[ kDocMime,	kWordIconFile, @YES, @YES ],
             @"docx":	@[ kDocxMime,	kWordIconFile, @YES, @YES ],
             @"xls":	@[ kXlsMime,	kExcelIconFile, @YES, @YES ],
             @"xlsx":	@[ kXlsxMime,	kExcelIconFile, @YES, @YES ],
             @"ppt":	@[ kPptMime,	kPowerPointIconFile, @YES, @YES ],
             @"pptx":	@[ kPptxMime,	kPowerPointIconFile, @YES, @YES ],
             
             // uncommon microsoft
             @"dotx":	@[ kDocMime,	kWordIconFile, @YES, @NO ],
             @"dotm":	@[ kDocxMime,	kWordIconFile, @YES, @NO ],
             @"xlsm":	@[ kXlsMime,	kExcelIconFile, @YES, @NO ],
             @"xltx":	@[ kXlsxMime,	kExcelIconFile, @YES, @NO ],
             @"xltm":	@[ kXlsMime,	kExcelIconFile, @YES, @NO ],
             @"xlsb":	@[ kXlsxMime,	kExcelIconFile, @YES, @NO ],
             @"xlam":	@[ kXlsxMime,	kExcelIconFile, @YES, @NO ],
             @"pptm":	@[ kPptxMime,	kPowerPointIconFile, @YES, @NO ],
             @"potx":	@[ kPptxMime,	kPowerPointIconFile, @YES, @NO ],
             @"pptm":	@[ kPptxMime,	kPowerPointIconFile, @YES, @NO ],
             @"ppam":	@[ kPptxMime,	kPowerPointIconFile, @YES, @NO ],
             @"ppsx":	@[ kPptxMime,	kPowerPointIconFile, @YES, @NO ],
             @"ppsm":	@[ kPptxMime,	kPowerPointIconFile, @YES, @NO ],
             
             // text
             @"txt":	@[ kTextMime,	kTextIconFile, @YES, @YES ],
             @"text":	@[ kTextMime,	kTextIconFile, @YES, @NO ],
             
             // calendar
             @"ics":	@[ kCalendarMime, kCalendarIconFile, @NO, @NO ],
            };
    }
    return sFileDictionary;
}

@end





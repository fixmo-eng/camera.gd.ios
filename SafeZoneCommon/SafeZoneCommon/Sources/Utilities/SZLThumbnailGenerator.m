#include <ImageIO/ImageIO.h>

#import "SZLThumbnailGenerator.h"
#import "GoodSecureFileManager.h"

@implementation SZLThumbnailGenerator

+ (CGFloat) maxPixelSizeForImageWithData:(NSData*)imageData forSize:(CGFloat)size
{
    UIImage* image = [UIImage imageWithData:imageData];

    CGFloat width, height, ratio;
    width  = image.size.width;
    height = image.size.height;

    if ( height > width ) {
        ratio = size / width;
    } else {
        ratio = size / height;
    }

    CGFloat maxPixelSize = MAX( (width * ratio), (height * ratio) );
    maxPixelSize = ceil(maxPixelSize);

    return maxPixelSize;
}

+ (NSData*) thumbnailDataForImageWithData:(NSData*)imageData forSize:(CGFloat)size
{
    CGFloat maxPixelSize = [SZLThumbnailGenerator maxPixelSizeForImageWithData:imageData forSize:size];

    CFTypeRef keys[3];
    CFTypeRef vals[3];

    keys[0] = kCGImageSourceCreateThumbnailFromImageAlways;
    vals[0] = kCFBooleanTrue;
    keys[1] = kCGImageSourceThumbnailMaxPixelSize;
    vals[1] = (__bridge CFNumberRef)@(maxPixelSize);
    keys[2] = kCGImageSourceCreateThumbnailWithTransform;
    vals[2] = kCFBooleanTrue;

    CFDictionaryRef thumbOpts = CFDictionaryCreate(NULL,
                                                   keys, vals,
                                                   3,
                                                   &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);

    CGImageSourceRef srcRef = CGImageSourceCreateWithData((__bridge CFDataRef)imageData, NULL);
    CGImageRef thumbRef = CGImageSourceCreateThumbnailAtIndex(srcRef, 0, thumbOpts);
    UIImage* thumbImg = [UIImage imageWithCGImage:thumbRef];
    NSData* thumbData = UIImagePNGRepresentation(thumbImg);

    CGImageRelease(thumbRef);
    CFRelease(srcRef);
    CFRelease(thumbOpts);

    return thumbData;
}

@end

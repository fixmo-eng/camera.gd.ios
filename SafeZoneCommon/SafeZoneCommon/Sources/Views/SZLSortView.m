//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//
#import "SZLSortView.h"

const static float kUIPickerHeight = 216.0f;
const static float kUIToolbarHeight = 44.0f;

@interface SZLSortView()
@property (nonatomic) BOOL showing;
@property (nonatomic) UIToolbar *toolbar;
@property (nonatomic) UILabel *toolbarLabel;
@end

@implementation SZLSortView

- (instancetype)initWithDelegate:(id<UIPickerViewDelegate, UIPickerViewDataSource, SZLSortViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
		
        self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.0f];
		
        self.toolbar = [[UIToolbar alloc] initWithFrame:CGRectZero];
        self.toolbarLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.toolbarLabel.text = self.title;
        self.toolbarLabel.lineBreakMode = NSLineBreakByClipping;
        self.toolbar.items = @[
							   [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(toggleSortCancel:)],
							   [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
							   [[UIBarButtonItem alloc] initWithCustomView:self.toolbarLabel],
							   [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
							   [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(toggleSortDone:)],
							   ];
        
		self.picker = [[UIPickerView alloc] initWithFrame:CGRectZero];
		self.picker.showsSelectionIndicator = YES;
		self.picker.backgroundColor = [UIColor whiteColor];
        
		[self.picker setDataSource:self.delegate];
		[self.picker setDelegate:self.delegate];
		
        [self addSubview:self.toolbar];
        [self addSubview:self.picker];
    }
    return self;
}

- (void)showInView:(UIView*)parent withCompletion:(void (^)(void))completion
{
    if (!self.showing) {
		[parent addSubview:self];
        [parent bringSubviewToFront:self];
		
        self.frame = [self frameForOffscreen];
        self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.0f];
        [UIView animateWithDuration:0.3f
                         animations:^{
                             self.frame = [self frameForOnscreen];
                         }
                         completion:^(BOOL inFinished) {
                             [UIView animateWithDuration:0.1f animations:^{
                                 self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
                             }];
                             if (completion) completion();
                         }];
    }
    self.showing = YES;
}

- (void)hideWithCompletion:(void (^)(void))completion
{
    if (self.showing) {
        [UIView animateWithDuration:0.1f
                         animations:^{
                             self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.0f];
                         }
                         completion:^(BOOL finished) {
                             [UIView animateWithDuration:0.3f
                                              animations:^{
                                                  self.frame = [self frameForOffscreen];
                                              }
                                              completion:^(BOOL inFinished) {
                                                  [self removeFromSuperview];
                                                  if (completion) completion();
                                              }];
                         }];
    }
    self.showing = NO;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	if (self.showing)
	{
		self.frame = [self frameForOnscreen];
	}
	else
	{
		self.frame = [self frameForOffscreen];
	}
	
	self.toolbar.frame = CGRectMake(0,
									CGRectGetHeight(self.frame) - kUIPickerHeight - kUIToolbarHeight,
									CGRectGetWidth(self.frame),
									kUIToolbarHeight);
	
	self.toolbarLabel.frame = [self.title boundingRectWithSize:self.toolbar.frame.size options:0 attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} context:nil];
	
	self.picker.frame = CGRectMake(0,
								   CGRectGetHeight(self.frame) - kUIPickerHeight,
								   CGRectGetWidth(self.frame),
								   kUIPickerHeight);
}

- (IBAction) toggleSortDone:(id)inSender
{
    [self.delegate sortViewDidFinish:self];
}

- (IBAction) toggleSortCancel:(id)inSender
{
    [self.delegate sortViewDidCancel:self];
}

- (void)setTitle:(NSString *)title
{
	[self.toolbarLabel setText:title];
	_title = title;
}

- (CGRect)frameForOffscreen
{
    // Sort view
    CGRect frame = self.superview.bounds;
    
    // Offscreen
    frame.origin.y += kUIPickerHeight;
    return frame;
}

- (CGRect)frameForOnscreen
{
    // Sort view
    return self.superview.bounds;
}

@end

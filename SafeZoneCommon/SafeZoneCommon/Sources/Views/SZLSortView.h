//
//  SafeZone
//
//  FIXMO CONFIDENTIAL
//
//  Copyright (c) 2014 Fixmo Inc. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//

#import <UIKit/UIKit.h>

@class SZLSortView;
@protocol SZLSortViewDelegate <NSObject>
- (void)sortViewDidFinish:(SZLSortView*)sortView;
- (void)sortViewDidCancel:(SZLSortView*)sortView;
@end

@interface SZLSortView : UIView
@property (nonatomic      )   NSString *title;
@property (nonatomic      )   UIPickerView *picker;
@property (nonatomic, weak)   id<UIPickerViewDelegate, UIPickerViewDataSource, SZLSortViewDelegate> delegate;

- (instancetype)initWithDelegate:(id<UIPickerViewDelegate, UIPickerViewDataSource, SZLSortViewDelegate>)delegate;
- (void)showInView:(UIView*)parent withCompletion:(void (^)(void))completion;
- (void)hideWithCompletion:(void (^)(void))completion;
@end

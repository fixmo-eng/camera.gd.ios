//
//  GoodSecureFileManager_SZL.h
//  SZSecurity
//
//  Created by Mike Wang on 2014-07-2.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "GoodSecureFileManager.h"

//C-API for getting the file key.
NSData* fetchFileKey();

@interface GoodSecureFileManager (SZL)

+(NSString *)createUniquePathForFile:(NSString *)name in:(NSString *)directory;

@end

//
//  GDCameraAppDelegate.m
//  GoodCamera
//
//  Created by Mike Wang on 2014-06-30.
//  Copyright (c) 2014 KreevRoo Consulting. All rights reserved.
//

#import <GD/GDServices.h>
#import <GD/GDAppDetail.h>
#import <GD/GDFileSystem.h>
#import "GDCameraAppDelegate.h"
#import "KRCCameraViewController.h"
#import "KRCCameraConstants.h"
#import "GoodSecureFileManager.h"
#import "SZLFile.h"
#import "SZLFileImage.h"
#import "AppSelectorViewController.h"
#import "DTAlertView.h"

#define kSendEmailServiceName				@"com.good.gfeservice.send-email"
#define kSendEmailServiceVersion			@"1.0.0.0"
#define kSendEmailMethod					@"sendEmail"

#define kRequestFilesServiceName			@"com.good.gdservice.request-files"
#define kRequestFilesServiceVersion			@"1.0.0.0"
#define kRequestFilesMethod					@"requestFiles"

#define kTransferFileServiceName			@"com.good.gdservice.transfer-file"
#define kTransferFileServiceVersion			@"1.0.0.0"
#define kTransferFileMethod					@"transferFile"

#define kTransferMultipleFilesServiceName	@"com.good.gdservice.transfer-multiple-files"
#define kTransferMultipleFilesServiceVersion @"1.0.0.3"
#define kTransferMultipleFilesMethod		@"transferFiles"

@interface GDCameraAppDelegate () <GDServiceDelegate, GDServiceClientDelegate, DTAlertViewDelegate>
{
	BOOL started;
    BOOL shouldShowAlertView;
}

@property (strong, nonatomic) GDiOS* good;
@property (strong, nonatomic) GDService* service;
@property (strong, nonatomic) GDServiceClient* serviceClient;

// A request for which we are the service provider.
@property (nonatomic, copy) NSString* serviceRequestApplication;
@property (nonatomic, copy) NSString* serviceRequestID;

// A request for which we are the consumer.
@property (nonatomic, copy) NSString* clientRequestApplication;
@property (nonatomic, copy) NSString* clientRequestID;
@property (nonatomic, strong) DTAlertView* clientRequestDTAlert;
@property (nonatomic, strong) void(^clientRequestCompletionHandler)(id params, NSArray* filePaths);
@property (nonatomic, strong) void(^appPickerCompletionHandler)(GDAppDetail* provider);

@property (nonatomic, weak) KRCCameraViewController* cameraVC;

@property (strong, nonatomic) UIPopoverController *appSelectorListPopoverController;
@property (strong, nonatomic) AppSelectorViewController *appSelectorListViewController;

@end

#pragma mark -

@implementation GDCameraAppDelegate

// If more than one provider is available, ask the user to choose and return the provider.
- (void)providerForService:(NSString*)service andVersion:(NSString*)version fromBarButton:(UIBarButtonItem*)barButton completionHandler:(void(^)(GDAppDetail* provider))completionHandler
{
	NSArray* allApps = [self distinctProvidersForService:service andVersion:version];
	
	if (allApps.count == 0)
	{
		completionHandler(nil);
	}
	else if (allApps.count == 1)
	{
		completionHandler(allApps[0]);
	}
	else
	{
		self.appPickerCompletionHandler = completionHandler;
		
		dispatch_async(dispatch_get_main_queue(),
		^{
			[self showPickerForApplicationList:allApps buttonForPopover:barButton];
		});
	}
}

// Is any provider available for this service?
- (BOOL)hasProviderForService:(NSString*)service andVersion:(NSString*)version
{
	return [self distinctProvidersForService:service andVersion:version].count > 0;
}

// Return a list of GDAppDetail objects for providers of a service, not including this app.
- (NSArray*)distinctProvidersForService:(NSString*)service andVersion:(NSString*)version
{
	NSArray* serviceProviders = [self.good getApplicationDetailsForService:service andVersion:version];
	NSMutableDictionary* distinctProviders = [NSMutableDictionary dictionary];
	NSString* thisApp = [[NSBundle mainBundle] bundleIdentifier];
	
	for (GDAppDetail* app in serviceProviders)
	{
		if (![app.address isEqualToString:thisApp])
		{
			distinctProviders[app.address] = app;
		}
	}
	
	return [distinctProviders allValues];
}

- (BOOL)canEmailPhotos
{
	return [self hasProviderForService:kSendEmailServiceName andVersion:kSendEmailServiceVersion];
}

- (void)emailPhotos:(NSArray*)photos fromBarButton:(UIBarButtonItem*)barButton
{
	if (photos.count > 0) [self sendPhotos:photos withService:kSendEmailServiceName version:kSendEmailServiceVersion method:kSendEmailMethod fromBarButton:barButton];
}

// Flag to check whether a requestFiles service is available.
- (BOOL)canImportImages
{
	return [self hasProviderForService:kRequestFilesServiceName andVersion:kRequestFilesServiceVersion];
}

// Use the requestFiles service to retrieve list of photos as attachments.
- (void)importImagesFromBarButton:(UIBarButtonItem*)barButton withCompletionHandler:(void(^)(id params, NSArray* filePaths))completionHandler
{
	[self providerForService:kRequestFilesServiceName andVersion:kRequestFilesServiceVersion fromBarButton:barButton completionHandler:^(GDAppDetail* provider)
	{
		if (provider)
		{
			// Put up modal alert to allow the user to cancel.
            shouldShowAlertView = YES;
			
			// Send blocking request for service.
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
			^{
				NSString* requestID = nil;
				NSError* error = nil;
				NSDictionary* params = @{@"extensionWhiteList":[[SZLFileImage supportedExtensions] allKeys]};
				
				[GDServiceClient sendTo:provider.address withService:kRequestFilesServiceName withVersion:provider.versionId withMethod:kRequestFilesMethod withParams:params withAttachments:nil bringServiceToFront:GDEPreferPeerInForeground requestID:&requestID error:&error];
				
				if (error)
				{
					DLog(@"Error sending mail: %@", error);
				}
				else
				{
					self.clientRequestID = requestID;
					self.clientRequestApplication = provider.address;
					self.clientRequestCompletionHandler = completionHandler;
				}
			});
		}
	}];
}

- (BOOL)canTransferPhotosWithCount:(NSInteger)count
{
	if (count == 1)
	{
		return [self hasProviderForService:kTransferFileServiceName andVersion:kTransferFileServiceVersion] || [self hasProviderForService:kTransferMultipleFilesServiceName andVersion:kTransferMultipleFilesServiceVersion];
	}
	else if (count > 1)
	{
		return [self hasProviderForService:kTransferMultipleFilesServiceName andVersion:kTransferMultipleFilesServiceVersion];
	}
	
	return NO;
}

- (void)transferPhotos:(NSArray*)photos fromBarButton:(UIBarButtonItem*)barButton
{
	if (photos.count == 1)
	{
		// FIXME: This will not give users the ability to choose a multiple file service
		// if a single file service is available.
		if ([self hasProviderForService:kTransferFileServiceName andVersion:kTransferFileServiceVersion])
		{
			[self sendPhotos:photos withService:kTransferFileServiceName version:kTransferFileServiceVersion method:kTransferFileMethod fromBarButton:barButton];
		}
		else
		{
			[self sendPhotos:photos withService:kTransferMultipleFilesServiceName version:kTransferMultipleFilesServiceVersion method:kTransferMultipleFilesMethod fromBarButton:barButton];
		}
	}
	else if (photos.count > 1)
	{
		[self sendPhotos:photos withService:kTransferMultipleFilesServiceName version:kTransferMultipleFilesServiceVersion method:kTransferMultipleFilesMethod fromBarButton:barButton];
	}
}

- (void)sendPhotos:(NSArray*)photos withService:(NSString*)service version:(NSString*)version method:(NSString*)method fromBarButton:(UIBarButtonItem*)barButton
{
	// Convert array of SZLFile objects to array of file paths.
	NSMutableArray* gdFiles = nil;
	if (photos && photos.count > 0)
	{
		gdFiles = [NSMutableArray arrayWithCapacity:photos.count];
		
		for (SZLFile* photoFile in photos)
		{
			[gdFiles addObject:photoFile.path];
		}
	}
	
	// Send service request.
	[self providerForService:service andVersion:version fromBarButton:barButton completionHandler:^(GDAppDetail* provider)
	{
		if (provider)
		{
			// Put up modal alert to allow the user to cancel.
            shouldShowAlertView = YES;
			
			// Send blocking request for service.
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
			^{
				NSString* requestID = nil;
				NSError* error = nil;
				
				[GDServiceClient sendTo:provider.address withService:service withVersion:provider.versionId withMethod:method withParams:nil withAttachments:gdFiles bringServiceToFront:GDEPreferPeerInForeground requestID:&requestID error:&error];
				
				if (error)
				{
					DLog(@"Error sending mail: %@", error);
				}
				else
				{
					self.clientRequestID = requestID;
					self.clientRequestApplication = provider.address;
				}
			});
		}
	}];
}

// Nil argument indicates that the user cancelled.
- (void)provideRequestedPhotos:(NSArray*)photos
{
	// Convert array of SZLFile objects to array of file paths.
	NSMutableArray* gdFiles = nil;
	if (photos && photos.count > 0)
	{
		gdFiles = [NSMutableArray arrayWithCapacity:photos.count];
		
		for (SZLFile* photoFile in photos)
		{
			[gdFiles addObject:photoFile.path];
		}
	}
	
	NSError* error = nil;
	BOOL success = [GDService replyTo:self.serviceRequestApplication withParams:nil bringClientToFront:GDEPreferPeerInForeground withAttachments:gdFiles requestID:self.serviceRequestID error:&error];
	
	if (!success)
	{
		NSLog(@"GDService replyTo error: %@", error);
	}
}

- (void)showPickerForApplicationList:(NSArray*)appList buttonForPopover:(UIBarButtonItem*)button
{
	if ([self.appSelectorListPopoverController isPopoverVisible])
	{
		return;
	}
	
	self.appSelectorListViewController = [[AppSelectorViewController alloc] initWithAppList:appList];
	self.appSelectorListViewController.delegate = self;
	
	// Push onto navigation stack on iPhone
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
	{
		[self.cameraVC.navigationController pushViewController:self.appSelectorListViewController animated:YES];

	}
	// Show popover on iPad
	else
	{
		self.appSelectorListPopoverController = [[UIPopoverController alloc] initWithContentViewController: self.appSelectorListViewController];
		_appSelectorListPopoverController.popoverContentSize = _appSelectorListViewController.view.frame.size;

		// 'Pop' the list of registered apps as a popover over the bar-button
		[self.appSelectorListPopoverController presentPopoverFromBarButtonItem:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
}

- (void)appSelected:(GDAppDetail*)appDetails
{
	if (self.appSelectorListPopoverController)
	{
		[self.appSelectorListPopoverController dismissPopoverAnimated:YES];
	}
	else
	{
		[self.cameraVC.navigationController popViewControllerAnimated:YES];
	}
	
	self.appSelectorListPopoverController = nil;
	self.appSelectorListViewController = nil;
	
	self.appPickerCompletionHandler(appDetails);
}

#pragma mark - DTAlertViewDelegate methods
- (void)alertView:(DTAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	[GDServiceClient cancelRequest:self.clientRequestID toApplication:self.clientRequestApplication];
	
    self.clientRequestDTAlert = nil;
	self.clientRequestID = nil;
	self.clientRequestApplication = nil;
	self.clientRequestCompletionHandler = nil;	// NOTE: completion handler is not called.
}


#pragma mark - UIApplicationDelegate methods

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{ 
	self.window = [[GDiOS sharedInstance] getWindow];

	self.good = [GDiOS sharedInstance];
	self.good.delegate = self;

	// Show the Good Authentication UI.
	[self.good authorize];

    [[[UIApplication sharedApplication] keyWindow] setTintColor:[UIColor colorWithRed:251./255. green:32./255. blue:37./255. alpha:1.]];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication*)application
{
}

- (void)applicationDidEnterBackground:(UIApplication*)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidEnterBackground object:nil];
    
    if (shouldShowAlertView && !self.clientRequestDTAlert)
    {
        shouldShowAlertView = NO;
        
        DTAlertViewButtonClickedBlock block = ^(DTAlertView *alertView, NSUInteger buttonIndex, NSUInteger cancelButtonIndex){
            if (buttonIndex == cancelButtonIndex) {
                [alertView setDismissAnimationWhenButtonClicked:DTAlertViewAnimationDefault];                
                return;
            }
        };
        
        self.clientRequestDTAlert = [DTAlertView alertViewUseBlock:block
                                                             title:NSLocalizedString(@"Waiting to receive photos", @"Alert title for importing photos")
                                                           message:NSLocalizedString(@"Tap Cancel to stop importing.", @"Alert message for importing photos")
                                                 cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel button")
                                               positiveButtonTitle:nil];
        [self.clientRequestDTAlert performSelector:@selector(show) withObject:nil afterDelay:0.5f];
    }
}

- (void)applicationWillEnterForeground:(UIApplication*)application
{
}

- (void)applicationDidBecomeActive:(UIApplication*)application
{
}

- (void)applicationWillTerminate:(UIApplication*)application
{
}

#pragma mark - GDServiceDelegate methods

- (void)GDServiceDidReceiveFrom:(NSString*)application forService:(NSString*)service withVersion:(NSString*)version forMethod:(NSString*)method withParams:(id)params withAttachments:(NSArray*)attachments forRequestID:(NSString*)requestID
{
	// Check service-specific parameters before processing the call...
	NSError* replyError = nil;

	// Request Files Service...
	if ([service isEqualToString:kRequestFilesServiceName])
	{
		NSInteger maxItems = 0;
		NSInteger maxItemSize = 0;
		NSInteger maxTotalSize = 0;
		NSSet* extensionBlackList = nil;
		NSSet* extensionWhiteList = nil;
		NSString* locationName = nil;

		// Check the method
		if (![method isEqualToString:kRequestFilesMethod])
		{
			NSString* errDescription = [NSString stringWithFormat:@"Method not found \"%@\"",  method];
			replyError = [NSError errorWithDomain:service code:GDServicesErrorMethodNotFound userInfo:@{NSLocalizedDescriptionKey: errDescription}];
		}
		// Check that there are no items in the attachments array.
		else if (attachments && attachments.count > 0)
		{
			NSString* errDescription = [NSString stringWithFormat:@"Attachments should be empty but has %lu", (unsigned long)attachments.count];
			replyError = [NSError errorWithDomain:service code:GDServicesErrorInvalidParams userInfo:@{NSLocalizedDescriptionKey: errDescription}];
		}
		// Check params
		else if (params)
		{
			id value = params[@"maximumItems"];
			if (value) maxItems = [value integerValue];
			
			value = params[@"maximumIndividualSize"];
			if (value) maxItemSize = value ? [value integerValue] : 0;
			
			value = params[@"maximumTotalSize"];
			if (value) maxTotalSize = value ? [value integerValue] : 0;
			
			value = params[@"extensionBlackList"];
			if (value) extensionBlackList = [NSSet setWithArray:value];
			
			value = params[@"extensionWhiteList"];
			if (value) extensionWhiteList = [NSSet setWithArray:value];
			
			locationName = params[@"locationName"];
			
			NSLog(@"Requested files with parameters: %@", params);
			if (locationName) NSLog(@"locationName (%@) is not a supported parameter", locationName);
		}
		
		// Switch to photo browser VC
		if (!replyError)
		{
			self.serviceRequestApplication = application;
			self.serviceRequestID = requestID;
			
			[self.cameraVC showPhotoPickerWithMaxItems:maxItems maxItemSize:maxItemSize maxTotalSize:maxTotalSize extensionWhiteList:extensionWhiteList extensionBlackList:extensionBlackList];
		}
	}
	// Transfer File Service
	else if ([service isEqualToString:kTransferFileServiceName])
	{
		// Check the method
		if (![method isEqualToString:kTransferFileMethod])
		{
			NSString* errDescription = [NSString stringWithFormat:@"Method not found \"%@\"",  method];
			replyError = [NSError errorWithDomain:service code:GDServicesErrorMethodNotFound userInfo:@{NSLocalizedDescriptionKey: errDescription}];
		}
		// Check that there is exactly one item in the attachments array.
		else if (!attachments || attachments.count != 1)
		{
			NSString* errDescription = [NSString stringWithFormat:@"Attachments should have one element but has %lu", (unsigned long)attachments.count];
			replyError = [NSError errorWithDomain:service code:GDServicesErrorInvalidParams userInfo:@{NSLocalizedDescriptionKey: errDescription}];
		}
		// Check that the attachment is of a valid file type.
		else if (![self.cameraVC canSaveFileWithExtension:[attachments[0] pathExtension]])
		{
			NSString* errDescription = [NSString stringWithFormat:@"Unsupported file type: %@", [attachments[0] pathExtension]];
			replyError = [NSError errorWithDomain:service code:2 userInfo:@{NSLocalizedDescriptionKey: errDescription}];
		}
		// Check that there are no params.
		else if (params)
		{
			NSString* errDescription = [NSString stringWithFormat:@"Should have no parameters, but has %@", params];
			replyError = [NSError errorWithDomain:service code:GDServicesErrorInvalidParams userInfo:@{NSLocalizedDescriptionKey: errDescription}];
		}
		
		// Save the file.
		if (!replyError)
		{
			[self saveAttachments:attachments];
			
			// Null response to signal success.
			NSError* error = nil;
			BOOL success = [GDService replyTo:application withParams:nil bringClientToFront:GDEPreferPeerInForeground withAttachments:nil requestID:requestID error:&error];
			
			if (!success)
			{
				NSLog(@"GDService replyTo error: %@", error);
			}
		}
	}
	// Transfer Multiple Files Service
	else if ([service isEqualToString:kTransferMultipleFilesServiceName])
	{
		// Check the method
		if (![method isEqualToString:kTransferMultipleFilesMethod])
		{
			NSString* errDescription = [NSString stringWithFormat:@"Method not found \"%@\"",  method];
			replyError = [NSError errorWithDomain:service code:GDServicesErrorMethodNotFound userInfo:@{NSLocalizedDescriptionKey: errDescription}];
		}
		// Check that there is at least one item in the attachments array.
		else if (!attachments || attachments.count < 1)
		{
			NSString* errDescription = [NSString stringWithFormat:@"Attachments should have one or more elements but has %lu", (unsigned long)attachments.count];
			replyError = [NSError errorWithDomain:service code:GDServicesErrorInvalidParams userInfo:@{NSLocalizedDescriptionKey: errDescription}];
		}
		// Check that there are no params.
		else if (params)
		{
			NSString* errDescription = [NSString stringWithFormat:@"Should have no parameters, but has %@", params];
			replyError = [NSError errorWithDomain:service code:GDServicesErrorInvalidParams userInfo:@{NSLocalizedDescriptionKey: errDescription}];
		}
		
		// Check that the attachments are of a valid file type.
		for (NSString* filePath in attachments)
		{
			if ([self.cameraVC canSaveFileWithExtension:[filePath pathExtension]]) continue;
			
			NSString* errDescription = [NSString stringWithFormat:@"Unsupported file type: %@", filePath];
			replyError = [NSError errorWithDomain:service code:2 userInfo:@{NSLocalizedDescriptionKey: errDescription}];
			break;
		}
		
		// Save the files.
		if (!replyError)
		{
			[self saveAttachments:attachments];
			
			// Null response to signal success.
			NSError* error = nil;
			BOOL success = [GDService replyTo:application withParams:nil bringClientToFront:GDEPreferPeerInForeground withAttachments:nil requestID:requestID error:&error];
			
			if (!success)
			{
				NSLog(@"GDService replyTo error: %@", error);
			}
		}
	}
	// Service not found
	else
	{
		NSString* errDescription = [NSString stringWithFormat:@"Service not found \"%@\"", service];
		replyError = [NSError errorWithDomain:GDServicesErrorDomain code:GDServicesErrorServiceNotFound userInfo:@{NSLocalizedDescriptionKey: errDescription}];
	}
	
	// If error, reply immediately.
	if (replyError)
	{
		NSError* error = nil;
		BOOL success = [GDService replyTo:application withParams:replyError bringClientToFront:GDEPreferPeerInForeground withAttachments:nil requestID:requestID error:&error];
		
		if (!success)
		{
			NSLog(@"GDService replyTo error: %@", error);
		}
	}
}

#pragma mark - GDServiceClientDelegate methods

- (void)GDServiceClientDidStartSendingTo:(NSString*)application withFilename:(NSString*)filename correspondingToRequestID:(NSString*)requestID
{
	// FIXME: Should disable the alert's Cancel button here.
	// This is not possible with a UIAlertView.
}

- (void)GDServiceClientDidFinishSendingTo:(NSString*)application withAttachments:(NSArray*)attachments withParams:(id)params correspondingToRequestID:(NSString*)requestID
{
	// Cancel the modal alert.
    [self.clientRequestDTAlert dismissWithAnimation:DTAlertViewAnimationDefault];
    self.clientRequestDTAlert = nil;
	
	if ([self.clientRequestID isEqualToString:requestID])
	{
		if (self.clientRequestCompletionHandler)
		{
			self.clientRequestCompletionHandler(params, attachments);
		}
		
		self.clientRequestID = nil;
		self.clientRequestApplication = nil;
		self.clientRequestCompletionHandler = nil;
	}
	else
	{
		NSLog(@"Unable to handle overlapping service requests.");
	}
}

- (void)GDServiceClientDidReceiveFrom:(NSString*)application withParams:(id)params withAttachments:(NSArray*)attachments correspondingToRequestID:(NSString*)requestID
{
	if ([self.clientRequestID isEqualToString:requestID])
	{
		NSArray* filePaths = @[];
		
		if (attachments)
		{
			filePaths = [self saveAttachments:attachments];
		}
		
		if (self.clientRequestCompletionHandler)
		{
			self.clientRequestCompletionHandler(params, filePaths);
		}
		
		self.clientRequestID = nil;
		self.clientRequestApplication = nil;
		self.clientRequestCompletionHandler = nil;
		
		[self.cameraVC.navigationController popViewControllerAnimated:YES];
	}
	else
	{
		NSLog(@"Unable to handle overlapping service requests.");
	}
}

// Returns array of local file paths.
- (NSArray*)saveAttachments:(NSArray*)attachments
{
	NSMutableArray* localPaths = [NSMutableArray arrayWithCapacity:attachments.count];
	
	// All service client responses are handled by saving that attached files.
	for (NSString* filePath in attachments)
	{
		if (![self.cameraVC canSaveFileWithExtension:[filePath pathExtension]]) continue;
		
		NSError* error = nil;
		NSData* fileData = [GDFileSystem readFromFile:filePath error:&error];
		NSDate* fileDate = nil;
		
		if (!error)
		{
			GDFileStat stat = {0};
			[GDFileSystem getFileStat:filePath to:&stat error:&error];
			fileDate = [NSDate dateWithTimeIntervalSince1970:stat.lastModifiedTime];
		}
		
		if (!error)
		{
			NSString* path = [self.cameraVC saveImageWithData:fileData extension:[filePath pathExtension] forDate:fileDate];
			if (path)
			{
				[localPaths addObject:path];
			}
		}
		else
		{
			NSLog(@"Error saving file: %@", error);
		}
	}
	
	return localPaths;
}

#pragma mark - GDiOSDelegate methods

-(void)handleEvent:(GDAppEvent*)anEvent
{
	// Called from _good when events occur, such as system startup.
	switch (anEvent.type)
	{
		case GDAppEventAuthorized:
			[self onAuthorized:anEvent];
			break;
		
		case GDAppEventNotAuthorized:
			[self onNotAuthorized:anEvent];
			break;
		
		case GDAppEventRemoteSettingsUpdate:
			//A change to application-related configuration or policy settings.
			break;
		
		case GDAppEventServicesUpdate:
			//A change to services-related configuration.
			break;
		
		case GDAppEventPolicyUpdate:
			//A change to one or more application-specific policy settings has been received.
			break;
	}
}

-(void)onNotAuthorized:(GDAppEvent*)anEvent
{
	// Handle the Good Libraries not authorized event.
	switch (anEvent.code)
	{
		case GDErrorActivationFailed:
		case GDErrorProvisioningFailed:
		case GDErrorPushConnectionTimeout:
		case GDErrorSecurityError:
		case GDErrorAppDenied:
		case GDErrorBlocked:
		case GDErrorWiped:
		case GDErrorRemoteLockout: 
		case GDErrorPasswordChangeRequired:
			// an condition has occured denying authorization, an application may wish to log these events
			NSLog(@"onNotAuthorized %@", anEvent.message);
			break;
		
		case GDErrorIdleLockout:
			// idle lockout is benign & informational
			break;
			
		default: 
			NSAssert(false, @"Unhandled not authorized event");
			break;
	}
}

-(void)onAuthorized:(GDAppEvent*)anEvent 
{
	// Handle the Good Libraries authorized event.
	switch (anEvent.code)
	{
		case GDErrorNone:
			if (!started)
			{
				// launch application UI here
				started = YES;
				NSString *thumbPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"tn"];
				
				[[SZLSecureFileManager defaultManager] createDirectoryAtPath:thumbPath withIntermediateDirectories:YES attributes:nil error:nil];
				
				self.window.backgroundColor = [UIColor whiteColor];
				KRCCameraViewController* cameraVC = [[KRCCameraViewController alloc] initWithNibName:@"KRCCameraView" bundle:[KRCCameraConstants resourceBundle]];
				UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:cameraVC];
				self.cameraVC = cameraVC;
				
				self.window.rootViewController = nc;
				[self.window makeKeyAndVisible];

				self.service = [[GDService alloc] init];
				self.service.delegate = self;

				self.serviceClient = [[GDServiceClient alloc] init];
				self.serviceClient.delegate = self;
			}
			break;
		
		default:
			NSAssert(false, @"Authorized startup with an error");
			break;
	}
}

@end

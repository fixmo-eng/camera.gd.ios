//
//  GoodSecureFileManager.m
//  SZSecurity
//
//  Created by Mike Wang on 2014-07-2.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "GoodSecureFileManager.h"
#import <GD/GDFileSystem.h>


static GoodSecureFileManager *_ao_defaultManager;
static NSData *_fkey;

NSData* fetchFileKey();

@implementation GoodSecureFileManager

+ (GoodSecureFileManager *)defaultManager
{
    if (!_ao_defaultManager) {
        _ao_defaultManager = [[[self class] alloc] init];
    }
    
    return _ao_defaultManager;
}

+ (NSString *) createUniquePathForFile:(NSString *)name in:(NSString *)directory
{
	NSString *toPath = [directory stringByAppendingPathComponent:name];
	if (![[GoodSecureFileManager defaultManager] fileExistsAtPath:toPath]) return toPath; // too easy
    
	// now go through and add an appendix to it
	NSString *fileBase = [name stringByDeletingPathExtension];
	NSString *fileExtension = [name pathExtension];
    
	for (int fileIndex=1; fileIndex<1000; fileIndex++) {
		NSString *fileName = [NSString stringWithFormat:@"%@_%d", fileBase, fileIndex];
		fileName = [fileName stringByAppendingPathExtension:fileExtension];
		toPath = [directory stringByAppendingPathComponent:fileName];
		if (![[GoodSecureFileManager defaultManager] fileExistsAtPath:toPath]) break;
	}
    
	return toPath;
}

- (NSDictionary*)attributesOfItemAtPath:(NSString*)path error:(NSError **)error
{
	@throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"You must use the fileSizeForPath: or modificationDateForPath: methods instead." userInfo:nil];
}

- (size_t)fileSizeForPath:(NSString*)path
{
	GDFileStat stat;
	NSError* error = nil;
	BOOL success = [GDFileSystem getFileStat:path to:&stat error:&error];
	return success ? (size_t)stat.fileLen : 0;
}

- (NSDate*)modificationDateForPath:(NSString*)path
{
	GDFileStat stat;
	NSError* error = nil;
	BOOL success = [GDFileSystem getFileStat:path to:&stat error:&error];
	return success ? [NSDate dateWithTimeIntervalSince1970:stat.lastModifiedTime] : nil;
}

- (BOOL)fileExistsAtPath:(NSString *)path {
	return [GDFileSystem fileExistsAtPath:path isDirectory:NO];
}

- (BOOL)fileExistsAtPath:(NSString *)path isDirectory:(BOOL *)isDirectory {
	return [GDFileSystem fileExistsAtPath:path isDirectory:isDirectory];
}

- (BOOL)createDirectoryAtPath:(NSString *)path withIntermediateDirectories:(BOOL)createIntermediates attributes:(NSDictionary *)attributes error:(NSError **)error {
	return [GDFileSystem createDirectoryAtPath:path withIntermediateDirectories:createIntermediates attributes:attributes error:error];
}

- (BOOL)createFileAtPath:(NSString *)path contents:(NSData *)data attributes:(NSDictionary *)attr {
	return [GDFileSystem writeToFile:data name:path error:nil];
}

- (NSData *)contentsAtPath:(NSString *)path {
	return [GDFileSystem readFromFile:path error:nil];
}

- (NSArray *)contentsOfDirectoryAtPath:(NSString *)path error:(NSError **)error {
	return [GDFileSystem contentsOfDirectoryAtPath:path error:error];
}

- (BOOL)copyItemAtPath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)error {
	NSData *fileData = [GDFileSystem readFromFile:srcPath error:error];
	if (!error && fileData) {
		return [GDFileSystem writeToFile:fileData name:dstPath error:error];
	}
	
	return NO;
}

- (BOOL)moveItemAtPath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)error {
	return [GDFileSystem moveItemAtPath:srcPath toPath:dstPath error:error];
}

- (BOOL)removeItemAtPath:(NSString *)path error:(NSError **)error {
	return [GDFileSystem removeItemAtPath:path error:error];
}

@end

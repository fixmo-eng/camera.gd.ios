//
//  GDCameraAppDelegate.h
//  GoodCamera
//
//  Created by Mike Wang on 2014-06-30.
//  Copyright (c) 2014 KreevRoo Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GD/GDiOS.h>
#import "AppSelectorViewController.h"

@interface GDCameraAppDelegate : UIResponder <UIApplicationDelegate, GDiOSDelegate, AppSelectorDelegate>

@property (strong, nonatomic) UIWindow *window;

// Flag to check whether a sendEmail service is available.
- (BOOL)canEmailPhotos;

// Use the sendEmail service to send a list of photos as attachments.
- (void)emailPhotos:(NSArray*)photos fromBarButton:(UIBarButtonItem*)barButton;

// Flag to check whether a requestFiles service is available.
- (BOOL)canImportImages;

// Use the requestFiles service to retrieve list of photos as attachments.
- (void)importImagesFromBarButton:(UIBarButtonItem*)barButton withCompletionHandler:(void(^)(id params, NSArray* filePaths))completionHandler;

// Flag to check whether a transferFiles service is available.
- (BOOL)canTransferPhotosWithCount:(NSInteger)count;

// Use the transferFiles service to send a list of photos as attachments.
- (void)transferPhotos:(NSArray*)photos fromBarButton:(UIBarButtonItem*)barButton;

// Callback to provide requested photos as a service response.
// Send with nil argument to indicate that the user cancelled.
- (void)provideRequestedPhotos:(NSArray*)photos;

@end

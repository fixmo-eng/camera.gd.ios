//
//  main.m
//  PowerCam
//
//  Created by Mike Wang on 2014-06-30.
//  Copyright (c) 2014 KreevRoo Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GD/GDiOS.h>
#import "GDCameraAppDelegate.h"

int main(int argc, char* argv[])
{
    @autoreleasepool {
        [GDiOS initializeWithClassNameConformingToUIApplicationDelegate:@"GDCameraAppDelegate"];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GDCameraAppDelegate class]));
    }
}





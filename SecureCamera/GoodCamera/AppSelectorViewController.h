/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import <UIKit/UIKit.h>

@class GDAppDetail;

@protocol AppSelectorDelegate

- (void)appSelected:(GDAppDetail*)appDetails;

@end


@interface AppSelectorViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

- (instancetype)initWithAppList:(NSArray*)appList;

@property (nonatomic, unsafe_unretained) id <AppSelectorDelegate> delegate;

@end

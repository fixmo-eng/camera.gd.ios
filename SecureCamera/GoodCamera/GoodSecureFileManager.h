//
//  GoodSecureFileManager.h
//  SZSecurity
//
//  Created by Mike Wang on 2014-07-2.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodSecureFileManager : NSFileManager
+ (GoodSecureFileManager *)defaultManager;

- (size_t)fileSizeForPath:(NSString*)path;
- (NSDate*)modificationDateForPath:(NSString*)path;

@end

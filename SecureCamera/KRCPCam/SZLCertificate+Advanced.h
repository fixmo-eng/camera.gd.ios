//
//  SZLCertificate+Advanced.h
//  SZSecurity
//
//  Created by Anluan O'Brien on 12-05-14.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZLCertificate.h"
#include <openssl/x509.h>

@interface SZLCertificate (Advanced)

-(X509*)X509Handle;

@end
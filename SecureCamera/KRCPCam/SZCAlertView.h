//
//  SZCAlertView.h
//  SafeZone
//
//  Created by Anluan O'Brien on 12-04-27.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SZCAlertView : UIView
{
    UIColor *_bgColor;
}
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
@property (nonatomic, strong) UILabel *message;
@property (nonatomic, strong) UILabel *title;

+(SZCAlertView*)setupProgressViewOn:(UIView*)view;
-(void)setMessageText:(NSString *)messageIn;
-(void)hide;

@end

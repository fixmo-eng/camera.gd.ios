//
//  SZLSecureFileManager_SZL.h
//  SZSecurity
//
//  Created by Anluan O'Brien on 2012-06-19.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZLSecureFileManagerr.h"

@interface SZLSecureFileManager (SZL)

+(NSString*)pathForHashedPath:(NSString*)path;
+(NSString*)hashedPathForPath:(NSString *)path;
+(void)encryptData:(NSData*)data toFileAtPath:(NSString*)path;
+(NSString *)decryptDataFromFileAtPath:(NSString *)path;
+(void)cleanup;

@end

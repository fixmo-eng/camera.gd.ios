//
//  SZLSecureDatabase+SZL.h
//  SZSecurity
//
//  Created by Anluan O'Brien on 12-04-18.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZLSecureDatabase.h"

@interface SZLSecureDatabase (SZL)
+(SZLSecureDatabase*)openSZLContainerSettings;
@end
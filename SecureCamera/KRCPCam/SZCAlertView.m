//
//  SZCAlertView.m
//  SafeZone
//
//  Created by Anluan O'Brien on 12-04-27.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZCAlertView.h"
#import <QuartzCore/QuartzCore.h>

#define SZCALERTMINHEIGHT 50.0f

@implementation SZCAlertView

@synthesize spinner;
@synthesize message;
@synthesize title;

- (id)initWithFrame:(CGRect)frame
{
    if (frame.size.height < SZCALERTMINHEIGHT) {
        frame.size.height = SZCALERTMINHEIGHT;
    }
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        [super setBackgroundColor:[UIColor clearColor]];
        self.layer.shadowOffset = CGSizeMake(0,0);
        self.layer.shadowRadius = 15.0f;
        self.layer.shadowOpacity = .6f;
        
        title = [[UILabel alloc] initWithFrame:CGRectZero];
        title.textAlignment = NSTextAlignmentCenter;
        title.textColor = [UIColor whiteColor];
        title.backgroundColor = [UIColor clearColor];
        title.font = [UIFont boldSystemFontOfSize:14];
        title.lineBreakMode = NSLineBreakByWordWrapping;
        title.numberOfLines = 2;
        
        message = [[UILabel alloc] initWithFrame:CGRectZero];
        message.textAlignment = NSTextAlignmentCenter;
        message.textColor = [UIColor whiteColor];
        message.backgroundColor = [UIColor clearColor];
        message.font = [UIFont systemFontOfSize:14];
        message.lineBreakMode = NSLineBreakByWordWrapping;
        message.numberOfLines = 0;
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [self addSubview:title];
        [self addSubview:message];
        [self addSubview:spinner];
    }
    return self;
}

-(void)layoutSubviews
{
    self.title.frame = CGRectMake (10,5,self.bounds.size.width - 20, 36);
    CGPoint realCenter = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    CGFloat spinnerSize = roundf(self.frame.size.width * .15f);
    CGRect spinnerFrame = CGRectMake(realCenter.x - spinnerSize/2,10 + self.title.frame.size.height,spinnerSize,spinnerSize);
    self.spinner.frame = spinnerFrame;
    self.message.frame = CGRectMake (10,self.bounds.size.height * .45f,self.bounds.size.width - 20, self.bounds.size.height * .5f);
}

-(void)setBackgroundColor:(UIColor *)backgroundColor
{
    _bgColor = backgroundColor;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:10];
    [[_bgColor colorWithAlphaComponent:.4f] setFill];
    [path fill];
    [[UIColor whiteColor] setStroke];
    [path setLineWidth:2.0f];
    [path stroke];
}

-(void)setMessageText:(NSString *)messageIn
{
    [self.spinner startAnimating];
    self.message.text = [messageIn copy];
    self.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.alpha = 1.0f;
    }];
}

#pragma mark - Progress View
// FIXME: do this better. Time constrained

+(SZCAlertView*)setupProgressViewOn:(UIView*)view
{
    if (view) {
        SZCAlertView* alertView = [[SZCAlertView alloc] initWithFrame:CGRectMake(0, 0, 180, 120)];
        alertView.center = CGPointMake(CGRectGetMidX(view.bounds),CGRectGetMidY(view.bounds));
        alertView.backgroundColor = [UIColor blackColor];
        alertView.hidden = NO;
        alertView.alpha = 0.0f;
        [view addSubview:alertView];
        return alertView;
    }
    return nil;
}

-(void)hide
{
    [UIView animateWithDuration:0.3f animations:^{
        self.alpha = 0.0f;
    } completion:^(BOOL finished) {
        self.hidden = YES;
        [self.spinner stopAnimating];
    }];
    
}

@end

//
//  KRCCameraConstants.h
//  PowerCam
//
//  Created by Magali Boizot-Roche on 2013-07-18.
//  Copyright (c) 2013 KreevRoo Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KRCCameraConstants : NSObject

+(NSBundle *)resourceBundle;
+ (CGFloat) thumbnailPixelSize;

@end

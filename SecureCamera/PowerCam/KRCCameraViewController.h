//
//  KRCCameraViewController.h
//  PowerCam
//
//  Created by Anluan O'Brien on 12/27/10.
//  Copyright (c) 2010 KreevRoo Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZLPhotoBrowserVC.h"

@class SZLPhotoBrowserVC;

@interface KRCCameraViewController : UIViewController<SZLPhotoBrowserDelegate>

@property (strong, nonatomic) IBOutlet UIView *cameraView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIToolbar *topToolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cameraRollButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *recordBarButtonItem;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *mediaSelectButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *switchCameraButtonItem;
@property (strong, nonatomic) IBOutlet UIButton *toggleFlashButton;

@property (nonatomic,strong) SZLPhotoBrowserVC* photoBrowser;

- (IBAction)showCameraRoll:(id)sender;
- (IBAction)toggleRecording:(id)sender;
- (IBAction)toggleMediaStyle:(id)sender;
- (IBAction)switchCamera:(id)sender;
- (IBAction)toggleFlash:(id)sender;

- (void)showPhotoPickerWithMaxItems:(NSInteger)maxItems maxItemSize:(NSInteger)maxItemSize maxTotalSize:(NSInteger)maxTotalSize extensionWhiteList:(NSSet*)extensionWhiteList extensionBlackList:(NSSet*)extensionBlackList;

- (BOOL)canSaveFileWithExtension:(NSString*)extension;
- (NSString*)saveImageWithData:(NSData*)data extension:(NSString*)extension forDate:(NSDate*)date;

@end

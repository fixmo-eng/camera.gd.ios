//
//  KRCCameraViewController.m
//  PowerCam
//
//  Created by Anluan O'Brien on 12/27/10.
//  Copyright (c) 2010 KreevRoo Consulting. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <ImageIO/ImageIO.h>
#include <libkern/OSAtomic.h>
#include <tgmath.h>

#import "KRCCameraViewController.h"
#import "SZLThumbnailGenerator.h"
#import "SZLPhotoBrowserVC.h"
#import "SZCAlertView.h"
#import "SZLFile.h"
#import "SZLFileImage.h"
#import "MWPhotoProtocol.h"

#import "KRCCameraConstants.h"
#import "GDCameraAppDelegate.h"

#import "GoodSecureFileManager.h"

#define IS_IPAD() ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)

static NSDateFormatter *dateFormatter;
static volatile int32_t thumbQueueDepth = 0;

@interface KRCCameraViewController ()

@property (nonatomic,strong) AVCaptureSession *captureSession;
@property (nonatomic,strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic,weak) AVCaptureDeviceInput *currentInput;
@property (nonatomic,readwrite) BOOL isRecording;
@property (nonatomic,readwrite) BOOL isMovie;

@property (nonatomic,strong) AVCaptureStillImageOutput *pictureOutput;
@property (nonatomic,strong) AVCaptureVideoDataOutput *movieOutput;
@property (nonatomic,strong) AVCaptureDeviceInput *frontCameraInput;
@property (nonatomic,strong) AVCaptureDeviceInput *backCameraInput;
@property (nonatomic,strong) SZCAlertView *progressView;

@property (nonatomic,strong) UIView *focussingBox;
@property (nonatomic) AVCaptureFlashMode currentFlashMode;

@property (nonatomic,strong) UIView *shutterView;

@end

@implementation KRCCameraViewController

+ (dispatch_queue_t) thumbQueue
{
    static dispatch_queue_t thumbQueue;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        thumbQueue = dispatch_queue_create("com.good.gd.camera.thumbnail", DISPATCH_QUEUE_SERIAL);
    });
    return thumbQueue;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.focussingBox = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"KRCCameraRsrc.bundle/FocusCrosshairs1"]];
        self.currentFlashMode = AVCaptureFlashModeAuto;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.navigationController.toolbarHidden = YES;
	self.photoBrowser = nil;
    
    self.focussingBox.alpha = 0;
    self.focussingBox.center = self.cameraView.center;
    [self.view addSubview:self.focussingBox];
    [self updateMediaSelectButton];
    NSError *error = nil;
    
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    DLog(@"avail devices: %@",devices);
    self.captureSession = [[AVCaptureSession alloc] init];
    [self.captureSession setSessionPreset:AVCaptureSessionPresetPhoto];
    
    for (AVCaptureDevice *device in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
        if ([device position] == AVCaptureDevicePositionFront) {
            self.frontCameraInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
            if (!self.frontCameraInput) {
                DLog(@"Error setting up front camera: %@",error);
            }
        } else {
            self.backCameraInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
            if (!self.backCameraInput) {
                DLog(@"Error setting up back camera: %@",error);
            }
        }
    }
    
    if (!self.backCameraInput) {
        DLog(@"Couldn't get device: %@",error);
    } else {
        [self.captureSession addInput:self.backCameraInput];
    }
    
    self.currentInput = self.backCameraInput;
    
    self.pictureOutput = [AVCaptureStillImageOutput new];
    if ( [self.captureSession canAddOutput:self.pictureOutput] )
        [self.captureSession addOutput:self.pictureOutput];
    
    UITapGestureRecognizer *focusTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(focusTapReceived:)];
    [self.cameraView addGestureRecognizer:focusTapGestureRecognizer];
    
    CALayer *viewLayer = [self.cameraView layer];
    
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    
    [self.previewLayer setBackgroundColor:[[UIColor blackColor] CGColor]];
    [self.previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.previewLayer setMasksToBounds:YES];
    [self.previewLayer setFrame:[viewLayer bounds]];
    self.previewLayer.connection.videoOrientation = [self previewOrientationForDeviceOrientation];
    
    [viewLayer addSublayer:self.previewLayer];
    [self.captureSession startRunning];
    
    if ([[self.currentInput device] isFlashAvailable]) {
        self.toggleFlashButton.hidden = NO;
        [self setFlashMode:self.currentFlashMode];
    } else {
        self.toggleFlashButton.hidden = YES;
    }

    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm:ss.SSSS a"];
    }

    self.shutterView = [[UIView alloc] initWithFrame:[self.cameraView frame]];
    self.shutterView.backgroundColor = [UIColor clearColor];
    self.shutterView.opaque = NO;
    self.shutterView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.shutterView.layer.opacity = 0.0f;
    [self.cameraView addSubview:self.shutterView];
}

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
	[self.previewLayer removeFromSuperlayer];
    self.previewLayer.session = nil;
    self.previewLayer = nil;
    [self.captureSession stopRunning];
    self.captureSession = nil;
	[super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD()) {
        return YES;
    }
    return interfaceOrientation == UIInterfaceOrientationPortrait;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.previewLayer setFrame:[[self.cameraView layer] bounds]];
    self.previewLayer.connection.videoOrientation = [self previewOrientationForDeviceOrientation];
}

- (IBAction)showCameraRoll:(id)sender
{
	if (thumbQueueDepth > 0)
	{
		[self showProgressViewWithTitle:SecureCamLocalizedStringFromTable(@"Saving photos", @"securecam", @"Saving photos")];
	}

	dispatch_async([KRCCameraViewController thumbQueue],
	^{
		// thumbQueue is a serial queue, so this block will execute after everything else is done.
		dispatch_async(dispatch_get_main_queue(),
		^{
			[self hideProgressView];

			if (!self.photoBrowser && self.navigationController.topViewController == self)
			{
				SZLPhotoBrowserVC* photoBrowser = [[SZLPhotoBrowserVC alloc] initWithDelegate:self];
				photoBrowser.canDelete = YES;
				photoBrowser.canTransfer = YES;
				photoBrowser.canEmail = [(GDCameraAppDelegate*)[UIApplication sharedApplication].delegate canEmailPhotos];
				photoBrowser.canImport = [(GDCameraAppDelegate*)[UIApplication sharedApplication].delegate canImportImages];
				self.photoBrowser = photoBrowser;
				[self.navigationController pushViewController:self.photoBrowser animated:YES];
			}
		});
	});
}

- (void)showPhotoPickerWithMaxItems:(NSInteger)maxItems maxItemSize:(NSInteger)maxItemSize maxTotalSize:(NSInteger)maxTotalSize extensionWhiteList:(NSSet*)extensionWhiteList extensionBlackList:(NSSet*)extensionBlackList
{
	if (thumbQueueDepth > 0)
	{
		[self showProgressViewWithTitle:SecureCamLocalizedStringFromTable(@"Saving photos", @"securecam", @"Saving photos")];
	}

	dispatch_async([KRCCameraViewController thumbQueue],
	^{
		// thumbQueue is a serial queue, so this block will execute after everything else is done.
		dispatch_async(dispatch_get_main_queue(),
		^{
			[self hideProgressView];
			[self.navigationController popViewControllerAnimated:NO];

			self.photoBrowser = [[SZLPhotoBrowserVC alloc] initWithDelegate:self];
			[self.navigationController pushViewController:self.photoBrowser animated:YES];
			
			self.photoBrowser.canDelete = NO;
			self.photoBrowser.canEmail = NO;
			self.photoBrowser.canImport = NO;
			self.photoBrowser.canTransfer = NO;
			self.photoBrowser.canSelect = YES;
			
			[self.photoBrowser setExtensionWhitelist:extensionWhiteList extensionBlacklist:extensionBlackList];
			self.photoBrowser.maxItems = maxItems;
			self.photoBrowser.maxItemSize = maxItemSize;
			self.photoBrowser.maxTotalSize = maxTotalSize;
			
			[self.photoBrowser.view setNeedsLayout];
		});
	});
}

- (IBAction)toggleRecording:(id)sender
{
    AVCaptureConnection *stillImageConnection = [self.pictureOutput connectionWithMediaType:AVMediaTypeVideo];
    AVCaptureVideoOrientation avcaptureOrientation = [self captureOrientationForDeviceOrientation];
    [stillImageConnection setVideoOrientation:avcaptureOrientation];
    [stillImageConnection setVideoScaleAndCropFactor:1.0];

    [self.pictureOutput setOutputSettings:[NSDictionary dictionaryWithObject:AVVideoCodecJPEG
                                                                 forKey:AVVideoCodecKey]];

    [self playShutterAnimation];

    [self.pictureOutput captureStillImageAsynchronouslyFromConnection:stillImageConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError* error)
	{

		if (error)
		{
			DLog(@"Error while capturing still image: (d=%@,c=%d)", error.domain, (int)error.code);
			return;
		}

		NSData* jpegData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
		[self saveCameraData:jpegData];
	}];
}

- (IBAction)toggleMediaStyle:(id)sender {
    self.isMovie = !self.isMovie;
    [self updateMediaSelectButton];
}

-(void)updateMediaSelectButton
{
    self.mediaSelectButton.title = (self.isMovie) ?
        SecureCamLocalizedStringFromTable(@"Movie", @"securecam", @"Movie"):
        SecureCamLocalizedStringFromTable(@"Still", @"securecam", @"Still");
}

-(CGPoint)convertViewCoordinatesToFocalPoint:(CGPoint)viewCoordinates
{
    CGPoint focalPoint = CGPointMake(.5f, .5f);
    CGSize frameSize = [self.cameraView frame].size;
    
    if (self.previewLayer.connection.videoMirrored) {
        viewCoordinates.x = frameSize.width - viewCoordinates.x;
    }
    
    for (AVCaptureInputPort *port in [[self currentInput] ports]) {
        if ([port mediaType] == AVMediaTypeVideo) {
            CGRect cleanAperture = CMVideoFormatDescriptionGetCleanAperture([port formatDescription], YES);
            CGSize apertureSize = cleanAperture.size;
            CGPoint point = viewCoordinates;
            
            CGFloat apertureRatio = apertureSize.height / apertureSize.width;
            CGFloat viewRatio = frameSize.width / frameSize.height;
            CGFloat xc = .5f;
            CGFloat yc = .5f;
            
            // Scale, switch x and y, and reverse x
            if (viewRatio > apertureRatio) {
                CGFloat y2 = apertureSize.width * (frameSize.width / apertureSize.height);
                xc = (point.y + ((y2 - frameSize.height) / 2.f)) / y2; // Account for cropped height
                yc = (frameSize.width - point.x) / frameSize.width;
            } else {
                CGFloat x2 = apertureSize.height * (frameSize.height / apertureSize.width);
                yc = 1.f - ((point.x + ((x2 - frameSize.width) / 2)) / x2); // Account for cropped width
                xc = point.y / frameSize.height;
            }
            focalPoint = CGPointMake(xc, yc);
            //break;
        }
    }
    return focalPoint;
}

-(void)focusTapReceived:(UIGestureRecognizer*)tapGesture
{
    CGPoint touchPoint = [tapGesture locationInView:tapGesture.view];
    
    CGPoint focusPoint = [self convertViewCoordinatesToFocalPoint:touchPoint];
    
    AVCaptureDevice *device = [self.currentInput device];
    if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [self showAutoFocusBoxAtPoint:touchPoint];
            [device setFocusPointOfInterest:focusPoint];
            [device setFocusMode:AVCaptureFocusModeAutoFocus];
            [device unlockForConfiguration];
            [self hideAutoFocusBox];
        } else {
            // handle focus error here.
        }
    }
    
    if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [self showAutoFocusBoxAtPoint:touchPoint];
            [device setExposurePointOfInterest:focusPoint];
            [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
            [device unlockForConfiguration];
            [self hideAutoFocusBox];
        } else {
            // handle focus error here.
        }
    }

}

-(void)showAutoFocusBoxAtPoint:(CGPoint)focusTouchPoint
{
    self.focussingBox.center = focusTouchPoint;
    
    [UIView animateWithDuration:1.0f animations:^{
        self.focussingBox.alpha = 1;
    }];
}

-(void)hideAutoFocusBox
{
    [UIView animateWithDuration:1.0f animations:^{
        self.focussingBox.alpha = 0;
    }];
}

- (IBAction)switchCamera:(id)sender
{
    if (self.currentInput == self.frontCameraInput) {
        [self.captureSession beginConfiguration];
        [self.captureSession removeInput:self.frontCameraInput];
        [self.captureSession addInput:self.backCameraInput];
        [self.captureSession commitConfiguration];
        self.currentInput = self.backCameraInput;
    } else {
        [self.captureSession beginConfiguration];
        [self.captureSession removeInput:self.backCameraInput];
        [self.captureSession addInput:self.frontCameraInput];
        [self.captureSession commitConfiguration];
        self.currentInput = self.frontCameraInput;
    }
    
    if ([[self.currentInput device] isFlashAvailable]) {
        self.toggleFlashButton.hidden = NO;
    } else {
        self.toggleFlashButton.hidden = YES;
    }
}

- (IBAction)toggleFlash:(id)sender
{
    switch (self.currentFlashMode) {
        case AVCaptureFlashModeAuto:
            self.currentFlashMode = AVCaptureFlashModeOff;
            [self.toggleFlashButton setTitle:SecureCamLocalizedStringFromTable(@"Off", @"securecam", @"Off") forState:UIControlStateNormal];
            break;
        case AVCaptureFlashModeOff:
            self.currentFlashMode = AVCaptureFlashModeOn;
            [self.toggleFlashButton setTitle:SecureCamLocalizedStringFromTable(@"On", @"securecam", @"On") forState:UIControlStateNormal];
            break;
        case AVCaptureFlashModeOn:
            self.currentFlashMode = AVCaptureFlashModeAuto;
            [self.toggleFlashButton setTitle:SecureCamLocalizedStringFromTable(@"Auto", @"securecam", @"Auto") forState:UIControlStateNormal];
            break;
    }
    [self setFlashMode:self.currentFlashMode];
}

-(void)setFlashMode:(AVCaptureFlashMode)mode
{
    AVCaptureDevice *device = [self.currentInput device];
    if ([device hasFlash] && [device isFlashAvailable]) {
        NSError *error = nil;
        if ([device lockForConfiguration:&error]) {
            [device setFlashMode:mode];
            [device unlockForConfiguration];
        } else {
            DLog(@"Problem locking device to set flash: %@",error);
        }
    }
}

-(AVCaptureVideoOrientation)previewOrientationForDeviceOrientation
{
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    AVCaptureVideoOrientation orientation = AVCaptureVideoOrientationPortrait;
    
    if (IS_IPAD()) {
        
        if (deviceOrientation == UIDeviceOrientationPortrait)
            orientation = AVCaptureVideoOrientationPortrait;
        else if (deviceOrientation == UIDeviceOrientationPortraitUpsideDown)
            orientation = AVCaptureVideoOrientationPortraitUpsideDown;
        
        // AVCapture and UIDevice have opposite meanings for landscape left and right (AVCapture orientation is the same as UIInterfaceOrientation)
        else if (deviceOrientation == UIDeviceOrientationLandscapeLeft)
            orientation = AVCaptureVideoOrientationLandscapeRight;
        else if (deviceOrientation == UIDeviceOrientationLandscapeRight)
            orientation = AVCaptureVideoOrientationLandscapeLeft;
    }
    return orientation;
}

-(AVCaptureVideoOrientation)captureOrientationForDeviceOrientation
{
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    AVCaptureVideoOrientation orientation = AVCaptureVideoOrientationPortrait;
    
    if (deviceOrientation == UIDeviceOrientationPortrait)
        orientation = AVCaptureVideoOrientationPortrait;
    else if (deviceOrientation == UIDeviceOrientationPortraitUpsideDown)
        orientation = AVCaptureVideoOrientationPortraitUpsideDown;
    
    // AVCapture and UIDevice have opposite meanings for landscape left and right (AVCapture orientation is the same as UIInterfaceOrientation)
    else if (deviceOrientation == UIDeviceOrientationLandscapeLeft)
        orientation = AVCaptureVideoOrientationLandscapeRight;
    else if (deviceOrientation == UIDeviceOrientationLandscapeRight)
        orientation = AVCaptureVideoOrientationLandscapeLeft;

    return orientation;
}

- (void)saveCameraData:(NSData*)jpegData
{
    [self saveImageWithData:jpegData extension:@"jpg" forDate:[NSDate date]];
}

- (BOOL)canSaveFileWithExtension:(NSString*)extension
{
	return [[[SZLFileImage supportedExtensions] allKeys] containsObject:[extension lowercaseString]];
}

- (NSString*)saveImageWithData:(NSData*)data extension:(NSString*)extension forDate:(NSDate*)date
{
    NSString *imageBasename = [dateFormatter stringFromDate:date];

    NSString* documentsFolder  = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* photosFolder     = [documentsFolder stringByAppendingPathComponent:@"photos"];
    NSString* thumbnailsFolder = [photosFolder stringByAppendingPathComponent:@"thumbnails"];

    NSString* imageFilename = [photosFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",imageBasename, extension]];
    NSString* thumbFilename = [thumbnailsFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageBasename]];

    id fileManager = [SZLSecureFileManager defaultManager];

    BOOL bOK;
    NSError* createErr;

    bOK = [fileManager createDirectoryAtPath:photosFolder withIntermediateDirectories:YES attributes:nil error:&createErr];
    if ( !bOK ) {
        if ( createErr ) DLog(@"Failure creating photos folder: %@", createErr);
        return nil;
    }

    bOK = [fileManager createDirectoryAtPath:thumbnailsFolder withIntermediateDirectories:YES attributes:nil error:&createErr];
    if ( !bOK ) {
        if ( createErr ) DLog(@"Failure creating thumbnails folder: %@", createErr);
        return nil;
    }

    bOK = [fileManager createFileAtPath:imageFilename contents:data attributes:nil];
    if ( !bOK ) {
        DLog(@"Failure creating image file.");

        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *av = [[UIAlertView alloc]
                               initWithTitle: SecureCamLocalizedStringFromTable(@"Error Saving", @"securecam", @"Error Saving")
                               message:SecureCamLocalizedStringFromTable(@"There was a problem saving your image.", @"securecam", @"There was a problem saving your image.")
                               delegate:nil
                               cancelButtonTitle:SecureCamLocalizedStringFromTable(@"OK", @"securecam", @"OK")
                               otherButtonTitles:nil];
            [av show];
        });

        return nil;
    }

    OSAtomicAdd32(1, &thumbQueueDepth);
    dispatch_async([KRCCameraViewController thumbQueue], ^{
        @autoreleasepool {
            [self saveThumbnailToPath:thumbFilename forImageAtPath:imageFilename];
            OSAtomicAdd32(-1, &thumbQueueDepth);
			
			dispatch_async(dispatch_get_main_queue(),
			^{
				[self.photoBrowser reloadData];
			});
        }
    });
	
	return imageFilename;
}

- (void) saveThumbnailToPath:(NSString*)thumbFilename forImageAtPath:(NSString*)imageFilename
{
    id fileManager = [SZLSecureFileManager defaultManager];

    NSData* fullData = [fileManager contentsAtPath:imageFilename];

    if ( fullData ) {
        NSTimeInterval tThumbStart = [NSDate date].timeIntervalSinceReferenceDate;
		CGFloat thumbSize = [KRCCameraConstants thumbnailPixelSize];
        NSData* thumbData = [SZLThumbnailGenerator thumbnailDataForImageWithData:fullData forSize:thumbSize];
        NSTimeInterval tThumbEnd = [NSDate date].timeIntervalSinceReferenceDate;
        DLog(@"Thumbnail generated: %.3f sec", (tThumbEnd - tThumbStart));

        BOOL bOK = [fileManager createFileAtPath:thumbFilename contents:thumbData attributes:nil];
        if ( !bOK ) {
            DLog(@"Error creating thumbnail image.");
        }
    }
}

-(SZCAlertView*)progressView
{
    if (!_progressView) {
        _progressView = [[SZCAlertView alloc] initWithFrame:CGRectMake(0, 0, 120, 100)];
        _progressView.center = CGPointMake(CGRectGetMidX(self.view.bounds),CGRectGetMidY(self.view.bounds));
        _progressView.alpha = 0.0f;
        _progressView.backgroundColor = [UIColor blackColor];
        [_progressView setHidden:YES];
    }
    if (_progressView.superview != self.view)
        [self.view addSubview:_progressView];
    
    return _progressView;
}

-(void)showProgressViewWithTitle:(NSString*)title
{
    [self.progressView.spinner startAnimating];
    self.progressView.hidden = NO;
    self.progressView.title.text = title;
    [self.progressView layoutSubviews];
    [UIView animateWithDuration:0.5f animations:^{
        _progressView.alpha = 1.0f;
    }];
}

-(void)hideProgressView
{
    [UIView animateWithDuration:0.5f
                     animations:^{
                         _progressView.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         _progressView.hidden = YES;
                         [_progressView.spinner stopAnimating];
                     }];
}

- (void) playShutterAnimation
{
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.autoreverses = YES;
    animation.duration = 0.175f;
    animation.repeatCount = 1;
    animation.fromValue = @(0.0f);
    animation.toValue = @(1.0f);

    [self.shutterView.layer addAnimation:animation forKey:nil];
}

#pragma mark - SZLPhotoBrowserDelegate methods

- (BOOL)canTransferPhotosWithCount:(NSInteger)count
{
	return [(GDCameraAppDelegate*)[UIApplication sharedApplication].delegate canTransferPhotosWithCount:count];
}

- (void)photoBrowser:(SZLPhotoBrowserVC*)photoBrowser didDelete:(NSArray*)photos
{
    for (id photo in photos) {
        SZLFileImage* fileImage = [photo isKindOfClass:[SZLFileImage class]] ? (SZLFileImage*) photo : nil;
        
        if (fileImage) {
            NSError *err;
            SZLFileImage* thumbnail = fileImage.thumbnail;
            if (thumbnail) {
                if ([[SZLSecureFileManager defaultManager] fileExistsAtPath:thumbnail.path]) {
                    BOOL success = [[SZLSecureFileManager defaultManager] removeItemAtPath:thumbnail.path error:&err];
                    if (!success)
                        DLog(@"File delete failed, error: %@", [err description]);
                }
            }
            
            if ([[SZLSecureFileManager defaultManager] fileExistsAtPath:fileImage.path]) {
                BOOL success = [[SZLSecureFileManager defaultManager] removeItemAtPath:fileImage.path error:&err];
                if (!success)
                    DLog(@"File delete failed, error: %@", [err description]);
            }
        }
    }

}

- (void)photoBrowser:(SZLPhotoBrowserVC*)photoBrowser didSelect:(NSArray*)photos
{
	[(GDCameraAppDelegate*)[UIApplication sharedApplication].delegate provideRequestedPhotos:photos];
}

- (void)photoBrowser:(SZLPhotoBrowserVC*)photoBrowser didEmail:(NSArray*)photos fromBarButton:(UIBarButtonItem*)barButton
{
	[(GDCameraAppDelegate*)[UIApplication sharedApplication].delegate emailPhotos:photos fromBarButton:barButton];
}

- (void)photoBrowser:(SZLPhotoBrowserVC*)photoBrowser didTransfer:(NSArray*)photos fromBarButton:(UIBarButtonItem*)barButton
{
	[(GDCameraAppDelegate*)[UIApplication sharedApplication].delegate transferPhotos:photos fromBarButton:barButton];
}

- (void)photoBrowserDidRequestFiles:(SZLPhotoBrowserVC*)photoBrowser fromBarButton:(UIBarButtonItem*)barButton
{
	[(GDCameraAppDelegate*)[UIApplication sharedApplication].delegate importImagesFromBarButton:barButton withCompletionHandler:^(id params, NSArray* filePaths)
	{
		// Scroll to the images here?
	}];
}

@end

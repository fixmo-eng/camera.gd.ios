//
//  KRCCameraConstants.m
//  PowerCam
//
//  Created by Magali Boizot-Roche on 2013-07-18.
//  Copyright (c) 2013 KreevRoo Consulting. All rights reserved.
//

#import "KRCCameraConstants.h"

@implementation KRCCameraConstants

+(NSBundle *)resourceBundle
{
	
	
    //NSURL* resourceBundlePath = [[NSBundle mainBundle] URLForResource:@"SZBResources"
     //                                                   withExtension:@"bundle"];
    
	
	NSString *resourceBundlePath = [[NSBundle mainBundle] pathForResource:@"KRCCameraRsrc" ofType:@"bundle"];
	NSBundle *resourceBundle = [NSBundle bundleWithPath:resourceBundlePath];
	return resourceBundle;
}

+ (CGFloat) thumbnailPixelSize
{
    return 160.0f;
}

@end

/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import <UIKit/UIKit.h>

/*
 * Alert helper class (extends UIAlertView) allowing invocation of a predefined selector upon tapping OK or Cancel.
 * It acts as the Alert view delegate and performs invocation of designated objects/selectors for each action.
 * It is very handy to keep code streamlined as UIAlertView is not modal.
 */

@interface AlertViewWithOKCancelAndInvocation : UIAlertView <UIAlertViewDelegate>

- (id)initWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle  okButtonTitle:(NSString *)okButtonTitle;
- (void)prepareInvocationForCancelButtonWithSelector:(SEL)aSelector withObject:(id)object andArguments:(id)argument, ... NS_REQUIRES_NIL_TERMINATION;
- (void)prepareInvocationForOKButtonWithSelector:(SEL)aSelector withObject:(id)object andArguments:(id)argument, ... NS_REQUIRES_NIL_TERMINATION;

@end

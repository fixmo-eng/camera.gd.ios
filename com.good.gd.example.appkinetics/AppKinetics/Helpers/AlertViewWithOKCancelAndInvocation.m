/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */
#import "AlertViewWithOKCancelAndInvocation.h"

/*
 * Alert helper class (extends UIAlertView) allowing invocation of a predefined selector upon tapping OK or Cancel.
 * It acts as the Alert view delegate and performs invocation of designated objects/selectors for each action.
 * It is very handy to keep code streamlined as UIAlertView is not modal.
 */


@implementation AlertViewWithOKCancelAndInvocation
{
    SEL selectorForOK;
    SEL selectorForCancel;
    id  targetForOKInvocation;
    id  targetForCancelInvocation;
    NSMutableArray *argumentsForInvocationForOKAction;
    NSMutableArray *argumentsForInvocationForCancelAction;
}

- (void)doInvokeSelector:(SEL)aSelector withObject:(id)object andArguments:(NSMutableArray*)arguments
{
    if (object==nil)
        return;
    NSMethodSignature *aSignature = [object methodSignatureForSelector:aSelector];
    NSInvocation *anInvocation = [NSInvocation invocationWithMethodSignature:aSignature];
    [anInvocation setSelector:aSelector];
    [anInvocation setTarget:object];
    NSInteger argIndex = 2; // parameter indexes start at 2 due to first 2 indices being fixed, i.e. obj and cmd
    for (id arg in arguments)
    {
        if ([arg isKindOfClass:[NSValue class]])
        {
            NSValue *currentValue = (NSValue*)arg;
            NSUInteger bufferSize = 0;
            NSGetSizeAndAlignment([currentValue objCType], &bufferSize, NULL);
            void* buffer = malloc(bufferSize);
            [currentValue getValue:buffer];
            [anInvocation setArgument:buffer atIndex:argIndex++];
            free (buffer);
        }
        else
        {
            void *pOrg = (__bridge void*)arg;
            [anInvocation setArgument:&pOrg atIndex:argIndex++];
        }
    }
    [anInvocation invoke];
}
- (void)prepareInvocationForOKButtonWithSelector:(SEL)aSelector withObject:(id)object andArguments:(id)argument, ...
{
    if (argumentsForInvocationForOKAction == nil)
    {
        argumentsForInvocationForOKAction = [[NSMutableArray alloc] init];
    }
    [argumentsForInvocationForOKAction removeAllObjects]; // remove any object left from any previous calls to this method
    selectorForOK = aSelector;
    targetForOKInvocation = object;
    va_list args;
    va_start(args, argument);
    for (id arg = argument; arg != nil; arg = va_arg(args, id))
    {
        [argumentsForInvocationForOKAction addObject:arg];
    }
    va_end(args);
}

- (void)prepareInvocationForCancelButtonWithSelector:(SEL)aSelector withObject:(id)object andArguments:(NSObject *)argument, ...
{
    if (argumentsForInvocationForCancelAction == nil)
    {
        argumentsForInvocationForCancelAction = [[NSMutableArray alloc] init];
    }
    [argumentsForInvocationForCancelAction removeAllObjects]; // remove any object left from any previous calls to this method
    selectorForCancel = aSelector;
    targetForCancelInvocation = object;
    va_list args;
    va_start(args, argument);
    for (id arg = argument; arg != nil; arg = va_arg(args, id))
    {
        [argumentsForInvocationForCancelAction addObject:arg];
    }
    va_end(args);
    
}

- (id)initWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle  okButtonTitle:(NSString *)okButtonTitle
{
    self = [super initWithTitle:title message:message delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:okButtonTitle, nil];
    return self;
}


// Alert view delegate call which will get executed upon user's interaction merely invokes our setup
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [alertView cancelButtonIndex])
    {
        // Invoking Cancel action
        [self doInvokeSelector:selectorForCancel withObject:targetForCancelInvocation andArguments:argumentsForInvocationForCancelAction];
        
    }
    else
    {
        // Invoking OK action (or other button)
        [self doInvokeSelector:selectorForOK withObject:targetForOKInvocation andArguments:argumentsForInvocationForOKAction];
        
    }
    
}

@end

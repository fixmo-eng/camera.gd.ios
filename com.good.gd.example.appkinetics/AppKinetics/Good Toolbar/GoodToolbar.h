/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import <UIKit/UIKit.h>

// This toolbar is used in a few different places in both iPhone and iPad

@interface GoodToolbar : UIToolbar

@end

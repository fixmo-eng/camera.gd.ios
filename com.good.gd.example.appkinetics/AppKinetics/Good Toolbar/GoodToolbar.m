/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import "GoodToolbar.h"
#import "AboutViewController.h"

@implementation GoodToolbar

-(id)initWithCoder:(NSCoder *)coder
{
    
	self = [super initWithCoder:coder];
    
    if(self)
    {
        //Adjust the tint
        [self setTintColor:maincolor];
        
        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        [infoButton addTarget:self action:@selector(showAbout:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *infoBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
                      
        UIBarButtonItem *autospacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]; 
        
        NSBundle* assetsBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"GDAssets"
                                                                                          ofType:@"bundle"]];
        NSString* path = [assetsBundle pathForResource:@"SECURED_GOOD_LOGO"
                                                ofType:@"png"];
        UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:path]];
        UIBarButtonItem *logoItem = [[UIBarButtonItem alloc] initWithCustomView:logoImageView];
        
        NSArray *itemsArray = [NSArray arrayWithObjects:
                               infoBarButtonItem,
                               logoItem,
                               autospacer,
                               nil];
        
        [self setItems:itemsArray animated:NO];
    }
    
    return self;
}

// Add About as modal
-(void)showAbout:(id)sender
{
    AboutViewController *aboutViewController = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
     {
         aboutViewController.modalPresentationStyle = UIModalPresentationFormSheet;
     }
    [[[UIApplication sharedApplication] keyWindow].rootViewController presentModalViewController:aboutViewController animated:YES];

    
}


@end

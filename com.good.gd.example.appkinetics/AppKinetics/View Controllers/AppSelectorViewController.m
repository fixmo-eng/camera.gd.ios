
#import "AppSelectorViewController.h"
#import <GD/GDiOS.h>
#import <GD/GDAppDetail.h>
#import <GD/GDServices.h>


@interface AppSelectorViewController ()
{
    IBOutlet    UITableView *_tableView;    
    NSArray                 *_arrayGDAppDetail;
}


@end


@implementation AppSelectorViewController

@synthesize delegate = _delegate;

#pragma mark - Initialization

- (id) initWithNibName: (NSString *) nibNameOrNil bundle: (NSBundle *) nibBundleOrNil
{
    if (self = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil])
    {
        // Initialize local array with the list of apps conforming to the Transfer-file service as returned by -getApplicationDetailsForService:andVersion:
        NSMutableArray *arrayGDAppDetail = [[[GDiOS sharedInstance] getApplicationDetailsForService:kFileTransferServiceName andVersion:kFileTransferServiceVersion] mutableCopy];
        // Remove the app that points to self i.e. this appId
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"SELF.applicationId!=%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"GDApplicationID"]];
        _arrayGDAppDetail = [arrayGDAppDetail filteredArrayUsingPredicate:predicate];

    }

    return self;
}

#pragma mark - View Management


- (void) viewDidLoad
{
    [super viewDidLoad];
    CGRect      frame;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 43;
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.separatorColor = [UIColor darkGrayColor];
    _tableView.scrollEnabled = YES;
    // For non-phone UI, we most likely want to shrink the frame to list size to use in a popover
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        frame = self.view.frame;
        frame.size.width = 278;
        frame.size.height = _tableView.rowHeight * ([_arrayGDAppDetail count] + 1);
    
        if (frame.size.height > 300)
            frame.size.height = 300;
            
            
        self.view.frame = frame;
    }
    

}

#pragma mark - Table Management

- (NSInteger) tableView: (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    return [_arrayGDAppDetail count] + 1;
}

- (UITableViewCell *) tableView: (UITableView *) tableView cellForRowAtIndexPath: (NSIndexPath *) indexPath
{
	static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    UIView          *selectedView;
    
    NSUInteger      row = [indexPath row];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: CellIdentifier];

        selectedView = [[UIView alloc] init];
        selectedView.backgroundColor = [UIColor whiteColor];
    
        [cell setSelectedBackgroundView: selectedView];
    }
    
    if (row == 0)
    {
        // Set up Open-in header
        cell.textLabel.text = @"Open in Good Apps...";
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont systemFontOfSize:22.0f];
   }
    else
    {
        // Extract name from the GDAppDetail container
        GDAppDetail *appDetail = [_arrayGDAppDetail objectAtIndex: row - 1];
        cell.textLabel.text = [appDetail name];
        cell.textLabel.textColor = [UIColor blueColor];
        cell.textLabel.font = [UIFont systemFontOfSize:17.0f];
    }
    cell.textLabel.highlightedTextColor = [UIColor blackColor];
    return cell;
}


- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
    GDAppDetail     *appDetail = nil;
    NSUInteger      row = [indexPath row];
    if (row == 0)
        return;
    appDetail = [_arrayGDAppDetail objectAtIndex: [indexPath row] - 1];
        
    [_delegate appSelected:[appDetail address] withVersion:[appDetail versionId] andName:[appDetail name]];
}


@end

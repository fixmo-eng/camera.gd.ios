/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import "AppSelectorViewController.h"

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate, AppSelectorDelegate>


@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) AppSelectorViewController *appSelectorListViewController;

- (void) refreshIfDetailItem:(id)item;

@end

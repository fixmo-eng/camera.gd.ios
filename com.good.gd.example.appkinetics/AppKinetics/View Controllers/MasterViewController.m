/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "AlertViewWithOKCancelAndInvocation.h"

#import <GD/GDFileSystem.h>
#import <GD/GDCReadStream.h>
#import <GD/GDSecureDocs.h>
#import <GD/GDAppDetail.h>

#define kRequestFilesServiceName    @"com.good.gdservice.request-files"
#define kRequestFilesServiceVersion @"1.0.0.0"
#define kRequestFilesMethod         @"requestFiles"

@interface MasterViewController ()
{
    NSMutableArray *fileList;
}
@property (strong, nonatomic) GDService *service;
@property (strong, nonatomic) GDServiceClient *serviceClient;

@end

@implementation MasterViewController

- (NSString*)documentsFolderPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask ,YES );
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

- (NSURL*)urlForPath:(NSString*)path
{
    return [NSURL fileURLWithPath:[path stringByExpandingTildeInPath]];
}

- (void)addToUrlListPath:(NSString*)path
{
    [fileList addObject:[self urlForPath:path]];
 
}

- (void)insertToUrlListPath:(NSString*)path
{
    [fileList insertObject:[self urlForPath:path] atIndex:0];

}

- (void) copyDefaultFiles
{
    NSArray *arrayOfPdfFiles = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
    for (NSString *filePath in arrayOfPdfFiles)
    {
        NSData *fileData = [NSData dataWithContentsOfFile:filePath];
        NSString *documentPathForFile = [[self documentsFolderPath] stringByAppendingPathComponent:
                                         [[filePath pathComponents] lastObject]];
        
        NSError *error = nil;
        [GDFileSystem writeToFile:fileData
                             name:documentPathForFile
                            error:&error];
        if (!error)
        {
            [self addToUrlListPath:documentPathForFile];
        }
    }
}

- (void)documentsDirectoryToLocalList
{
    BOOL isDirectory = NO;
    NSError *error = nil;
    
    if (![GDFileSystem fileExistsAtPath:[self documentsFolderPath] isDirectory:&isDirectory])
    {
        [GDFileSystem createDirectoryAtPath:[self documentsFolderPath] withIntermediateDirectories:YES attributes:nil error:&error];
    }
    NSArray *filesArray = [GDFileSystem contentsOfDirectoryAtPath:[self documentsFolderPath] error:&error];
    
    for (NSString *filePath in filesArray)
    {
        [self addToUrlListPath:[[self documentsFolderPath] stringByAppendingPathComponent:filePath]];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        fileList = [[NSMutableArray alloc] init]; // this will hold file paths (NSURL)

        self.title = @"Documents";
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            self.clearsSelectionOnViewWillAppear = NO;
            self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
        }
        [self initGDServices];
    }
    return self;
}

//reset file list to the initial state: it will contain only the default files
-(IBAction) resetFileList:(id)sender
{
    //delete all the files
    for ( NSURL *urlToFile in fileList )
    {
        NSString *pathToFile = [urlToFile path];
        
        if ([self.detailViewController.detailItem isEqual:urlToFile])
        {
            self.detailViewController.detailItem = nil;
        }
        
        NSError *error = nil;
        [GDFileSystem removeItemAtPath:pathToFile error:&error];
    }
    [fileList removeAllObjects];
    
    [self copyDefaultFiles]; //copy all the default files
    [[self tableView] reloadData]; //refresh tableView
}

- (IBAction)getFilesFromProvider:(id)sender
{
	NSArray* serviceProviders = [[GDiOS sharedInstance] getApplicationDetailsForService:kRequestFilesServiceName andVersion:nil];
	NSString* appAddress = nil;
	NSString* appVersion = nil;
	
	if (serviceProviders.count > 0)
	{
		// FIXME: Give user a choice of which app to use.
		GDAppDetail* appDetail = appDetail = serviceProviders[0];
		
		appAddress = appDetail.address;
		appVersion = appDetail.versionId;
	}
	else
	{
		NSLog(@"No service providers were found for service: %@", kRequestFilesServiceName);
		appAddress = @"com.good.gd.camera";
		appVersion = @"1.0.0.0";
	}
    
	NSString* requestID = nil;
	NSError* error = nil;
	
	[GDServiceClient sendTo:appAddress withService:kRequestFilesServiceName withVersion:appVersion withMethod:kRequestFilesMethod withParams:nil withAttachments:nil bringServiceToFront:GDEPreferPeerInForeground requestID:&requestID error:&error];
	
	if (error)
	{
		NSLog(@"Error sending mail: %@", error);
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //create edit and reset buttons
    UIBarButtonItem* resetButtonItem =  [[UIBarButtonItem alloc]
                                         initWithTitle:@"Reset"
                                         style:UIBarButtonItemStyleBordered
                                         target:self
                                         action:@selector(resetFileList :)];
    
    [self.navigationItem setLeftBarButtonItems:@[self.editButtonItem, resetButtonItem]];
    
    [self documentsDirectoryToLocalList]; // populate local list of files from the Documents dir
    
    NSError *error = nil;
    NSArray *filesArray = [GDFileSystem contentsOfDirectoryAtPath:[self documentsFolderPath] error:&error];

    //if there are no files and it's the first start of the application, we'll copy default files
    if ([filesArray count] == 0)
    {
        //retrieving flag applicationWasLoaded indicating whether the application had been loaded before
        NSUserDefaults *savedState = [NSUserDefaults standardUserDefaults];
        Boolean applicationWasLoaded = [savedState boolForKey : @"applicationWasLoaded"];
        if ( NO == applicationWasLoaded )
        {
            // Copy sample pdf docs from the main bundle to the secure file system
            [self resetFileList:self];
            
            //save applicationWasLoaded flag
            [savedState setBool: YES forKey:@"applicationWasLoaded" ];
            [savedState synchronize ];
        }
    }
    
    UIBarButtonItem* addButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(getFilesFromProvider:)];
	self.navigationItem.rightBarButtonItem = addButtonItem;
	
    [[self tableView] reloadData];
}

- (void)viewDidUnload
{
    self.serviceClient = nil;
    self.service = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View delgates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return fileList.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }


    NSURL *urlToFile = fileList[indexPath.row];
    cell.textLabel.text = [[urlToFile path] lastPathComponent];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSURL *urlElement = fileList[indexPath.row];
        NSString *pathToFile = [urlElement path];
        
        if([self.detailViewController.detailItem isEqual:urlElement])
            self.detailViewController.detailItem = nil;
        
        NSError *error = nil;
        [GDFileSystem removeItemAtPath:pathToFile error:&error];
        
        // Here we should check for error and handle it appropriately
        
        [fileList removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSURL *urlElement = fileList[indexPath.row];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
	    if (!self.detailViewController)
        {
	        self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPhone" bundle:nil];
	    }
	    self.detailViewController.detailItem = urlElement;
        [self.navigationController pushViewController:self.detailViewController animated:YES];
    }
    else
    {
        self.detailViewController.detailItem = urlElement;
    }
}

#pragma mark - Init GD services

- (void)initGDServices
{
    // Create GD service
    self.service = [[GDService alloc] init];
    self.service.delegate = self;
    
    // Create GD ServiceClient
    self.serviceClient = [[GDServiceClient alloc] init];
    self.serviceClient.delegate = self;
    
  
}

#pragma mark - GDServiceDelegate

// This GD delegate call will be called when the service sends a file to us.
- (void) GDServiceDidReceiveFrom:(NSString*)application
                      forService:(NSString*)service
                     withVersion:(NSString*)version
                       forMethod:(NSString*)method
                      withParams:(id)params
                 withAttachments:(NSArray*)attachments
                    forRequestID:(NSString*)requestID
{
    
    /* 
     Check the following service-specific parameters before processing the call...
     1. Check the service is "com.good.gdservice.transfer-file".
     2. Check that the method is "transferFile".
     3. Check that params is nil.
     4. Check that there is exactly one item in the attachments array.
     */

    NSString *errDescription = nil;
    NSError *replyParams = nil;

    if (![service isEqualToString:kFileTransferServiceName])
    {
        errDescription = [NSString stringWithFormat:@"Service not found \"%@\"", service];
        replyParams = [NSError errorWithDomain:GDServicesErrorDomain code:GDServicesErrorServiceNotFound
                       userInfo:[NSDictionary dictionaryWithObject:errDescription
                                                            forKey:NSLocalizedDescriptionKey]];
    }
    else if (![method isEqualToString:kFileTransferMethod])
    {
        errDescription = [NSString stringWithFormat:@"Method not found \"%@\"",  method];
        replyParams = [NSError errorWithDomain:service code:GDServicesErrorMethodNotFound
                                      userInfo:[NSDictionary
                                      dictionaryWithObject:errDescription
                                      forKey:NSLocalizedDescriptionKey]];
    }
    else if (params || attachments.count!=1)
    {
        if (params)
        {
            errDescription = [NSString stringWithFormat:@"Parameters for method \"%@\" should be nil but are \"%@\"",  method, [params description]];
        }
        else
        {
            errDescription = [NSString
                              stringWithFormat:@"Attachments should have one element but has %d",
                              attachments.count];
        }
        replyParams = [NSError errorWithDomain:service code:GDServicesErrorInvalidParams
                                      userInfo:[NSDictionary
                                                dictionaryWithObject:errDescription
                                                forKey:NSLocalizedDescriptionKey]];
                
    }
    else // all OK, proceed with processing the attachment/payload
    {
        NSString *filePath = [attachments objectAtIndex:0];
        NSString *fileName = [filePath lastPathComponent];
        NSString *localFilePathToSave = [[self documentsFolderPath] stringByAppendingPathComponent:fileName];
        if ([GDFileSystem fileExistsAtPath:localFilePathToSave isDirectory:NO])
        {
            
            // Notify that file will be clobbered if saved as it exists
            AlertViewWithOKCancelAndInvocation *alert = [[AlertViewWithOKCancelAndInvocation alloc] initWithTitle:@"File exists!" message:[NSString stringWithFormat:@"File %@ already exist, select Cancel or Overwrite!", fileName] cancelButtonTitle:@"Cancel" okButtonTitle:@"Overwrite"];
            // We're only interested in OK action i.e. Overwrite, for Cancel we don't do anything.
            [alert prepareInvocationForOKButtonWithSelector:@selector(saveReceivedFile:filePath:) withObject:self andArguments:localFilePathToSave, filePath, nil];
            [alert show];
            return;
            
            
        }
        [self saveReceivedFile:localFilePathToSave filePath:filePath];
        if (! fileList)
        {
            fileList = [[NSMutableArray alloc] init];
        }
        // Update our local list with the newly received file
        [self insertToUrlListPath:localFilePathToSave];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    }
    // Lastly, we reply to the sender with the replyParams and a nil attachment
    NSError *replyErr;
    BOOL replyOK = [GDService replyTo:application
                    withParams:replyParams
                    bringClientToFront:GDENoForegroundPreference
                    withAttachments:nil
                    requestID:requestID
                    error:&replyErr];
    // If the reply failed, we simply log it in this example
    if (!replyOK)
    {
        NSLog(@"GDServiceDidReceiveFrom failed to reply \"%@\" %d \"%@\"",
        [replyErr domain], [replyErr code],
        [replyErr localizedDescription]);
    }

    
}

- (void)saveReceivedFile:(NSString *)localFilePathToSave filePath:(NSString *)filePath
{
    NSError *error = nil;
    [GDFileSystem moveItemAtPath:filePath toPath:localFilePathToSave error:&error];
    
    if (error)
    {
        GDFileStat *fileStat = NULL;
        
        NSError *errorInFileStat = nil;
        BOOL bReceivedFileStat = [GDFileSystem getFileStat:filePath to:fileStat error:&errorInFileStat];
        
        if(bReceivedFileStat && fileStat)
        {
            NSLog(@"error message:%@",[[error localizedDescription]  stringByAppendingFormat:@"\nFileStat - %@",fileStat]);
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Saving File"
                                                        message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else
    {
        // Upon the received file being successfully saved, inform the detailed controller to re-render its view, if this item's content is currently displayed (since the item's content has changed)
        [_detailViewController refreshIfDetailItem:[self urlForPath:localFilePathToSave]];
    }
}


#pragma mark - GDServiceClientDelegate

- (void) GDServiceClientDidReceiveFrom:(NSString*)application
                            withParams:(id)params
                       withAttachments:(NSArray*)attachments
              correspondingToRequestID:(NSString*)requestID
{
    // For now, simply log the request made...
    NSLog(@"GDServiceClientDidReceiveFrom Called with params application:(%@) params:(%@) attachments(y/n):(%@) requestID:(%@)", (application),(nil!=params?@"YES":@"NO"),(nil!=attachments?@"YES":@"NO"),(requestID));
    for (NSString *filePath in attachments)
	{
		NSError *error = nil;
		NSData *fileData = [GDFileSystem readFromFile:filePath error:&error];
		NSString *documentPathForFile = [[self documentsFolderPath] stringByAppendingPathComponent:[[filePath pathComponents] lastObject]];
        
		if (!error)
		{
			[GDFileSystem writeToFile:fileData name:documentPathForFile error:&error];
		}
		
		if (!error)
		{
			GDFileStat stat;
			[GDFileSystem getFileStat:documentPathForFile to:&stat error:&error];
			NSLog(@"File: %@  Size: %llu  Last Modified: %@", documentPathForFile, stat.fileLen, [NSDate dateWithTimeIntervalSince1970:stat.lastModifiedTime]);
		}
		
		if (!error)
		{
			[self addToUrlListPath:documentPathForFile];
		}
		else
		{
			NSLog(@"Error writing file: %@", error);
		}
	}
	
    [[self tableView] reloadData];
}

@end

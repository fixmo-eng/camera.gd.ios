/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

-(IBAction)dismissAbout:(id)sender;

@end

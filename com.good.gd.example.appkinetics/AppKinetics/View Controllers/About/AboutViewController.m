/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */


#import "AboutViewController.h"

@implementation AboutViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"About"];
}


// Action for close button
#pragma mark - action
-(void)dismissAbout:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}



@end

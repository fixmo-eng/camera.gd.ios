/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import "DetailViewController.h"
#import <GD/GDServices.h>
#import <GD/GDFileSystem.h>

/*
 *  Popover view for presenting the list of GD apps for AppKinetics file transfer
 */


@interface DetailViewController ()
{
    NSMutableDictionary *_mimeTypes;
}
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (strong, nonatomic) UIPopoverController *appSelectorListPopoverController;

- (void)configureView;
@end

@implementation DetailViewController
@synthesize webView = _webView;
@synthesize appSelectorListViewController = _appSelectorListViewController;
@synthesize appSelectorListPopoverController = _appSelectorListPopoverController;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    _detailItem = newDetailItem;
    
    // Update the view.
    // Note: we update the detailed view even in case of re-setting the same item, in case the content this item points to has since been overwritten.
    [self configureView];
    
    if (self.masterPopoverController != nil)
    {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

// This function should be called to re-render the item's content (if it is currently the item being shown)
- (void) refreshIfDetailItem:(id)item
{
    if ([item isMemberOfClass:[NSURL class]] && ([self.detailItem isEqual:item]))
    {
        [self configureView];        
    }
}

- (void)configureView
{
    
    // We configure UIWebView to display the content of the detailed item.

    if (self.detailItem && [self.detailItem isMemberOfClass:[NSURL class]])
    {
        NSURL *urlToFile = (NSURL*)self.detailItem;
        NSError *fileReadError = nil;
        // Present the file inside the webView, resolve mime type of the file, show only supported types
        NSString *filePathString = [urlToFile path];
        NSData *fileData = [GDFileSystem readFromFile:filePathString error:&fileReadError];
        NSString *pathExtension = [[filePathString pathExtension] lowercaseString]; // our extensions are all lower case in plist
        NSString *mimeType = [_mimeTypes objectForKey:pathExtension];
        if (mimeType)
        {
            // load the selected file into the webview
            [self.webView loadData:fileData MIMEType:mimeType textEncodingName:@"utf-8" baseURL:urlToFile];
        }
        else
        {
            // Show 'file type not supported' message on the webView
            static NSString * const HTMLContent = @"<html>"
            "<head>"
            "<title>   </title>"
            "<style type=\"text/css\">"
            "<!--h1	{text-align:center; font-family:Arial, Helvetica, Sans-Serif;}"
            "p	{text-indent:20px;"
            "}--></style></head><body bgcolor = \"#ffffcc\" text = \"#000000\"><div style=\"margin-top: 40px;\"></div><h1>This file type is not supported!</h1></body></html>";
            [self.webView loadHTMLString:HTMLContent baseURL:nil];

        }
         
    }
    else
    {
        // Clear the web-view content
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Here we configure views and buttons on the controller
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareFile:)];
    
    self.navigationItem.rightBarButtonItem = addButton;
    self.webView.allowsInlineMediaPlayback = YES;
    // set the bounce on the WebView off for our application (solution for iOS 5.0 and later)
    if ([_webView respondsToSelector:@selector(scrollView)])
    {
        [[_webView scrollView] setBounces: NO];
    }
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"Document View";
        // Load supported mime types from local plist
        NSString *mimeTypesFile = [[NSBundle mainBundle] pathForResource:@"SupportedMimeTypes" ofType:@"plist"];
        _mimeTypes= [[NSMutableDictionary alloc] initWithContentsOfFile:mimeTypesFile];
        NSLog(@"mime dictionary: %@", _mimeTypes);
    }
    return self;
}

#pragma mark - UIBarButton actions
- (void)shareFile:(id)sender
{
    if ([self.appSelectorListPopoverController isPopoverVisible])
    {
        return;
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        self.appSelectorListViewController = [[AppSelectorViewController alloc] initWithNibName: @"AppSelectorViewController_iPhone" bundle: nil];
        self.appSelectorListViewController.delegate = self;
        [self.navigationController pushViewController:self.appSelectorListViewController animated:YES];
        
    }
    else // UI for iPad
    {
        self.appSelectorListViewController = [[AppSelectorViewController alloc] initWithNibName: @"AppSelectorViewController_iPad" bundle: nil];
        self.appSelectorListViewController.delegate = self;
        
        self.appSelectorListPopoverController = [[UIPopoverController alloc] initWithContentViewController: self.appSelectorListViewController];
        _appSelectorListPopoverController.popoverContentSize = _appSelectorListViewController.view.frame.size;
        
        // 'Pop' the list of registered apps as a popover over the bar-button
        [self.appSelectorListPopoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
}
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = @"Files";
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

#pragma mark - AppSelectorDelegate delegates
/*
 * The following delegate calls GDServiceClient directly to perform file-transfer service for brevity and as an illustrative sample. In the production level code though, one might consider creating a secure store manager class or a service class to contain and manage desired functionality.
 */
- (void)appSelected:(NSString *)applicationAddress withVersion:(NSString *)version andName:(NSString *)name
{
    if (self.detailItem==nil)
    {
        UIAlertView *alert =  [[UIAlertView alloc] initWithTitle:@"Note"
                                    message:@"Please make sure a file is selected"
                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    // The designated app for sending of this file has been selected. Perform file-transfer service
    NSError *error = nil;
    static NSString * const errTitle = @"Error Sending File";
    
    NSString *secureFilePath = [(NSURL*)self.detailItem path];
    
    // Send the file
    if ([secureFilePath length]>0)
    {
        NSArray *attachments = [NSArray arrayWithObject:secureFilePath];
        NSString *requestID = nil;
        BOOL isRequestAccepted = [GDServiceClient sendTo:applicationAddress
                                             withService:kFileTransferServiceName
                                             withVersion:kFileTransferServiceVersion
                                              withMethod:kFileTransferMethod
                                              withParams:nil
                                         withAttachments:attachments
                                     bringServiceToFront:GDEPreferPeerInForeground
                                               requestID:&requestID
                                                   error:&error];
        if (!isRequestAccepted || error)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:errTitle
                                  message:[error localizedDescription]
                                  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
 
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:errTitle
                                    message:@"Filepath is missing"
                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }

}

@end

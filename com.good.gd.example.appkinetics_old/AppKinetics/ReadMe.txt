### AppKinetics ###

================================================================================
DESCRIPTION:

This sample shows how to search for, create and subscribe to client services (AppKinetics). It demonstrates these concepts by implementing a consumer and a provider for the Transfer File service. It is intended as a starting point for developers making use of Good Dynamics AppKinetics™ services.

For more examples visit the
Good Dynamics Network:
https://begood.good.com/community/gdn

Support:
https://begood.good.com/community/gdn/support

================================================================================
BUILD REQUIREMENTS:

iOS SDK 4.1 or later

================================================================================
RUNTIME REQUIREMENTS:

iOS 5.1 or later

================================================================================
CHANGES FROM PREVIOUS VERSIONS:

================================================================================
(c) 2013 Good Technology Corporation. All rights reserved.
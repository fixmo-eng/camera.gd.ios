/*
 *  AppDelegate.h
 *  AppKinetics
 *
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import <GD/GDiOS.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, GDiOSDelegate>
{
    BOOL started;
}


@property (strong, nonatomic) UINavigationController *navigationController;

@property (strong, nonatomic) UISplitViewController *splitViewController;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) GDiOS *good;

@end

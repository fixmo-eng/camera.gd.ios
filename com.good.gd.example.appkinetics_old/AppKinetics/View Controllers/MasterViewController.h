/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import <GD/GDServices.h>
#import <GD/GDiOS.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController <GDServiceDelegate, GDServiceClientDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;

@end

/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import <UIKit/UIKit.h>

@protocol AppSelectorDelegate
- (void) appSelected: (NSString *)applicationAddress withVersion:(NSString*)version andName:(NSString*)name;
@end

@interface AppSelectorViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, unsafe_unretained) id <AppSelectorDelegate> delegate;

@end

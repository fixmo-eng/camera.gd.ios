/*
 *  AppDelegate.m
 *  AppKinetics
 *
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

#import "AppDelegate.h"
#import "MasterViewController.h"
#import "DetailViewController.h"


@implementation AppDelegate
@synthesize window = _window;
@synthesize good = _good;
@synthesize navigationController = _navigationController;
@synthesize splitViewController = _splitViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Set the main window
    self.window = [[GDiOS sharedInstance] getWindow];
    
    //Set a GDiOS instance and set the delegate
    self.good = [GDiOS sharedInstance];
    _good.delegate = self;
    
    //Set a flag so that view controllers can check the status
    started = NO;
    
    //Show the Good Authentication UI
    [_good authorize];
    
    //See 'On Authorized' for example app progression    
    
    return YES;    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Good Dynamics Delegate Methods

-(void)handleEvent:(GDAppEvent*)anEvent
{
    /* Called from _good when events occur, such as system startup. */
    
    switch (anEvent.type)
    {
		case GDAppEventAuthorized:
        {
            [self onAuthorized:anEvent];
			break;
        }
		case GDAppEventNotAuthorized:
        {
            [self onNotAuthorized:anEvent];
			break;
        }
		case GDAppEventRemoteSettingsUpdate:
        {
            // handle app config changes
			break;
        }
        case GDAppEventServicesUpdate:
        {
            NSLog(@"GDAppEventServicesUpdate %@", anEvent.message);
            break;
        }
        default:
            NSLog(@"Unhandled Event");
            break;
    }
}

-(void) onNotAuthorized:(GDAppEvent*)anEvent
{
    /* Handle the Good Libraries not authorized event. */
    
    switch (anEvent.code)
    {
        case GDErrorActivationFailed:
        case GDErrorProvisioningFailed:
        case GDErrorPushConnectionTimeout:
        case GDErrorSecurityError:
        case GDErrorAppDenied:
        case GDErrorBlocked:
        case GDErrorWiped:
        case GDErrorRemoteLockout:
        case GDErrorPasswordChangeRequired:
        {
            // an condition has occured denying authorization, an application may wish to log these events
            NSLog(@"onNotAuthorized %@", anEvent.message);
            break;
        }
        case GDErrorIdleLockout:
        {
            // idle lockout is benign & informational
            break;
        }
        default:
            NSAssert(false, @"Unhandled not authorized event");
            break;
    }
}

// Handle the Good Libraries authorized event.
-(void) onAuthorized:(GDAppEvent*)anEvent
{
    switch (anEvent.code)
    {
        case GDErrorNone:
			if (!started)
			{
				// Lunch application UI here
				started = YES;
				MasterViewController *masterViewController = nil;
				
				if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
				{
					masterViewController = [[MasterViewController alloc] initWithNibName:@"MasterViewController_iPhone" bundle:nil];
					self.navigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];
					[self.navigationController.navigationBar setTintColor:maincolor];
					self.window.rootViewController = self.navigationController;
				}
				else
				{
					masterViewController = [[MasterViewController alloc] initWithNibName:@"MasterViewController_iPad" bundle:nil];
					UINavigationController *masterNavigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];

					DetailViewController *detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPad" bundle:nil];
					UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailViewController];

					// Set the tint of the nav bars
					[masterNavigationController.navigationBar setTintColor:maincolor];
					[detailNavigationController.navigationBar setTintColor:maincolor];

					masterViewController.detailViewController = detailViewController;

					// Set up the split-view controller
					self.splitViewController = [[UISplitViewController alloc] init];
					self.splitViewController.delegate = detailViewController;
					self.splitViewController.viewControllers = @[masterNavigationController, detailNavigationController];

					// Set window's root controller as the split-view controller
					self.window.rootViewController = self.splitViewController;
				}
			}
			break;
        
        default:
            NSAssert(false, @"Authorized startup with an error");
            break;
    }
}

@end

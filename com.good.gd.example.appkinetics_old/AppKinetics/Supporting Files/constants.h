/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

// This constants file is included in the pch file for common lightweight functions throughout the app


// Color related macros
#define RGB(r,g,b) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1.0f]
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

// Define Good color
#define maincolor RGB(232,23,54)

/* File Transfer Service defines
 *
 * This service is for transferring a copy of a file from one application to a second application. 
 * The action to be taken by the second application on the file is unspecified. The intended providers of this service would be 
 * applications that can take only a single action or that prompt the user what action to take as soon as the request is 
 * received.
 *
 * Track this page to be notified of new versions of the service.
 * Service ID: com.good.gdservice.transfer-file
 * Latest Version: 1.0.0.0
 *
 */
#define kFileTransferServiceName    @"com.good.gdservice.transfer-file"
#define kFileTransferServiceVersion @"1.0.0.0"
#define kFileTransferMethod         @"transferFile"
